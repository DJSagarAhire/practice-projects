##Deepest Node in Binary Tree

*Problem Statement*: Given a binary tree, find the deepest leaf node in it.

The solution uses the fact that the last node in the level-order traversal is guaranteed to be the deepest.
