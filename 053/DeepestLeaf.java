import java.util.Queue;
import java.util.LinkedList;

/**
   Finds the deepest node in a binary tree. Requires BinaryTree.class
 */
public class DeepestLeaf
{
    public static <Item> Item findIterative(BinaryTree<Item> t)
    {
	BinaryTree<Item>.Node root = t.root;

	Queue<BinaryTree<Item>.Node> q = new LinkedList<BinaryTree<Item>.Node>();
	q.add(root);

	BinaryTree<Item>.Node lastRemoved = null;
	while(!q.isEmpty())
	{
	    lastRemoved = q.remove();
	    if(lastRemoved.left != null)
		q.add(lastRemoved.left);
	    if(lastRemoved.right != null)
		q.add(lastRemoved.right);
	}

	return lastRemoved.item;
    }

    public static void main(String[] ar)
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(4);
	BinaryTree<Integer>.Node n2 = t.insertNode(t.root, 2, true);
	BinaryTree<Integer>.Node n6 = t.insertNode(t.root, 6, false);
	BinaryTree<Integer>.Node n5 = t.insertNode(n6, 5, true);

	System.out.println(findIterative(t));
    }
}
