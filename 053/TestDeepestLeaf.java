import static org.junit.Assert.*;
import org.junit.Test;

public class TestDeepestLeaf
{
    @Test
    public void lrSkew()
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(4);
	BinaryTree<Integer>.Node n2 = t.insertNode(t.root, 2, true);
	BinaryTree<Integer>.Node n3 = t.insertNode(n2, 3, false);
	BinaryTree<Integer>.Node n6 = t.insertNode(t.root, 6, false);

	assertEquals((int) DeepestLeaf.findIterative(t), 3);
    }

    @Test
    public void rlSkew()
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(4);
	BinaryTree<Integer>.Node n2 = t.insertNode(t.root, 2, true);
	BinaryTree<Integer>.Node n6 = t.insertNode(t.root, 6, false);
	BinaryTree<Integer>.Node n5 = t.insertNode(n6, 5, true);

	assertEquals((int) DeepestLeaf.findIterative(t), 5);
    }

    @Test
    public void leftLinear()
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(4);
	BinaryTree<Integer>.Node n3 = t.insertNode(t.root, 3, true);
	BinaryTree<Integer>.Node n2 = t.insertNode(n3, 2, true);
	BinaryTree<Integer>.Node n1 = t.insertNode(n2, 1, true);

	assertEquals((int) DeepestLeaf.findIterative(t), 1);
    }

    @Test
    public void rightLinear()
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(4);
	BinaryTree<Integer>.Node n5 = t.insertNode(t.root, 5, false);
	BinaryTree<Integer>.Node n6 = t.insertNode(n5, 6, false);
	BinaryTree<Integer>.Node n7 = t.insertNode(n6, 7, false);

	assertEquals((int) DeepestLeaf.findIterative(t), 7);
    }

    @Test
    public void multipleNodes()
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(4);
	BinaryTree<Integer>.Node n2 = t.insertNode(t.root, 2, true);
	BinaryTree<Integer>.Node n6 = t.insertNode(t.root, 6, false);

	assertEquals((int) DeepestLeaf.findIterative(t), 6);
    }
}
