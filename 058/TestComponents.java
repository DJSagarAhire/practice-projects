import static org.junit.Assert.*;
import org.junit.Test;

public class TestComponents
{
    @Test
    public void isolatedVertex()
    {
	GraphLists i1 = new GraphLists(1);

	assertEquals(Components.findNumber(i1), 1);
    }

    @Test
    public void connected()
    {
	GraphLists c3 = new GraphLists(3);
	c3.addEdge(0, 1);
	c3.addEdge(1, 0);
	c3.addEdge(0, 2);
	c3.addEdge(2, 0);
	c3.addEdge(1, 2);
	c3.addEdge(2, 1);

	assertEquals(Components.findNumber(c3), 1);
    }

    @Test
    public void multipleIsolatedVertices()
    {
	GraphLists i3 = new GraphLists(3);

	assertEquals(Components.findNumber(i3), 3);
    }

    @Test
    public void multiplePaths()
    {
	GraphLists p2p2 = new GraphLists(4);
	p2p2.addEdge(0, 1);
	p2p2.addEdge(1, 0);
	p2p2.addEdge(2, 3);
	p2p2.addEdge(3, 2);

	assertEquals(Components.findNumber(p2p2), 2);
    }

    @Test
    public void cycleAndIsolatedVertex()
    {
	GraphLists c3i3 = new GraphLists(6);
	c3i3.addEdge(0, 1);
	c3i3.addEdge(1, 0);
	c3i3.addEdge(0, 2);
	c3i3.addEdge(2, 0);
	c3i3.addEdge(1, 2);
	c3i3.addEdge(2, 1);

	assertEquals(Components.findNumber(c3i3), 4);
    }
}
