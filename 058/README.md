##Number of Components in a Graph

*Problem Statement*: Given a graph, find its number of connected components.

The algorithm to find the number of connected components uses breadth-first search as follows:

1: Start with all vertices unvisited, and number of components = 0.  
2: As long as at least one unvisited vertex exists, perform a breadth-first search starting from that vertex to visit all vertices that can be visited and increment the number of components.

This uses the fact that a graph traversal can visit all vertices that are connected to the particular vertex.

Time Complexity: **O(n)**  
Space Complexity: **O(n)**
