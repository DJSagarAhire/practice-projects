import java.util.Queue;

/**
   Finds the number of components of a graph. Requires GraphLists.class.
 */
public class Components
{
    public static int findNumber(GraphLists g)
    {
	boolean[] visited = new boolean[g.size];
	int components = 0;
	int n = 0;

	do
	{
	    components++;
	    visited = breadthFirst(g, n, visited);
	    n = firstUnvisited(visited);
	} while(n != -1);

	return components;
    }

    private static boolean[] breadthFirst(GraphLists g, int node, boolean[] visited)
    {
	boolean[] discovered = new boolean[g.size];
	System.arraycopy(visited, 0, discovered, 0, visited.length);

	Queue<Integer> nodeQueue = new java.util.ArrayDeque();
	nodeQueue.add(node);
	discovered[node] = true;

	while(nodeQueue.size() != 0)
	{
	    int currentNode = nodeQueue.remove();
	    visited[currentNode] = true;

	    for(Object connectedNode: g.lists[currentNode])
		if(!discovered[(int) connectedNode])
		{
		    discovered[(int) connectedNode] = true;
		    nodeQueue.add((int) connectedNode);
		}
	}

	return visited;
    }

    private static int firstUnvisited(boolean[] visited)
    {
	for(int i=0; i<visited.length; i++)
	    if(!visited[i])
		return i;
	return -1;
    }
}
