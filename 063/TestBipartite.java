import static org.junit.Assert.*;
import org.junit.Test;

public class TestBipartite
{
    @Test
    public void singleEdge()
    {
	GraphLists e = new GraphLists(2);
	e.addEdge(0, 1);
	e.addEdge(1, 0);

	assertTrue(Bipartite.checkBipartite(e));
    }

    @Test
    public void path()
    {
	GraphLists p4 = new GraphLists(4);
	p4.addEdge(0, 1);
	p4.addEdge(1, 0);
	p4.addEdge(1, 2);
	p4.addEdge(2, 1);
	p4.addEdge(2, 3);
	p4.addEdge(3, 2);

	assertTrue(Bipartite.checkBipartite(p4));
    }

    @Test
    public void oddCycle()
    {
	GraphLists c3e = new GraphLists(4);
	c3e.addEdge(0, 1);
	c3e.addEdge(1, 0);
	c3e.addEdge(1, 2);
	c3e.addEdge(2, 1);
	c3e.addEdge(2, 3);
	c3e.addEdge(3, 2);
	c3e.addEdge(0, 2);
	c3e.addEdge(2, 0);

	assertTrue(!Bipartite.checkBipartite(c3e));
    }

    @Test
    public void star()
    {
	GraphLists k14 = new GraphLists(5);
	k14.addEdge(0, 4);
	k14.addEdge(4, 0);
	k14.addEdge(1, 4);
	k14.addEdge(4, 1);
	k14.addEdge(2, 4);
	k14.addEdge(4, 2);
	k14.addEdge(3, 4);
	k14.addEdge(4, 3);

	assertTrue(Bipartite.checkBipartite(k14));
    }

    @Test
    public void disconnectedBipartite()
    {
	GraphLists k12e = new GraphLists(5);
	k12e.addEdge(0, 1);
	k12e.addEdge(1, 0);
	k12e.addEdge(1, 3);
	k12e.addEdge(3, 1);
	k12e.addEdge(2, 4);
	k12e.addEdge(4, 2);

	assertTrue(Bipartite.checkBipartite(k12e));
    }

    @Test
    public void disconnectedNonBipartite()
    {
	GraphLists k12c3e = new GraphLists(7);
	k12c3e.addEdge(0, 1);
	k12c3e.addEdge(1, 0);
	k12c3e.addEdge(1, 2);
	k12c3e.addEdge(2, 1);
	k12c3e.addEdge(2, 3);
	k12c3e.addEdge(3, 2);
	k12c3e.addEdge(0, 2);
	k12c3e.addEdge(2, 0);
	k12c3e.addEdge(4, 5);
	k12c3e.addEdge(5, 4);
	k12c3e.addEdge(5, 6);
	k12c3e.addEdge(6, 5);

	assertTrue(!Bipartite.checkBipartite(k12c3e));
    }
}
