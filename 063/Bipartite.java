import java.util.Queue;

/**
   Finds whether a graph is bipartite. Requires GraphLists.class.
 */
public class Bipartite
{
    public static boolean checkBipartite(GraphLists g)
    {
	Queue<Integer> q = new java.util.ArrayDeque<>();

	boolean[] visited = new boolean[g.size];
	boolean[] discovered = new boolean[g.size];
	int[] color = new int[g.size];
	int numVisited = 0;

	while(numVisited < g.size)
	{
	    // Start with an unvisited vertex
	    for(int i=0; i<g.size; i++)
		if(!visited[i])
		{
		    q.add(i);
		    discovered[i] = true;
		    color[i] = 1;
		    break;
		}

	    while(q.size() > 0)
	    {
		int node = q.remove();
		visited[node] = true;
		numVisited++;

		for(Object connectedNode: g.lists[node])
		{
		    if(color[node] == color[(int) connectedNode])
			return false;

		    if(!discovered[(int) connectedNode])
		    {
			discovered[(int) connectedNode] = true;
			color[(int) connectedNode] = color[node] == 1 ? 2 : 1;
			q.add((int) connectedNode);
		    }
		}
	    }
	}

	return true;
    }
}
