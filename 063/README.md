##Check if Graph is Bipartite

*Problem Statement*: Given a simple undirected graph, check if it is bipartite.

This is an application of BFS which is performed by 2-coloring the graph. If a 2-coloring is possible, then the graph is bipartite, otherwise it isn't. If the graph is disconnected, special care needs to be taken to perform BFS on all the components.
