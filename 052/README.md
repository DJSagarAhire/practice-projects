##Check if a Binary Tree is Balanced

*Problem Statement*: Accept a binary tree and check if it is balanced. The difference between the heights of any 2 subtrees of a balanced binary tree does not exceed 1.

The algorithm, at a particular node, finds the height of the 2 subtrees below it. If any of the subtrees is unbalanced, it returns -1. If the current node is unbalanced, it returns -1. Else it returns the height of the node.

Time Complexity: **O(n)**
