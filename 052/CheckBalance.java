/**
   Checks if a binary tree is balanced. Requires BinaryTree.class
 */
public class CheckBalance
{
    public static <Item> boolean check(BinaryTree<Item> t)
    {
	return checkNodes(t.root) != -1;
    }

    // Returns height if node is balanced, -1 if not
    private static <Item> int checkNodes(BinaryTree<Item>.Node n)
    {
	if(n == null)
	    return 0;

	int left = checkNodes(n.left);
	int right = checkNodes(n.right);

	if(left == -1 || right == -1)
	    return -1;
	else if(Math.abs(left-right) > 1)
	    return -1;
	else
	    return Math.max(left, right) + 1;
    }

    public static void main(String[] ar)
    {
	BinaryTree<Integer> t1 = new BinaryTree<Integer>(4);
	BinaryTree.Node n2 = t1.insertNode(t1.root, 2, true);
	// BinaryTree.Node n1 = t1.insertNode(n2, 1, true);
	// BinaryTree.Node n3 = t1.insertNode(n2, 3, false);
	BinaryTree.Node n8 = t1.insertNode(t1.root, 8, false);
	BinaryTree.Node n5 = t1.insertNode(n8, 5, true);
	BinaryTree.Node n6 = t1.insertNode(n5, 6, false);

	System.out.println(check(t1));
    }
}
