import java.util.ArrayList;
/**
   Finds the lowest common ancestor of 2 nodes in a binary tree. Requires BinaryTree.class.
 */
public class LCA
{
    public static <Item> Item findAncestor(Item i, Item j, BinaryTree<Item> t)
    {
	ArrayList<Item> pathi = findPath(i, t.root, new ArrayList<Item>());
	ArrayList<Item> pathj = findPath(j, t.root, new ArrayList<Item>());

	if(pathi == null || pathj == null)
	    return null;

	int minSize = Math.min(pathi.size(), pathj.size());

	for(int count=0; count<minSize; count++)
	    if(!pathi.get(count).equals(pathj.get(count)))
		return pathi.get(count-1);
	
	return pathi.get(minSize-1);
    }

    private static <Item> ArrayList<Item> findPath(Item i, BinaryTree<Item>.Node tn, ArrayList<Item> path)
    {
	if(tn == null)
	    return null;

	ArrayList<Item> newPath = new ArrayList(path);
	newPath.add(tn.item);

	if(tn.item.equals(i))
	    return newPath;

	ArrayList<Item> leftPath = findPath(i, tn.left, newPath);
	ArrayList<Item> rightPath = findPath(i, tn.right, newPath);

	if(leftPath != null)
	    return leftPath;
	else if(rightPath != null)
	    return rightPath;
	else
	    return null;
    }

    public static void main(String[] ar)
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(4);
	BinaryTree.Node n2 = t.insertNode(t.root, 2, true);
	BinaryTree.Node n1 = t.insertNode(n2, 1, true);
	BinaryTree.Node n3 = t.insertNode(n2, 3, false);
	BinaryTree.Node n6 = t.insertNode(t.root, 6, false);
	BinaryTree.Node n5 = t.insertNode(n6, 5, true);
	BinaryTree.Node n7 = t.insertNode(n6, 7, false);

	System.out.println("LCA of 1, 6: " + findAncestor(1, 6, t));
    }
}
