##Lowest Common Ancestor in Binary Tree

*Problem Statement*: Given a binary tree and values of 2 nodes in the binary tree, find the lowest common ancestor of the 2 nodes in the tree.

Consider the following image:

![LCA Example](http://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Lowest_common_ancestor.svg/130px-Lowest_common_ancestor.svg.png)

Here, the lowest common ancestor of nodes `x` and `y` is the one marked in dark green.

The algorithm used to find the lowest common ancestor is as follows:

1: Find the paths from the root to the 2 nodes `x` and `y` using recursion from the root. This takes O(n) time in general.  
2: Look for the first mismatching node in the 2 paths.  
3: Return the value prior to the first mismatch as the answer. If one does not exist, return the last entry of the shorter path. (This happens when `x` is a descendent of `y` or vice-versa).

Time Complexity: **O(n)**, Space Complexity: **O(h)**

For a binary search tree, this can be adapted into an O(h) time algorithm as path finding will require much fewer recursive calls.
