import static org.junit.Assert.*;
import org.junit.Test;

public class TestLCA
{
    private static BinaryTree<Integer> getCompleteTree()
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(4);
	BinaryTree.Node n2 = t.insertNode(t.root, 2, true);
	BinaryTree.Node n1 = t.insertNode(n2, 1, true);
	BinaryTree.Node n3 = t.insertNode(n2, 3, false);
	BinaryTree.Node n6 = t.insertNode(t.root, 6, false);
	BinaryTree.Node n5 = t.insertNode(n6, 5, true);
	BinaryTree.Node n7 = t.insertNode(n6, 7, false);

	return t;
    }

    private static BinaryTree<Integer> getLinearTree()
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(1);
	BinaryTree.Node n2 = t.insertNode(t.root, 2, false);
	BinaryTree.Node n3 = t.insertNode(n2, 3, false);
	BinaryTree.Node n4 = t.insertNode(n3, 4, false);
	BinaryTree.Node n5 = t.insertNode(n4, 5, false);

	return t;
    }

    @Test
    public void cbtRoot()
    {
	BinaryTree<Integer> t = getCompleteTree();

	assertEquals((int) LCA.findAncestor(2, 7, t), 4);
    }

    @Test
    public void cbtSubtree()
    {
	BinaryTree<Integer> t = getCompleteTree();

	assertEquals((int) LCA.findAncestor(1, 3, t), 2);
    }

    @Test
    public void cbtNodeItself()
    {
	BinaryTree<Integer> t = getCompleteTree();

	assertEquals((int) LCA.findAncestor(2, 3, t), 2);
    }

    @Test
    public void cbtNonExisting()
    {
	BinaryTree<Integer> t = getCompleteTree();

	assertNull(LCA.findAncestor(20, 3, t));
    }

    @Test
    public void linNormal()
    {
	BinaryTree<Integer> t = getLinearTree();

	assertEquals((int) LCA.findAncestor(2, 4, t), 2);
    }

    @Test
    public void linNonExisting()
    {
	BinaryTree<Integer> t = getLinearTree();

	assertNull(LCA.findAncestor(8, -3, t));
    }
}
