/**
   Implements a solution to the Josephus problem using Queues.
   Requires MyQueue.class
   @author Sagar Ahire
 */

public class Josephus
{
    MyQueue<Integer> q = new MyQueue<Integer>();

    /**
       Initializes the queue for a run of the algorithm.
       @param n Number of people in the initial circle, i.e. size of the queue
     */
    private void initializeQueue(int n)
    {
	for(int i=1; i<=n; i++)
	    q.enqueue(i);
    }

    /**
       Accepts the number of people in the initial circle and the interval after which each person will be killed.
       Prints the sequence of kills to stdout, followed by the last surviving person.
       @param n Number of people in the initial circle
       @param m Interval after which next person is killed
     */
    public void solve(int n, int m)
    {
	if(n <= 1)
	    throw new IllegalArgumentException("Invalid value for n: " + n);

	if(m <= 1)
	    throw new IllegalArgumentException("Invalid value for m: " + m);

	initializeQueue(n);

	int i=0;
	while(q.size() != 1)
	{
	    i++;

	    int person = q.dequeue();
	    if(i == m)
	    {
		System.out.print(person + " ");
		i = 0;
	    }
	    else
		q.enqueue(person);
	}
	System.out.println("\nSurvivor: " + q.dequeue());
    }

    public static void main(String[] ar)
    {
	Josephus test = new Josephus();
	test.solve(9, 2);
    }
}
