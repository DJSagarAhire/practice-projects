##Josephus Problem using Queues

*Problem Statement*: Solve the [Josephus Problem](http://en.wikipedia.org/wiki/Josephus_problem) for a given *n* and *m* using Queues.

The Josephus problem involves *n* people sitting in a circle. Starting from person #1, every *m*th person is killed. The problem is to find the order in which the people are killed, and thereby, finding the last person to survive.

This can be solved using Queues as follows:

1: Accept 2 integers `n` and `m`. If either of the two are less than or equal to 1, throw an `IllegalArgumentException`.

2: Initialize a queue with `n` elements 1, 2, ..., `n`.

3: Set a counter `i` = 0.

4: While the size of the queue is not 1, do the following:  
4.1: Increment `i`.  
4.2: Dequeue a `person` from the queue.  
4.3: If `i` == `m`, `person` is killed. Print `person` and set `i`=0.  
4.4: Else, `person` survives. Enqueue it back.

5: Now that the size of the queue is 1, dequeue the last `person` and print it out as the survivor.

The complexity of this implementation is **O(n)**. However, this is because the entire order of killings is output by the algorithm.
