##Root-to-Leaf Paths of Binary Tree

*Problem Statement*: Given a binary tree, print all root-to-leaf paths in it.

Simple depth-first recursive traversal. Nothing to see here.
