import java.util.ArrayList;
import java.util.Arrays;

/**
   Prints all root-to-leaf paths of a given binary tree. Requires BinaryTree.class
 */
public class RootToLeaf
{
    public static void printPaths(BinaryTree t)
    {
	printNodePaths(t.root, new ArrayList());
    }

    private static void printNodePaths(BinaryTree.Node tn, ArrayList path)
    {
	ArrayList newpath = new ArrayList(path);
	newpath.add(tn.item);

	if(tn.left != null)
	    printNodePaths(tn.left, newpath);
	if(tn.right != null)
	    printNodePaths(tn.right, newpath);

	if(tn.left == null && tn.right == null)
	{
	    Object[] arr = new Object[newpath.size()];
	    newpath.toArray(arr);
	    System.out.println(Arrays.toString(arr));
	}
    }

    public static void main(String[] ar)
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(4);
	BinaryTree.Node n1 = t.insertNode(t.root, 2, true);
	BinaryTree.Node n2 = t.insertNode(n1, 1, true);
	BinaryTree.Node n3 = t.insertNode(n1, 3, false);
	BinaryTree.Node n4 = t.insertNode(t.root, 7, false);

	printPaths(t);
    }
}
