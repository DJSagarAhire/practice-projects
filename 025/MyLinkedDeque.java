public class MyLinkedDeque<Item>
{
    private class Node
    {
	Item item;
	Node prev;
	Node next;
    }
    private Node front = null;
    private Node back = null;
    private int size = 0;

    public void enqueueFront(Item i)
    {
	Node n = new Node();
	n.item = i;
	n.prev = null;

	if(front != null)
	    front.prev = n;
	n.next = front;

	front = n;
	if(back == null)
	    back = n;
	size++;
    }

    public void enqueueBack(Item i)
    {
	Node n = new Node();
	n.item = i;
	n.next = null;

	if(back != null)
	    back.next = n;
	n.prev = back;

	back = n;
	if(front == null)
	    front = n;
	size++;
    }

    public Item dequeueFront()
    {
	if(front == null)
	    throw new java.util.NoSuchElementException("Attempt to dequeue from an empty deque");

	Item i = front.item;
	front = front.next;

	if(front != null)
	    front.prev = null;

	if(front == null)
	    back = null;

	size--;
	return i;
    }

    public Item dequeueBack()
    {
	if(front == null)
	    throw new java.util.NoSuchElementException("Attempt to dequeue from an empty deque");

	Item i = back.item;
	back = back.prev;

	if(back != null)
	    back.next = null;

	if(back == null)
	    front = null;

	size--;
	return i;
    }

    public Item peekFront()
    {
	if(front == null)
	    throw new java.util.NoSuchElementException("Attempt to dequeue from an empty deque");

	return front.item;
    }

    public Item peekBack()
    {
	if(front == null)
	    throw new java.util.NoSuchElementException("Attempt to dequeue from an empty deque");

	return back.item;
    }

    public int getSize()
    {
	return size;
    }

    public boolean isEmpty()
    {
	return front == null;
    }

    public static void main(String[] ar)
    {
	MyLinkedDeque<Integer> test = new MyLinkedDeque<Integer>();

	System.out.println("Enqueue 1 at back.");
	test.enqueueBack(1);

	System.out.println("Enqueue 2 at front.");
	test.enqueueFront(2);

	System.out.println("Dequeue from front: " + test.dequeueFront());

	System.out.println("Enqueue 3 at back.");
	test.enqueueBack(3);

	System.out.println("Size: " + test.getSize());

	System.out.println("Dequeue from back: " + test.dequeueBack());

	System.out.println("Peek from back: " + test.peekBack());

	System.out.println("Size: " + test.getSize());

	System.out.println("Enqueue 4 at back.");
	test.enqueueBack(4);

	System.out.println("Enqueue 5 at back.");
	test.enqueueBack(5);

	System.out.println("Dequeue from front: " + test.dequeueFront());

	System.out.println("Dequeue from front: " + test.dequeueFront());

	System.out.println("Dequeue from back: " + test.dequeueBack());
    }
}
