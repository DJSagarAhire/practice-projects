/**
   This class contains implementations of methods to compute the nth Fibonacci number, for some 'n'
   @author Sagar Ahire
 */
public class Fibonacci
{
    /**
       Computes the nth Fibonacci number using recursion.
       @param n A whole number (0, 1, 2, ...)
       @return The nth Fibonacci number
     */
    public int recursiveFibonacci(int n)
    {
	if(n < 0)
	    throw new IllegalArgumentException("Negative number found");
	if(n == 0 || n == 1)
	    return n;
	else
	    return recursiveFibonacci(n-1) + recursiveFibonacci(n-2);
    }

    /**
       Computes the nth Fibonacci number using iteration.
       @param n A whole number (0, 1, 2, ...)
       @return The nth Fibonacci number
     */
    public int iterativeFibonacci(int n)
    {
	if(n < 0)
	    throw new IllegalArgumentException("Negative number found");
	int cur = 0, next = 1;

	for(int i=0; i<n; i++)
	{
	    next += cur;
	    cur = next - cur;
	}
	return cur;
    }

    /**
       Driver method for testing
     */
    public static void main(String[] ar)
    {
	int num;
	if(ar.length == 1)
	    num = Integer.parseInt(ar[0]);
	else
	    num = 8;
	Fibonacci test = new Fibonacci();
	System.out.println("Computing recursively the fibonacci number " + num + ": " + test.recursiveFibonacci(num));
	System.out.println("Computing iteratively the fibonacci number " + num + ": " + test.iterativeFibonacci(num));
    }
}
