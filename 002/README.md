##002: Return the nth Fibonacci Number

Two Java implementations are provided in `Fibonacci.java`. The recursive implementation is provided in `recursiveFibonacci()` while the iterative implementation is provided in `iterativeFibonacci()`. Both functions accept a whole number `n` and return the `n`^th fibonacci number (conforming to [this list](https://en.wikipedia.org/wiki/Fibonacci_number#List_of_Fibonacci_numbers)).

###Recursive Implementation

The steps followed in the recursive implementation are as follows:

1: Accept `n`. If it is negative, throw an `IllegalArgumentException`.

2: If `n` is 0 or 1, return `n`.

3: Return `recursiveFibonacci(n-1)` + `recursiveFibonacci(n-2)`. This directly implements the idea of a fibonacci number being the sum of the previous 2 fibonacci numbers.

The disadvantage of this method is that the number of invocations to `recursiveFibonacci()` is exponential in `n` (each invocation branches off into 2 invocations). In addition, the space requirements are as much as the time requirements due to use of stack space.

###Iterative Implementation

The iterative implementation does away with function calls and instead adds up the numbers in a loop. The idea is that the *next* fibonacci number can be computed using the current and the previous fibonacci number. Then the current fibonacci number can be returned. This allows the entire computation to proceed using only 2 variables + 1 counter.

The steps followed in the iterative implementation are as follows:

1: Accept `n`. If it is negative, throw an `IllegalArgumentException`.

2: Initialize `cur`, the current fibonacci number, to 0, and `next`, the next fibonacci number, to 1.

3: For `i` = 0 to (`n`-1), do the following:

3.1: `next` = `next` + `cur` -- Updates the next fibonacci number using the 2 currently available

3.2: `cur` = `next` - `cur` -- Updates the current fibonacci number by extracting it from `next`
	
4: Return `cur`.

This runs in **O(n)** time with **O(1)** space requirements.
