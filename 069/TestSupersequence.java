import static org.junit.Assert.*;
import org.junit.Test;

public class TestSupersequence
{
    @Test
    public void sameStrings()
    {
	assertEquals(Supersequence.find("hello", "hello"), 5);
    }

    @Test
    public void fullyDifferentStrings()
    {
	assertEquals(Supersequence.find("abc", "defg"), 7);
    }

    @Test
    public void partiallyDifferent()
    {
	assertEquals(Supersequence.find("beta", "theta"), 6);
    }

    @Test
    public void subString()
    {
	assertEquals(Supersequence.find("triangle", "angle"), 8);
    }
}
