##Shortest Common Supersequence

*Problem Statement*: Given 2 strings `s` and `t`, find the length of the shortest common supersequence. A supersequence of `s` and `t` is a sequence which contains both `s` and `t` as subsequences.

This is extremely similar to minimum edit distance, with the only difference being that if 2 characters in the 2 strings match, we still need to add 1 to the cost because the final supersequence needs to have the corresponding character.

The recurrence is, thus, given as follows:  
![](http://quicklatex.com/cache3/ql_36cbbe50aead553fb0b4981f5f72c4ee_l3.png)

Time Complexity: **O(mn)**  
Space Complecity: **O(mn)**
