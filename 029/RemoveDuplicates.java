import java.util.HashMap;

public class RemoveDuplicates
{
    public <Item> void removeDuplicates(MySkeletalLinkedList<Item> l)
    {
	if(l.head == null)
	    return;

	HashMap<Item, Boolean> map = new HashMap<Item, Boolean>();
	MySkeletalLinkedList.Node prev = null;
	MySkeletalLinkedList.Node curr = l.head;

	while(curr != null)
	{
	    if(map.containsKey(curr.item))
	    {
		prev.next = curr.next;
		curr = curr.next;
	    }
	    else
	    {
		map.put((Item)curr.item, true);
		if(prev == null)
		    prev = l.head;
		else
		    prev = prev.next;
		curr = curr.next;
	    }
	}
    }

    public static void main(String[] ar)
    {
	RemoveDuplicates test = new RemoveDuplicates();

	MySkeletalLinkedList<Integer> l1 = new MySkeletalLinkedList<Integer>();
	l1.createList(new Integer[] {1,2,2,4});
	test.removeDuplicates(l1);
	l1.printList();
	System.out.println();

	MySkeletalLinkedList<Integer> l2 = new MySkeletalLinkedList<Integer>();
	l2.createList(new Integer[] {1,1,1});
	test.removeDuplicates(l2);
	l2.printList();
	System.out.println();

	MySkeletalLinkedList<Integer> l3 = new MySkeletalLinkedList<Integer>();
	l3.createList(new Integer[] {1,2,3,4});
	test.removeDuplicates(l3);
	l3.printList();
	System.out.println();
    }
}
