import java.util.List;
import java.util.LinkedList;
import java.util.Iterator;

/**
   This class contains an implementation of the Sieve of Eratosthenes method to find a list of Prime numbers.
   @author Sagar Ahire
 */
public class Sieve
{
    /**
       Returns an array of prime numbers up to a specific integer 'num' using the Sieve of Eratosthenes method
       @param num An integer (num >= 2)
       @return An integer array containing all prime numbers less than 'num'
     */
    public List<Integer> listPrimes(int num)
    {
	double numsqrt = Math.sqrt(num);
	List<Integer> primes = new LinkedList<Integer>();

	// Initialize list
	for(Integer i=2; i<=num; i++) 
	    primes.add(i);

	// Implement Sieve
	for(int j=0; j<primes.size(); j++)
	{
	    Integer divisor = primes.get(j); // Outer loop get = n * O(n) = O(n^2)
	    if(divisor > numsqrt)
		break;

	    Iterator<Integer> iterator = primes.listIterator(j+1);
	    while(iterator.hasNext())
	    {
		Integer k = iterator.next(); // Inner loop iterator get = n^2 * O(1) = O(n^2)
		if(k % divisor == 0)
		    iterator.remove(); // Inner loop iterator remove = n^2 * O(1) = O(n^2)
	    }
	}
	return primes;
    }

    /**
       Prints an array of prime numbers up to a specific integer 'num'. Uses 'listPrimes()'.
       @param num An integer (num >= 2)
     */
    public void printPrimes(int num)
    {
	List<Integer> primes = listPrimes(num);
	for(Integer p : primes)
	    System.out.println(p);
    }

    public static void main(String[] ar)
    {
	int num;
	if(ar.length == 1)
	    num = Integer.parseInt(ar[0]);
	else
	    num = 20;

	Sieve test = new Sieve();
	test.printPrimes(num);
    }
}
