##003: Sieve of Eratosthenes

*Problem Statement*: Using the Sieve of Eratosthenes method, return/print a list of prime numbers less than a specified number `num`.

A Java implementation is provided. It makes heavy use of the `LinkedList` as the major operation in the sieve algorithm is creation of a list, followed by removal of elements at each iteration. Note that, for a linked list,

* Retrieval using `get()` is O(n)
* Retrieval using an iterator is O(1)
* Removal using an iterator is O(1)

The algorithm to create a list of primes using the sieve can thus be created as follows:

1: Accept `num`. If it is less than 2, throw an `IllegalArgumentException`.

2: Initialize a `LinkedList` of `Integer`s, `primes` to hold values from 2 to `num`.

3: For `j` = 0 to `primes.size()`, do the following. (Note that the size automatically keeps reducing as elements are removed by the sieve.)

3.1: Retrieve the element at `j`. This is the `divisor` for this round of the sieve.

3.2: If `divisor` is greater than `sqrt(num)`, stop. No more primes can be removed.

3.3: Starting at location `j`+1, start retrieving elements of the sieve. This is performed using an iterator to get the benefits of constant-time retrieval and removal.

3.4: For each such element `k`, if `k` % `divisor` = 0, remove `k` from `primes`.

4: Return `primes`.
