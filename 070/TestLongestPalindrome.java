import static org.junit.Assert.*;
import org.junit.Test;

public class TestLongestPalindrome
{
    @Test
    public void oddFullPalindrome()
    {
	assertEquals(LongestPalindrome.find("abbba"), "abbba");
    }

    @Test
    public void evenFullPalindrome()
    {
	assertEquals(LongestPalindrome.find("abba"), "abba");
    }

    @Test
    public void oddSubPalindrome()
    {
	assertEquals(LongestPalindrome.find("abaa"), "aba");
    }

    @Test
    public void evenSubPalindrome()
    {
	assertEquals(LongestPalindrome.find("abaabbaa"), "aabbaa");
    }

    @Test
    public void noPalindrome()
    {
	assertEquals(LongestPalindrome.find("abcd"), "a");
    }
}
