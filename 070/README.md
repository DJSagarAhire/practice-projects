##Longest Palindromic Substring

*Problem Statement*: Given a string `s`, find the longest substring in it which is a plaindrome.

The algorithm used performs the task in **O(n^2)** time and **O(1)** extra space. This is known as the *Center Expansion Algorithm*. It uses the fact that every character by itself is a palindrome, and a string is a palindrome if its first anc last character are equal and the rest of it is already a palindrome. The algorithm is also covered [here](http://www.geeksforgeeks.org/longest-palindromic-substring-set-2/) in slightly more detail.
