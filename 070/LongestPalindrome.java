public class LongestPalindrome
{
    public static String find(String s)
    {
	String currentLongest = "";
	int begin = s.length();

	// Odd-length palindromes with center at s[k]
	for(int k=0; k<s.length(); k++)
	{
	    // Number of characters before & after center
	    for(int l=0; l<s.length(); l++)
	    {
		int i = k-l;
		int j = k+l;

		if(i < 0 || j >= s.length())
		    break;

		if(s.charAt(i) != s.charAt(j))
		    break;

		if(j-i+1 > currentLongest.length() || (j-i+1 == currentLongest.length() && i < begin))
		{
		    currentLongest = s.substring(i, j+1);
		    begin = i;
		}
	    }
	}

	// Even-length palindromes with center at s[k] and s[k+1]
	for(int k=0; k<s.length()-1; k++)
	{
	    // Number of characters before and after center
	    for(int l=0; l<s.length(); l++)
	    {
		int i = k-l;
		int j = k+1+l;

		if(i < 0 || j >= s.length())
		    break;

		if(s.charAt(i) != s.charAt(j))
		    break;

		if(j-i+1 > currentLongest.length() || (j-i+1 == currentLongest.length() && i < begin))
		    currentLongest = s.substring(i, j+1);
	    }
	}

	return currentLongest;
    }
}
