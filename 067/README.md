##Clock Angle Problem

*Problem Statement*: Given the time on a clock, calculate the angle between the hour hand and the minute hand. Angles are measured as the smallest possible, i.e. reflex angles are not allowed.

The code is rather self-explanatory. :)
