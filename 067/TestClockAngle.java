import static org.junit.Assert.*;
import org.junit.Test;

public class TestClockAngle
{
    @Test
    public void clock1200()
    {
	assertEquals(ClockAngle.angle(12, 0), 0, 0.01);
    }

    @Test
    public void clock0630()
    {
	assertEquals(ClockAngle.angle(6, 30), 15, 0.01);
    }

    @Test
    public void clock0530()
    {
	assertEquals(ClockAngle.angle(5, 30), 15, 0.01);
    }

    @Test
    public void clock0600()
    {
	assertEquals(ClockAngle.angle(6, 0), 180, 0.01);
    }

    @Test
    public void clock0220()
    {
	assertEquals(ClockAngle.angle(2, 20), 50, 0.01);
    }
}
