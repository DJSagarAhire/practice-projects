public class ClockAngle
{
    public static double angle(int h, int m)
    {
	double exacth = h + m/60.0;
	double ang = Math.abs(30*exacth - 6*m);
	ang = Math.min(ang, 360-ang);
	return ang;
    }
}
