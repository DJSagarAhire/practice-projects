import java.util.HashMap;

/**
   Gives length of shortest path between 2 vertices of a weighted graph using Dijkstra's Shortest Path algorithm.
   Requires WeightedGraph.class
 */
public class Dijkstra
{
    // Finds length of shortest path between vertex 's' and 't' in graph 'g'
    public static double findLength(WeightedGraph g, int s, int t)
    {
	HashMap<Integer, Double> tentativeLengths = new HashMap<>();
	HashMap<Integer, Double> finalLengths = new HashMap<>();
	finalLengths.put(s, 0.0);
	for(int v: g.edges[s].keySet())
	    tentativeLengths.put(v, g.edges[s].get(v));

	boolean found = false;
	while(tentativeLengths.size() > 0)
	{
	    int minTentative = getMinimum(tentativeLengths);
	    double minTentativeLength = tentativeLengths.remove(minTentative);
	    finalLengths.put(minTentative, minTentativeLength);

	    if(minTentative == t)
	    {
		found = true;
		break;
	    }

	    for(int v: g.edges[minTentative].keySet())
	    {
		double candidateTentative = minTentativeLength + g.edges[minTentative].get(v);
		if(tentativeLengths.containsKey(v))
		    tentativeLengths.put(v, Math.min(tentativeLengths.get(v), candidateTentative));
		else
		    tentativeLengths.put(v, candidateTentative);
	    }
	}

	if(found)
	    return finalLengths.get(t);
	else
	    return -1.0;
    }

    private static int getMinimum(HashMap<Integer, Double> minWeights)
    {
	int minVertex = Integer.MAX_VALUE;
	double minValue = Double.MAX_VALUE;

	for(int v: minWeights.keySet())
	    if(minWeights.get(v) < minValue)
	    {
		minVertex = v;
		minValue = minWeights.get(v);
	    }

	return minVertex;
    }
}
