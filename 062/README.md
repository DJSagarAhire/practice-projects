##Dijkstra's Shortest Path Algorithm

*Problem Statement*: Given a weighted graph `g` and two vertices `s` and `t` in `g`, find the length of the shortest path between `s` and `t` using [Dijkstra's shortest path algorithm](http://en.wikipedia.org/wiki/Dijkstra's_algorithm).

An implementation that runs in **O(n^2)** is given.
