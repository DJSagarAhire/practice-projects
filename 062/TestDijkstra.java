import static org.junit.Assert.*;
import org.junit.Test;

public class TestDijkstra
{
    @Test
    public void path()
    {
	WeightedGraph p4 = new WeightedGraph(4);

	p4.addEdge(0, 1, 2);
	p4.addEdge(1, 0, 2);
	p4.addEdge(1, 2, 4);
	p4.addEdge(2, 1, 4);
	p4.addEdge(2, 3, 12);
	p4.addEdge(3, 2, 12);

	assertEquals(Dijkstra.findLength(p4, 1, 3), 16, 0.01);
    }

    @Test
    public void pathWithAddedEdge1()
    {
	WeightedGraph p4 = new WeightedGraph(4);

	p4.addEdge(0, 1, 2);
	p4.addEdge(1, 0, 2);
	p4.addEdge(1, 2, 4);
	p4.addEdge(2, 1, 4);
	p4.addEdge(2, 3, 12);
	p4.addEdge(3, 2, 12);
	p4.addEdge(1, 3, 3);
	p4.addEdge(3, 1, 3);

	assertEquals(Dijkstra.findLength(p4, 0, 3), 5, 0.01);
    }

    @Test
    public void pathWithAddedEdge2()
    {
	WeightedGraph p4 = new WeightedGraph(4);

	p4.addEdge(0, 1, 2);
	p4.addEdge(1, 0, 2);
	p4.addEdge(1, 2, 4);
	p4.addEdge(2, 1, 4);
	p4.addEdge(2, 3, 3);
	p4.addEdge(3, 2, 3);
	p4.addEdge(1, 3, 12);
	p4.addEdge(3, 1, 12);

	assertEquals(Dijkstra.findLength(p4, 0, 3), 9, 0.01);
    }

    @Test
    public void complete()
    {
	WeightedGraph k4 = new WeightedGraph(4);

	k4.addEdge(0, 1, 6);
	k4.addEdge(1, 0, 6);
	k4.addEdge(1, 2, 8);
	k4.addEdge(2, 1, 8);
	k4.addEdge(2, 3, 5);
	k4.addEdge(3, 2, 5);
	k4.addEdge(3, 0, 9);
	k4.addEdge(0, 3, 9);
	k4.addEdge(0, 2, 1);
	k4.addEdge(2, 0, 1);
	k4.addEdge(1, 3, 4);
	k4.addEdge(3, 1, 4);

	assertEquals(Dijkstra.findLength(k4, 0, 3), 6, 0.01);
    }
}
