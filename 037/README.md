##Sort Array by Frequency

*Problem Statement*: Accept an array and sort its elements by descending order of frequency of their occurence.

The steps of the algorithm are as follows:

1: Create a HashMap of all unique elements in the array and their frequency.

2: Create a reverse HashMap with count values as keys and the elements that occur `count` number of times as values.

3: Create a max heap with all the distinct count values.

4: For each element `count` in the max heap,  
4.1: Add to the `result` array `count` copies of every element that occurs `count` times. This can be found from the reverse HashMap.

Time Complexity: **O(n log n)**, Space Complexity: **O(n)**
