import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Arrays;

public class SortFrequency
{
    public static Comparable[] sort(Comparable[] a)
    {
	// 1: Maintain HashMap of all unique elts and their counts
	HashMap<Comparable, Integer> eltMap = new HashMap<Comparable, Integer>();

	for(Comparable c : a)
	{
	    if(eltMap.containsKey(c))
		eltMap.put(c, eltMap.get(c)+1);
	    else
		eltMap.put(c, 1);
	}

	// 2: Maintain a reverse-hashmap of counts and arrays with all elts having particular count,
	// Simultaneously, maintain a max heap of all possible counts

	HashMap<Integer, ArrayList<Comparable>> countMap = new HashMap<Integer, ArrayList<Comparable>>();
	PriorityQueue<Integer> countQueue = new PriorityQueue<Integer>(10, Collections.reverseOrder());

	for(Comparable c : eltMap.keySet())
	{
	    int currentCount = eltMap.get(c);
	    if(!countMap.containsKey(currentCount))
		countMap.put(currentCount, new ArrayList<Comparable>());
	    ArrayList<Comparable> eltList = (ArrayList)countMap.get(currentCount);
	    eltList.add(c);

	    // (ArrayList<Comparable>)(eltMap.get(currentCount)).add(c);

	    if(!countQueue.contains(currentCount))
		countQueue.add(currentCount);
	}

	// 3: Remove elts from the max heap. For each of them,
	// put in the result array 'count' instances of all elts with that count.
	Comparable[] result = new Comparable[a.length];
	int respointer = 0;

	while(countQueue.size() != 0)
	{
	    int currentCount = countQueue.remove();
	    ArrayList<Comparable> eltList = countMap.remove(currentCount);

	    for(Comparable c : eltList)
		for(int i=0; i<currentCount; i++)
		    result[respointer++] = c;
	}

	return result;
    }

    public static void main(String[] ar)
    {
	Integer[] test = new Integer[] {2, 3, 6, 6, 2, 6, 3, 3, 3, 1, 4};
	System.out.println(Arrays.toString(SortFrequency.sort(test)));
    }
}
