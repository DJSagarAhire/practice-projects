import static org.junit.Assert.*;
import org.junit.Test;

public class TestSortFrequency
{
    @Test
    public void normalTest()
    {
	Integer[] test = new Integer[] {2, 3, 6, 6, 2, 6, 3, 3, 3, 1, 4};
	assertArrayEquals(new Integer[]{3, 3, 3, 3, 6, 6, 6, 2, 2, 1, 4}, SortFrequency.sort(test));
    }

    @Test
    public void singleElement()
    {
	Integer[] test = new Integer[] {3};
	assertArrayEquals(new Integer[] {3}, SortFrequency.sort(test));
    }

    @Test
    public void repeatedSingleElement()
    {
	Integer[] test = new Integer[] {3,3,3,3};
	assertArrayEquals(new Integer[] {3,3,3,3}, SortFrequency.sort(test));
    }

    @Test
    public void sameTopCounts()
    {
	Integer[] test = new Integer[] {3,4,3,4,3,4,3,4};
	assertArrayEquals(new Integer[] {3,3,3,3,4,4,4,4}, SortFrequency.sort(test));
    }
}
