public class Median
{
    public static Comparable find(Comparable[] a)
    {
	return quickSelect(a, 0, a.length-1, (a.length-1)/2);
    }

    private static Comparable quickSelect(Comparable[] a, int low, int high, int location)
    {
	if(low == high && low == location)
	    return a[low];

	int pivotLocation = (int)(Math.random() * (high-low)) + low;
	Comparable pivot = a[pivotLocation];

	a[pivotLocation] = a[low];
	a[low] = pivot;

	int i=low+1, j=high;

	while(true)
	{
	    for(; a[i].compareTo(pivot) < 0; i++)
		if(i == high)
		    break;

	    for(; a[j].compareTo(pivot) > 0; j--)
		if(j == low)
		    break;

	    if(i >= j)
		break;

	    Comparable temp = a[i];
	    a[i] = a[j];
	    a[j] = temp;
	}

	a[low] = a[j];
	a[j] = pivot;

	if(j == location)
	    return a[j];
	else if(j > location)
	    return quickSelect(a, low, j, location);
	else
	    return quickSelect(a, j, high, location);
    }

    public static void main(String[] ar)
    {
	System.out.println(Median.find(new Integer[] {4,2,5,1,3}));
    }
}
