##Find Median of an Array

*Problem Statement*: Accept an array and find its median.

The algorithm used is a modified version of Quicksort called Quickselect. The array is partitioned as in quicksort, but instead of recursing on both partitions, as in Quicksort, recursion is performed only on the partition which contains the element of the desired rank in the array. This makes the algorithm O(n) on an average but O(n^2) in the worst case. Strategies used for mitigating worst-case probability in quicksort can also be used here.

Time Complexity: **O(n)**, Space Complexity: **O(1)** except for call stack
