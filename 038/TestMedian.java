import static org.junit.Assert.*;
import org.junit.Test;

public class TestMedian
{
    @Test
    public void evenLength()
    {
	assertEquals(Median.find(new Integer[] {2, 3, 6, 1, 4, 5}), 3);
    }

    @Test
    public void oddLength()
    {
	assertEquals(Median.find(new Integer[] {4, 3, 2, 5, 1}), 3);
    }

    @Test
    public void alreadySorted()
    {
	assertEquals(Median.find(new Integer[] {1, 2, 3, 4, 5}), 3);
    }

    @Test
    public void reverseSorted()
    {
	assertEquals(Median.find(new Integer[] {5, 4, 3, 2, 1}), 3);
    }

    @Test
    public void singleElement()
    {
	assertEquals(Median.find(new Integer[] {5}), 5);
    }
}
