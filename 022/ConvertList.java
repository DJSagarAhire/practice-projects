public class ConvertList
{
    public static MySkeletalDoublyLinkedList convert(MySkeletalLinkedList sl)
    {
	MySkeletalDoublyLinkedList dl = new MySkeletalDoublyLinkedList();
	dl.head = dl.new Node();

	if(sl.head == null)
	{
	    dl.head = null;
	    dl.tail = null;
	    return dl;
	}
	else
	{
	    dl.head.item = sl.head.item;
	    dl.head.prev = null;
	}

	MySkeletalDoublyLinkedList.Node prev = dl.head;
	MySkeletalDoublyLinkedList.Node curr = null;
	MySkeletalLinkedList.Node scurr = sl.head.next;

	while(scurr != null)
	{
	    curr = dl.new Node();
	    curr.item = scurr.item;
	    curr.prev = prev;
	    curr.prev.next = curr;

	    scurr = scurr.next;
	    prev = curr;
	}

	if(sl.head.next == null)
	    dl.tail = dl.head;
	else
	    dl.tail = curr;

	return dl;
    }

    public static void main(String[] ar)
    {
	MySkeletalLinkedList<Integer> sl = new MySkeletalLinkedList<Integer>();
	sl.createList(new Integer[]{1,2,3,4,5});
	MySkeletalDoublyLinkedList<Integer> dl = new MySkeletalDoublyLinkedList<Integer>();
	dl = convert(sl);
	dl.printList();
	dl.printListBackwards();
    }
}
