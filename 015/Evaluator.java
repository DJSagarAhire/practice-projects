/**
   Implements an arithmetic expression evaluator using Dijkstra's 2-stack algorithm.
   Requires MyStack.class
   @author Sagar Ahire
 */

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Evaluator
{
    enum Operator
    {
	ADD, SUB, MUL, DIV, PAREN
    }

    MyStack<Operator> operatorStack;
    MyStack<Double> operandStack;

    /**
       Accepts an arithmetic expression and evaluates it using the algorithm.
       The expression can only consist of +, -, *, /, (, ), spaces and numbers (integers and decimals both supported.).
       @param exp The expression as a string.
       @return The result of the evaluation.
     */
    public double evaluate(String exp)
    {
	operatorStack = new MyStack<Operator>();
	operandStack = new MyStack<Double>();

	Pattern pattern = Pattern.compile("\\d+\\.?\\d*");

	exp = exp.trim();

	while(!exp.equals(""))
	{
	    if(exp.startsWith("("))
	    {
		operatorStack.push(Operator.PAREN);
		exp = exp.substring(1).trim();
	    }

	    else if(exp.startsWith("+"))
	    {
		// If there is a * or / on the top, pop it first and perform the operation
		while(!operatorStack.isEmpty() && (operatorStack.peek() == Operator.MUL || operatorStack.peek() == Operator.DIV))
		{
		    double num = performOperation(operatorStack.pop(), operandStack.pop(), operandStack.pop());
		    operandStack.push(num);
		}

		operatorStack.push(Operator.ADD);
		exp = exp.substring(1).trim();
	    }

	    else if(exp.startsWith("-"))
	    {
		// If there is a * or / on the top, pop it first and perform the operation
		while(!operatorStack.isEmpty() && (operatorStack.peek() == Operator.MUL || operatorStack.peek() == Operator.DIV))
		{
		    double num = performOperation(operatorStack.pop(), operandStack.pop(), operandStack.pop());
		    operandStack.push(num);
		}

		operatorStack.push(Operator.SUB);
		exp = exp.substring(1).trim();
	    }

	    else if(exp.startsWith("*"))
	    {
		operatorStack.push(Operator.MUL);
		exp = exp.substring(1).trim();
	    }

	    else if(exp.startsWith("/"))
	    {
		operatorStack.push(Operator.DIV);
		exp = exp.substring(1).trim();
	    }

	    else if(exp.startsWith(")"))
	    {
		while(operatorStack.peek() != Operator.PAREN)
		{
		    double num = performOperation(operatorStack.pop(), operandStack.pop(), operandStack.pop());
		    operandStack.push(num);
		}
		operatorStack.pop();    // Pop and discard the PAREN
		exp = exp.substring(1).trim();
	    }

	    else
	    {
		Matcher matcher = pattern.matcher(exp);
		if(matcher.lookingAt() && matcher.start() == 0)
		{
		    double num = Double.parseDouble(matcher.group());
		    operandStack.push(num);
		    exp = exp.substring(matcher.end()).trim();
		}
		else
		    throw new IllegalArgumentException("Invalid expression encountered: " + exp);
	    }
	}

	while(operandStack.size() != 1)
	{
	    Operator op = operatorStack.pop();

	    if(op == Operator.PAREN)
		throw new IllegalArgumentException("Mismatched brackets in expression");

	    double num = performOperation(op, operandStack.pop(), operandStack.pop());
	    operandStack.push(num);
	}

	return (operandStack.pop());
    }

    /**
       Accepts an Operator and 2 numbers and performs the specified operation on them.
       The number to be popped first is assumed to be num1.
       @param num1 The first popped number
       @param num2 The second popped number
       @return The result of the operation
     */
    private double performOperation(Operator op, double num1, double num2)
    {
	if(op == Operator.ADD)
	    return (num2 + num1);
	if(op == Operator.SUB)
	    return (num2 - num1);
	if(op == Operator.MUL)
	    return (num2 * num1);
	if(op == Operator.DIV)
	    return (num2 / num1);
	else
	    throw new IllegalArgumentException("Invalid operator encountered: " + op);
    }

    public static void main(String[] ar)
    {
	Evaluator test = new Evaluator();
	System.out.println("2+5 = " + test.evaluate("2+5"));
	System.out.println("2+5.25 = " + test.evaluate("2+5.25"));
	System.out.println("(2+5)*3 = " + test.evaluate("(2+5)*3"));
	System.out.println("(2+5)*(4-1) = " + test.evaluate("(2+5)*(4-1)"));
	System.out.println("(2+4)/3 = " + test.evaluate("(2+4)/3"));
	System.out.println("(2+4.2)/3.5 = " + test.evaluate("(2+4.2)/3.5"));
	System.out.println("3+6/3 = " + test.evaluate("3+6/3"));
	System.out.println("6/3+3 = " + test.evaluate("6/3+3"));
	System.out.println("10 + 2 * 6 = " + test.evaluate("10 + 2 * 6"));
	System.out.println("100 * 2 + 12 = " + test.evaluate("100 * 2 + 12"));
	System.out.println("100 * ( 2 + 12 ) = " + test.evaluate("100 * ( 2 + 12 )"));
	System.out.println("100 * ( 2 + 12 ) / 14 = " + test.evaluate("100 * ( 2 + 12 ) / 14"));
    }
}
