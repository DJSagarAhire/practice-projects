##Dijkstra's 2-stack Arithmetic Expression Evaluation

*Problem Statement*: Use Dijkstra's 2-stack Expression Evaluation algorithm to evaluate an input arithmetic expression.

This is a variation of the [Shunting Yard Algorithm](http://en.wikipedia.org/wiki/Shunting-yard_algorithm) which, instead of converting the infix expression to postfix, evaluates the infix expression directly.

The algorithm is as follows:

1: Accept a string `exp`, containing an expression to evaluate.

2: Create 2 empty stacks: an `operatorStack` for storing the operators and an `operandStack` for storing the operands.

3: Depending on the start of `exp`, do the following:

3.1: If `exp` starts with "("  
3.1.1: Push it on `operatorStack` and remove it from `exp`.

3.2: If `exp` starts with "+"  
3.2.1: As long as the top of `operatorStack` contains '*' or '/',  
3.2.1.1: Pop the operator and 2 operands and perform the operation.  
3.2.1.2: Push the result of the operation back on `operandStack`.  
3.2.2: Push '+' on `operatorStack` and remove it from `exp`.

3.3: If `exp` starts with "-"  
Perform same steps as 3.2 for '-'.

3.4: If `exp` starts with "*"  
3.4.1: Push '*' on `operatorStack` and remove it from `exp`.

3.5: If `exp` starts wih "/"  
3.5.1: Push '/' on `operatorStack` and remove it from `exp`.

3.6: If `exp` starts with ")"  
3.6.1: Until the top of `operatorStack` contains '(',  
3.6.1.1: Pop the operator and 2 operands and perform the operation.  
3.6.1.2: Push the result of the operation back on `operandStack`.  
3.6.2: Remove the bracket from `exp`.

3.7: If `exp` starts with a number `num`  
3.7.1: Push `num` on `operandStack` and remove it from `exp`.

3.8: If `exp` ends with anything else, report an `IllegalArgumentException`.

4: Repeat Step 3 until `exp` is empty.

5: While the size of `operandStack` is greater than 1, do the following:

5.1: Pop an operator from the `operatorStack`.  
5.2: If it is a '(', throw an `IllegalArgumentException`.  
5.3: Else, pop 2 operands and perform the operation, and push the result back on `operandStack`.

6: Return the top of `operandStack` as the final output.
