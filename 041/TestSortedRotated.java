import static org.junit.Assert.*;
import org.junit.Test;

public class TestSortedRotated
{
    @Test
    public void normalOddLength()
    {
	Integer[] test = new Integer[] {3, 6, 7, 10, 13, -2, 0};
	assertEquals(SortedRotated.find(test, -2), 5);
    }

    @Test
    public void normalEvenLength()
    {
	Integer[] test = new Integer[] {3, 6, 7, 10, 13, -2};
	assertEquals(SortedRotated.find(test, 7), 2);
    }

    @Test
    public void firstElement()
    {
	Integer[] test = new Integer[] {3, 6, 7, 10, 13, -2};
	assertEquals(SortedRotated.find(test, 3), 0);
    }

    @Test
    public void lastElement()
    {
	Integer[] test = new Integer[] {3, 6, 7, 10, 13, -2};
	assertEquals(SortedRotated.find(test, -2), 5);
    }

    @Test
    public void unrotated()
    {
	Integer[] test = new Integer[] {2, 3, 6, 9, 10, 14, 16, 22};
	assertEquals(SortedRotated.find(test, 9), 3);
    }

    @Test
    public void singleElement()
    {
	Integer[] test = new Integer[] {6};
	assertEquals(SortedRotated.find(test, 6), 0);
    }

    @Test
    public void doesNotExist()
    {
	Integer[] test = new Integer[] {3, 6, 7, 10, 13, -2};
	assertEquals(SortedRotated.find(test, 8), -1);
    }

    @Test
    public void doesNotExistUnrotated()
    {
	Integer[] test = new Integer[] {2, 3, 6, 9, 10, 14, 16, 22};
	assertEquals(SortedRotated.find(test, 17), -1);
    }
}
