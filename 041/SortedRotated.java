public class SortedRotated
{
    public static int find(Comparable[] a, Comparable elt)
    {
	int start = findStart(a, 0, a.length-1);
	return findElement(a, 0, a.length-1, start, elt);
    }

    private static int findStart(Comparable[] a, int low, int high)
    {
	if(low == high)
	    return low;

	int mid = (low + high) / 2;
	if(a[low].compareTo(a[mid]) < 0)
	    return findStart(a, mid+1, high);
	else
	    return findStart(a, low, mid);
    }

    private static int findElement(Comparable[] a, int low, int high, int start, Comparable elt)
    {
	if(low > high)
	    return -1;

	int mid = (low + high) / 2;
	int realLow = (low + start) % a.length;
	int realMid = (mid + start) % a.length;
	int realHigh = (high + start) % a.length;

	if(a[realMid].compareTo(elt) == 0)
	    return realMid;
	else if(a[realMid].compareTo(elt) < 0)
	    return findElement(a, mid+1, high, start, elt);
	else
	    return findElement(a, low, mid-1, start, elt);
    }

    public static void main(String[] ar)
    {
	System.out.println(find(new Integer[] {3, 4, 5, 7, 9, 0, 1}, 4));
	System.out.println(find(new Integer[] {0, 1, 3, 4, 5, 7, 9}, 4));
    }
}
