##Find Element in Sorted, Rotated Array

*Problem Statement*: Given an array which is sorted, but has its elements rotated cyclically by a certain distance, find the location of an element.

This is achieved by running 2 modified binary searches:

1: To find the position where the sorted array starts, i.e. the location of the minimum element in the array.  
2: To find a particular element, taking into account the rotation and the starting position of the same.

Time Complexity: **O(long n)**, Space Complexity: **O(1)**
