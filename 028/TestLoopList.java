import org.junit.Test;
import static org.junit.Assert.*;

public class TestLoopList
{
    @Test
    public void cycleHash()
    {
	LoopList test = new LoopList();

	MySkeletalLinkedList<Integer> l = new MySkeletalLinkedList<Integer>();
	l.createList(new Integer[] {1,2,3,4,5});
	l.head.next.next.next.next.next = l.head.next.next;

	MySkeletalLinkedList.Node res = test.detectLoopHashmap(l);
	assertEquals((int) res.item, 3);
    }

    @Test
    public void cycleFloyd()
    {
	LoopList test = new LoopList();

	MySkeletalLinkedList<Integer> l = new MySkeletalLinkedList<Integer>();
	l.createList(new Integer[] {1,2,3,4,5});
	l.head.next.next.next.next.next = l.head.next.next;

	MySkeletalLinkedList.Node res = test.detectLoopFloyd(l);
	assertEquals((int) res.item, 3);
    }

    @Test
    public void emptyHash()
    {
	LoopList test = new LoopList();

	MySkeletalLinkedList<Integer> l = new MySkeletalLinkedList<Integer>();
	l.head = null;

	MySkeletalLinkedList.Node res = test.detectLoopHashmap(l);
	assertNull(res);
    }

    @Test
    public void emptyFloyd()
    {
	LoopList test = new LoopList();

	MySkeletalLinkedList<Integer> l = new MySkeletalLinkedList<Integer>();
	l.head = null;

	MySkeletalLinkedList.Node res = test.detectLoopFloyd(l);
	assertNull(res);
    }

    @Test
    public void fullCircularHash()
    {
	LoopList test = new LoopList();

	MySkeletalLinkedList<Integer> l = new MySkeletalLinkedList<Integer>();
	l.createList(new Integer[] {1,2,3,4,5});
	l.head.next.next.next.next.next = l.head;

	MySkeletalLinkedList.Node res = test.detectLoopHashmap(l);
	assertEquals((int) res.item, 1);
    }

    @Test
    public void fullCircularFloyd()
    {
	LoopList test = new LoopList();

	MySkeletalLinkedList<Integer> l = new MySkeletalLinkedList<Integer>();
	l.createList(new Integer[] {1,2,3,4,5});
	l.head.next.next.next.next.next = l.head;

	MySkeletalLinkedList.Node res = test.detectLoopFloyd(l);
	assertEquals((int) res.item, 1);
    }

    @Test
    public void singleElementCycleHash()
    {
	LoopList test = new LoopList();

	MySkeletalLinkedList<Integer> l = new MySkeletalLinkedList<Integer>();
	l.createList(new Integer[] {1});
	l.head.next = l.head;

	MySkeletalLinkedList.Node res = test.detectLoopHashmap(l);
	assertEquals((int) res.item, 1);
    }

    @Test
    public void singleElementCycleFloyd()
    {
	LoopList test = new LoopList();

	MySkeletalLinkedList<Integer> l = new MySkeletalLinkedList<Integer>();
	l.createList(new Integer[] {1});
	l.head.next = l.head;

	MySkeletalLinkedList.Node res = test.detectLoopFloyd(l);
	assertEquals((int) res.item, 1);
    }

    @Test
    public void noCycleHash()
    {
	LoopList test = new LoopList();

	MySkeletalLinkedList<Integer> l = new MySkeletalLinkedList<Integer>();
	l.createList(new Integer[] {1,2,3,4,5});

	MySkeletalLinkedList.Node res = test.detectLoopHashmap(l);
	assertNull(res);
    }

    @Test
    public void noCycleFloyd()
    {
	LoopList test = new LoopList();

	MySkeletalLinkedList<Integer> l = new MySkeletalLinkedList<Integer>();
	l.createList(new Integer[] {1,2,3,4,5});

	MySkeletalLinkedList.Node res = test.detectLoopFloyd(l);
	assertNull(res);
    }
}
