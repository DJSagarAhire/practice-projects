import java.util.HashMap;

public class LoopList
{
    public MySkeletalLinkedList.Node detectLoopHashmap(MySkeletalLinkedList l)
    {
	HashMap<MySkeletalLinkedList.Node, Boolean> hash = new HashMap<MySkeletalLinkedList.Node, Boolean>();

	for(MySkeletalLinkedList.Node n=l.head; n != null; n=n.next)
	{
	    if(hash.containsKey(n))
		return n;
	    else
		hash.put(n, true);
	}

	return null;
    }

    public MySkeletalLinkedList.Node detectLoopFloyd(MySkeletalLinkedList l)
    {
	// 1: Run Floyd's algorithm
	MySkeletalLinkedList.Node slow = l.head;
	MySkeletalLinkedList.Node fast = l.head;

	while(true)
	{
	    if(fast == null || fast.next == null || fast.next.next == null)   // Either current node or one of next 2 nodes is null. No cycle exists.
		return null;

	    slow = slow.next;
	    fast = fast.next.next;

	    if(slow == fast)    // Cycle found
		break;
	}

	// 2: Find loop length
	slow = slow.next;
	int loopLength = 1;

	while(slow != fast)
	{
	    slow = slow.next;
	    loopLength++;
	}

	// 3: Traverse loopLength nodes with first pointer
	MySkeletalLinkedList.Node first = l.head;
	MySkeletalLinkedList.Node second = l.head;

	for(int i=0; i<loopLength; i++, first = first.next);

	// 4: Traverse first and second pointers together till they meet
	for(; first != second; first = first.next, second = second.next);

	return first;
    }
}
