##Detect Loop in Linked List

*Problem Statement*: Write a function which accepts a linked list and detects if it has a cycle in it. If yes, it returns the point at which the cycle occurs.

Two implementations are given, similar to #027: `detectLoopHashmap()` using a Hashmap, and `detectLoopFloyd()` using [Floyd's Cycle Detection Algorithm](http://en.wikipedia.org/wiki/Cycle_detection#Tortoise_and_hare), modified to output the node at which the cycle occurs.

###Hashing-based Implementation

The following is the algorithm for the hashing-based implementation:

1: Initialize a node pointer `n` to point to the head of the list.  
2: Check if `n` is already in the hashmap. If yes, return `n` as the point of the cycle.  
3: If not, move to the next node and repeat until the list is exhausted.  
4: If `n` reaches `null`, return failure (the list has no cycle).

Time complexity: **O(n)**, Space complexity: **O(n)**  
Traversals required: 1

##Floyd-based Implementation

The following is the algorithm for the implementation based on Floyd's algorithm:

1: Initialize 2 pointers `fast` and `slow` to the head of the list.  
2: Increment `fast` in 2 steps and `slow` in 1 step per iteration until either `fast` encounters `null` or `fast` and `slow` are equal.  
3: If `fast` encounters `null`, return failure (the list has no cycle).  
4: Else, move the `slow` pointer forward again till it meets the fast pointer (we are now in the cycle, the pointers are guaranteed to meet again). Measure the number of steps it takes for this to happen, this is the `loopLength`.  
5: Initialize 2 pointers `first` and `second` to the head of the list.  
6: Move `first` by `loopLength` nodes.  
7: Then move `first` and `second` together one node at a time until they both meet. The node at which they meet is the point at which the cycle occurs.

Time complexity: **O(n)**, Space complexity: **O(1)**  
Traversals required: 5
