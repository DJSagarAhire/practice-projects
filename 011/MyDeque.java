/**
   Provides an array-based implementation of a Double-Ended Queue (Deque).
   Operations supported:
   1) enqueueFront
   2) enqueueBack
   3) dequeueFront
   4) dequeueBack
   5) peekFront
   6) peekBack
   7) isEmpty
   8) size
 */
public class MyDeque<Item>
{
    private Item[] d = (Item[]) new Object[4];
    private int front = 2;  // Next insertion at front will be at 1
    private int back = 1;   // Next insertion at back will be at 2
    // For an empty deque back < front (indices have overlapped).

    private void resize(int size)
    {
	Item[] temp = (Item[]) new Object[size];

	int frontOffset = d.length/2 - front;   // The number of indices the front is to the left of the center
	int backOffset = d.length/2 - back;     // The number of indices the back is to the left of the center (typically negative)

	for(int i=frontOffset; i >= backOffset; i--)
	    temp[temp.length/2 - i] = d[d.length/2 - i];

	front = temp.length/2 - frontOffset;
	back = temp.length/2 - backOffset;

	d = temp;
    }

    /**
       Adds an element at the front of the deque.
       @param i Item to be added.
     */
    public void enqueueFront(Item i)
    {
	if(front == 0)   // Deque is full at the front
	    resize(d.length * 2);
	d[--front] = i;
    }

    /**
       Adds an element at the back of the deque.
       @param i Item to be added.
     */
    public void enqueueBack(Item i)
    {
	if(back == d.length-1)   // Deque is full at the back
	    resize(d.length * 2);
	d[++back] = i;
    }

    /**
       Removes an element from the front of the deque.
       @return The item at the front of the deque.
     */
    public Item dequeueFront()
    {
	if(isEmpty())
	    throw new java.util.NoSuchElementException("Attempt to remove element from empty deque");
	if(front > d.length * 3/8 && back < d.length * 5/8)   // Deque has shrunk
	    resize(d.length / 2);
	Item i = d[front];
	d[front++] = null;

	return i;
    }

    /**
       Removes an element from the back of the deque.
       @return The item at the back of the deque.
     */
    public Item dequeueBack()
    {
	if(isEmpty())
	    throw new java.util.NoSuchElementException("Attempt to remove element from empty deque");
	if(front > d.length * 3/8 && back < d.length * 5/8)   // Deque has shrunk
	    resize(d.length / 2);
	Item i = d[back];
	d[back--] = null;
	return i;
    }

    /**
       Returns the item at the front of the deque but does not remove it.
       @return The item at the front of the deque.
     */
    public Item peekFront()
    {
	if(isEmpty())
	    throw new java.util.NoSuchElementException("Attempt to remove element from empty deque");
	return d[front];
    }

    /**
       Returns the item at the back of the deque but does not remove it.
       @return The item at the back of the deque.
     */
    public Item peekBack()
    {
	if(isEmpty())
	    throw new java.util.NoSuchElementException("Attempt to remove element from empty deque");
	return d[back];
    }

    /**
       Checks if the deque is empty.
       @return true if the deque is empty, false otherwise.
     */
    public boolean isEmpty()
    {
	return (back < front);
    }

    /**
       Returns the number of elements in the deque.
       @return Integer indicating number of elements in the deque.
     */
    public int size()
    {
	return (back - front + 1);
    }

    public static void main(String[] ar)
    {
	MyDeque test = new MyDeque();

	System.out.println("Enqueue 1 at back.");
	test.enqueueBack(1);

	System.out.println("Enqueue 2 at front.");
	test.enqueueFront(2);

	System.out.println("Enqueue 3 at back.");
	test.enqueueBack(3);

	System.out.println("Size: " + test.size());

	System.out.println("Dequeue from back: " + test.dequeueBack());

	System.out.println("Peek from back: " + test.peekBack());

	System.out.println("Dequeue from back: " + test.dequeueBack());

	System.out.println("Size: " + test.size());

	System.out.println("Enqueue 4 at back.");
	test.enqueueBack(4);

	System.out.println("Enqueue 5 at back.");
	test.enqueueBack(5);

	System.out.println("Dequeue from front: " + test.dequeueFront());

	System.out.println("Dequeue from front: " + test.dequeueFront());

	System.out.println("Dequeue from back: " + test.dequeueBack());
    }
}
