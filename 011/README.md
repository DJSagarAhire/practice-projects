##Array-based Deque Implementation

This is an implementation of a Deque (Double-Ended Queue) using an underlying array. The operations supported are:

* EnqueueFront  
* DequeueFront  
* EnqueueBack  
* DequeueBack  
* Is Empty  
* Size  
* PeekFront  
* PeekBack
