/**
   Provides implementation to compute the nth Catalan number
   @author Sagar Ahire
 */
public class Catalan
{
    /**
       Generates the nth Catalan number by providing a method using dynamic programming on Catalan formula
       C(n) = sum(i=0 to n-1)[C(i)*C(n-i-1)]
    */
    public long dynamicCatalan(int num)
    {
	if(num < 0)
	    throw new IllegalArgumentException("Invalid num: " + num);

	long[] c = new long[num+1];
	c[0] = 1;

	// Fill up the array ('table') up to 'num'th value
	for(int n=1; n<=num; n++)
	{
	    c[n] = 0;
	    for(int i=0; i<n; i++)
		c[n] += (c[i] * c[n-i-1]);
	}

	return c[num];
    }

    /**
       Generates the nth Catalan number by providing a method using the product formula
       C(n) = product(k=2 to n) [(n+k)/k]
    */
    public long productCatalan(int num)
    {
	if(num < 0)
	    throw new IllegalArgumentException("Invalid num: " + num);

	double c = 1;
	for(double k=2; k<=num; k++)
	    c *= (num+k) / k;
	return (long)c;
    }

    /**
       Driver method for testing
     */
    public static void main(String[] ar)
    {
	int num;
	if(ar.length == 1)
	    num = Integer.parseInt(ar[0]);
	else
	    num = 8;

	Catalan test = new Catalan();
	System.out.println("Catalan number " + num + " using dynamic programming is " + test.dynamicCatalan(num));
	System.out.println("Catalan number " + num + " using product is " + test.productCatalan(num));
    }
}
