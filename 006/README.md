##006: Catalan Numbers

*Problem Statement*: Compute the `n`th Catalan number.

[Catalan Numbers](http://en.wikipedia.org/wiki/Catalan_number) are very important in various combinatorial applications. For example, the nth Catalan number is the number of ways n pairs of parentheses can be arranged in a balanced way.

###Dynamic Programming Implementation

This implementation uses the following formula:

![C(n) = sum(i=0 to n-1)[C(i)*C(n-i-1)]](http://quicklatex.com/cache3/ql_200a4685580b76cc769a8ab2954100a8_l3.png)

However, instead of a flat-out recursive implementation, which would be woefully inefficient, the program uses dynamic programming instead, to remember the computations that were performed previously.

The algorithm used is as follows:

1: Accept `num`. If it is negative, throw an `IllegalArgumentException`.

2: Create an array `c` with size `num`+1. Initialize it as `c[0]` = 1 and everything else = 0.

3: For `n` = 1 to `num`, perform the following. This will compute the `n`th Catalan number.

3.1: For `i` = 0 to `n`, `c[n] = c[n] + (c[i] * c[n-i-1])`.

4: Return `c[num]`.

This implementation actually computes *all* the Catalan numbers upto `num` as they are all required for the computation due to the recursive formula. As a result, the time complexity is **O(n^2)**. While this method naturally lends itself to problems that require computing of a sequence of Catalan numbers, it is possible to be more efficient if only a single number is required.

###Product Formula Implementation

This implementation uses the following formula:

![C(n) = product(k=2 to n)[(n+k)/k]](http://quicklatex.com/cache3/ql_a23d3852b8ab75f74cb372028e1372af_l3.png)

This formula does not require computation of all Catalan numbers upto `n` and thus proves to be much more efficient.

The algorithm is as follows:

1: Accept `num`. If it is negative, throw an `IllegalArgumentException`.

2: Initialize a double-type variable `c` = 0.

3: For `k` = 2 to `num`, `c = c * (num+k) / k`.

4: Return `c`.

Since this solution requires only one loop, its time complexity is **O(n)**. Moreover, since no previous Catalan numbers need to be stored, the space requirement is **O(1)** rather than the O(n) of the dynamic programming method.
