##Diameter of Binary Tree

*Problem Statement*: Find the diameter of a binary tree.

The 'diameter' of a binary tree is the length of the longest path in it. Consider the following:

![Example of Diameter](http://geeksforgeeks.org/wp-content/uploads/tree_diameter.GIF)

The diameter of a tree can be found recursively as the maximum of the following:

1: The diameter of the left subtree  
2: The diameter of the right subtree  
3: The length of the longest path in the current tree passing through the root, which is the height of the left subtree + the height of the right subtree.

Time Complexity: O(n) as each node is examined only twice - once for calculating the diameter and once for calculating the height.
