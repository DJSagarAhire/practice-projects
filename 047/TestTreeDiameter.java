import static org.junit.Assert.*;
import org.junit.Test;

public class TestTreeDiameter
{
    @Test
    public void notThroughRoot()
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(8);
	BinaryTree.Node n1 = t.insertNode(t.root, 3, true);
	BinaryTree.Node n2 = t.insertNode(t.root, 10, false);
	BinaryTree.Node n3 = t.insertNode(n1, 2, true);
	BinaryTree.Node n4 = t.insertNode(n3, 1, true);
	BinaryTree.Node n5 = t.insertNode(n4, 0, true);
	BinaryTree.Node n6 = t.insertNode(n1, 6, false);
	BinaryTree.Node n7 = t.insertNode(n6, 5, true);
	BinaryTree.Node n8 = t.insertNode(n6, 7, false);
	BinaryTree.Node n9 = t.insertNode(n8, 3, false);

	assertEquals(TreeDiameter.diameter(t), 6);
    }

    @Test
    public void throughRoot()
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(8);
	BinaryTree.Node n1 = t.insertNode(t.root, 3, true);
	BinaryTree.Node n2 = t.insertNode(t.root, 10, false);
	BinaryTree.Node n3 = t.insertNode(n1, 2, true);
	BinaryTree.Node n6 = t.insertNode(n1, 6, false);
	BinaryTree.Node n7 = t.insertNode(n6, 5, true);
	BinaryTree.Node n8 = t.insertNode(n6, 7, false);
	BinaryTree.Node n9 = t.insertNode(n8, 3, false);

	assertEquals(TreeDiameter.diameter(t), 5);
    }

    @Test
    public void completeBinaryTree()
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(4);
	BinaryTree.Node n2 = t.insertNode(t.root, 2, true);
	BinaryTree.Node n1 = t.insertNode(n2, 1, true);
	BinaryTree.Node n3 = t.insertNode(n2, 3, false);
	BinaryTree.Node n6 = t.insertNode(t.root, 6, false);
	BinaryTree.Node n5 = t.insertNode(n6, 5, true);
	BinaryTree.Node n7 = t.insertNode(n6, 7, false);

	assertEquals(TreeDiameter.diameter(t), 4);
    }

    @Test
    public void linearBinaryTree()
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(1);
	BinaryTree.Node n2 = t.insertNode(t.root, 2, false);
	BinaryTree.Node n3 = t.insertNode(n2, 3, false);
	BinaryTree.Node n4 = t.insertNode(n3, 4, false);
	BinaryTree.Node n5 = t.insertNode(n4, 5, false);

	assertEquals(TreeDiameter.diameter(t), 4);
    }

    @Test
    public void singleVertex()
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(10);

	assertEquals(TreeDiameter.diameter(t), 0);
    }
}
