/**
   Finds the diameter of a binary tree. Requires BinaryTree.class
 */

public class TreeDiameter
{
    public static int diameter(BinaryTree t)
    {
	return diameterNodes(t.root);
    }

    private static int diameterNodes(BinaryTree.Node tn)
    {
	if(tn == null)
	    return 0;

	int longestPathAtRoot = heightNodes(tn.left) + heightNodes(tn.right);
	return Math.max(diameterNodes(tn.left), Math.max(diameterNodes(tn.right), longestPathAtRoot));
    }

    private static int heightNodes(BinaryTree.Node tn)
    {
	if(tn == null)
	    return 0;
	return 1 + Math.max(heightNodes(tn.left), heightNodes(tn.right));
    }
}
