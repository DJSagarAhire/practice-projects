/**
   Provides an implementation of 2 stacks in a single array.
   @author Sagar Ahire
 */
public class My3Stacks<Item>
{
    private Item[] arr = (Item []) new Object[6];
    private int top1 = -1;
    private int top2 = 1;
    private int top3 = 3;

    /**
       Resizes the array holding the stacks to a specified new size.
       @param newsize The new size of the array.
     */
    private void resize(int newsize)
    {
	Item[] temp = (Item []) new Object[newsize];

	for(int i=0; i <= top1; i++)
	    temp[i] = arr[i];

	for(int i=newsize/3, j=arr.length/3; j <= top2; i++, j++)
	    temp[i] = arr[j];

	for(int i=2*newsize/3, j=2*arr.length/3; j <= top3; i++, j++)
	    temp[i] = arr[j];

	top1 = size1() - 1;
	top2 = newsize/3 + size2() - 1;
	top3 = 2*newsize/3 + size3() - 1;

	arr = temp;
	System.out.println("Resized with new size " + newsize);
    }

    /**
       Pushes an item into Stack 1.
       @param i Item to be pushed.
     */
    public void push1(Item i)
    {
	if(top1 == arr.length/3 - 1)
	    resize(arr.length * 2);
	arr[++top1] = i;
    }

    /**
       Pushes an item into Stack 2.
       @param i Item to be pushed.
     */
    public void push2(Item i)
    {
	if(top2 == 2*arr.length/3 - 1)
	    resize(arr.length * 2);
	arr[++top2] = i;
    }

    /**
       Pushes an item into Stack 3.
       @param i Item to be pushed.
     */
    public void push3(Item i)
    {
	if(top3 == arr.length - 1)
	    resize(arr.length * 2);
	arr[++top3] = i;
    }

    /**
       Pops an item from Stack 1.
       @return Item at the top of stack 1.
     */
    public Item pop1()
    {
	if(isEmpty1())
	    throw new java.util.EmptyStackException();

	Item i = arr[top1];
	arr[top1--] = null;

	if(stacksVacant())
	    resize(arr.length/2);

	return i;
    }

    /**
       Pops an item from Stack 2.
       @return Item at the top of stack 2.
     */
    public Item pop2()
    {
	if(isEmpty2())
	    throw new java.util.EmptyStackException();

	Item i = arr[top2];
	arr[top2--] = null;

	if(stacksVacant())
	    resize(arr.length/2);

	return i;
    }

    /**
       Pops an item from Stack 3.
       @return Item at the top of stack 3.
     */
    public Item pop3()
    {
	if(isEmpty3())
	    throw new java.util.EmptyStackException();

	Item i = arr[top3];
	arr[top3--] = null;

	if(stacksVacant())
	    resize(arr.length/2);

	return i;
    }

    /**
       Returns the item at the top of Stack 1 but does not remove it from the stack.
       @return Item at the top of Stack 1.
     */
    public Item peek1()
    {
	if(isEmpty1())
	    throw new java.util.EmptyStackException();
	return arr[top1];
    }

    /**
       Returns the item at the top of Stack 2 but does not remove it from the stack.
       @return Item at the top of Stack 2.
     */
    public Item peek2()
    {
	if(isEmpty2())
	    throw new java.util.EmptyStackException();
	return arr[top2];
    }

    /**
       Returns the item at the top of Stack 3 but does not remove it from the stack.
       @return Item at the top of Stack 3.
     */
    public Item peek3()
    {
	if(isEmpty3())
	    throw new java.util.EmptyStackException();
	return arr[top3];
    }

    /**
       Returns the number of elements in Stack 1.
       @return Number of elements in Stack 1.
     */
    public int size1()
    {
	return top1 + 1;
    }

    /**
       Returns the number of elements in Stack 2.
       @return Number of elements in Stack 2.
     */
    public int size2()
    {
	return top2 - arr.length/3 + 1;
    }

    /**
       Returns the number of elements in Stack 3.
       @return Number of elements in Stack 3.
     */
    public int size3()
    {
	return top3 - 2*arr.length/3 + 1;
    }

    /**
       Checks if Stack 1 is empty.
       @return true if Stack 1 is empty, false otherwise.
     */
    public boolean isEmpty1()
    {
	return size1() == 0;
    }

    /**
       Checks if Stack 2 is empty.
       @return true if Stack 2 is empty, false otherwise.
     */
    public boolean isEmpty2()
    {
	return size2() == 0;
    }

    /**
       Checks if Stack 3 is empty.
       @return true if Stack 3 is empty, false otherwise.
     */
    public boolean isEmpty3()
    {
	return size3() == 0;
    }

    /**
       Checks if the stacks are underutilized and need to be resized.
       This method returns true if all 3 of the stacks have sizes less than 1/4 of the allocated size.
     */
    private boolean stacksVacant()
    {
	return (size1() < arr.length/12 && size2() < arr.length/12 && size3() < arr.length/12);
    }

    public static void main(String[] ar)
    {
	My3Stacks<Integer> test = new My3Stacks<Integer>();

	System.out.println("Push 1 in Stack 1.");
	test.push1(1);

	System.out.println("Push 2 in Stack 1.");
	test.push1(2);

	System.out.println("Push 3 in Stack 2.");
	test.push2(3);

	System.out.println("Is Stack 2 empty: " + test.isEmpty2());

	System.out.println("Is Stack 3 empty: " + test.isEmpty3());

	System.out.println("Pop Stack 2: " + test.pop2());

	System.out.println("Push 4 in Stack 2.");
	test.push2(4);

	System.out.println("Push 5 in Stack 1.");
	test.push1(5);

	System.out.println("Push 6 in Stack 1.");
	test.push1(6);

	System.out.println("Push 7 in Stack 1.");
	test.push1(7);

	System.out.println("Size of Stack 1: " + test.size1());

	System.out.println("Push 8 in Stack 1.");
	test.push1(8);

	System.out.println("Push 9 in Stack 2.");
	test.push2(9);

	System.out.println("Pop Stack 2: " + test.pop2());

	System.out.println("Pop Stack 1: " + test.pop1());

	System.out.println("Pop Stack 1: " + test.pop1());

	System.out.println("Pop Stack 1: " + test.pop1());

	System.out.println("Pop Stack 1: " + test.pop1());

	System.out.println("Pop Stack 1: " + test.pop1());

	System.out.println("Size of Stack 1: " + test.size1());

	System.out.println("Pop Stack 2: " + test.pop2());

	System.out.println("Push 10 in Stack 3.");
	test.push3(10);

	System.out.println("Push 11 in Stack 3.");
	test.push3(11);

	System.out.println("Push 12 in Stack 3.");
	test.push3(12);

	System.out.println("Push 13 in Stack 3.");
	test.push3(13);

	System.out.println("Push 14 in Stack 3.");
	test.push3(14);

	System.out.println("Pop Stack 1: " + test.pop1());

	System.out.println("Pop Stack 3: " + test.pop3());

	System.out.println("Pop Stack 3: " + test.pop3());
    }
}
