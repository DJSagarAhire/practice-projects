##3 Stacks using 1 Array

*Problem Statement*: Implement 3 stacks using 1 single array.

Here, Stack 1 grows from 0, Stack 2 grows from `size`/3 and Stack 3 grows from 2*`size`/3.
