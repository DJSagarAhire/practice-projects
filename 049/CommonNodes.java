import java.util.ArrayList;
import java.util.Collections;

/**
   Finds the common nodes in 2 binary search trees. Requires BinarySearchTree.class from #043
 */
public class CommonNodes
{
    // Solution 1: Direct Recursion

    public static <Item extends Comparable<Item>> Object[] findCommonRecursive(BinarySearchTree<Item> t1, BinarySearchTree<Item> t2)
    {
	ArrayList<Item> nodes = findCommonRecursiveNodes(t1.root, t2.root);
	Collections.sort(nodes);
	return nodes.toArray();
    }

    private static <Item extends Comparable<Item>> ArrayList<Item> findCommonRecursiveNodes(BinarySearchTree<Item>.Node tn1, BinarySearchTree<Item>.Node tn2)
    {
	ArrayList<Item> currentNodes = new ArrayList<Item>();
	if(tn1 == null || tn2 == null)
	    return currentNodes;

	if(tn1.item.equals(tn2.item))
	{
	    currentNodes.add(tn1.item);
	    currentNodes.addAll(findCommonRecursiveNodes(tn1.left, tn2.left));
	    currentNodes.addAll(findCommonRecursiveNodes(tn1.right, tn2.right));
	    return currentNodes;
	}
	else if(tn1.item.compareTo(tn2.item) > 0)    // tn1.item > tn2.item
	{
	    currentNodes.addAll(findCommonRecursiveNodes(tn1.left, tn2));
	    for(Item item: findCommonRecursiveNodes(tn1, tn2.right))
		if(! currentNodes.contains(item))
		    currentNodes.add(item);
	    return currentNodes;
	}
	else                                         // tn1.item < tn2.item
	{
	    currentNodes.addAll(findCommonRecursiveNodes(tn1.right, tn2));
	    for(Item item: findCommonRecursiveNodes(tn1, tn2.left))
		if(! currentNodes.contains(item))
		    currentNodes.add(item);
	    return currentNodes;
	}
    }

    // Solution 2: Inorder Traversal

    public static <Item extends Comparable<Item>> Object[] findCommonTraversal(BinarySearchTree<Item> t1, BinarySearchTree<Item> t2)
    {
	ArrayList<Item> io1 = inOrderList(t1.root, new ArrayList<Item>());
	ArrayList<Item> io2 = inOrderList(t2.root, new ArrayList<Item>());

	int t1P = 0;
	int t2P = 0;

	ArrayList<Item> common = new ArrayList<Item>();

	while(t1P < io1.size() && t2P < io1.size())
	{
	    Item i1 = io1.get(t1P);
	    Item i2 = io2.get(t2P);

	    if(i1.equals(i2))
	    {
		common.add(i1);
		t1P++;
		t2P++;
	    }
	    else if(i1.compareTo(i2) > 0)     // i1 > i2
		t2P++;
	    else
		t1P++;
	}

	return common.toArray();
    }

    private static <Item extends Comparable<Item>> ArrayList<Item> inOrderList(BinarySearchTree<Item>.Node tn, ArrayList<Item> list)
    {
	if(tn == null)
	    return list;
	ArrayList<Item> newlist = inOrderList(tn.left, list);
	newlist.add(tn.item);
	inOrderList(tn.right, newlist);

	return newlist;
    }

    public static void main(String[] ar)
    {
	BinarySearchTree<Integer> t1 = new BinarySearchTree<Integer>(4);
	t1.insert(6);
	t1.insert(2);
	t1.insert(5);

	BinarySearchTree<Integer> t2 = new BinarySearchTree<Integer>(2);
	t2.insert(3);
	t2.insert(4);
	t2.insert(5);

	System.out.println(java.util.Arrays.toString(findCommonTraversal(t1, t2)));
    }
}
