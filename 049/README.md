##Common Nodes in 2 BSTs

*Problem Statement*: Accept 2 binary search trees and output the common nodes/values present in both.

2 algorithms have been implemented.

###1: Direct Recursive Solution

This algorithm recurses on the BST straight from the root. At each node, the following is performed: (`node1` belongs to Tree 1 and `node2` belongs to Tree 2. The algorithm starts from the root of both nodes).

1: If `node1.value` = `node2.value`,  
Add `node1.value` to a list of common nodes.  
Recursively call the algorithm on:  
1) `node1.left` and `node2.left`  
2) `node1.right` and `node2.right`  

2: If `node1.value` > `node2.value`,  
Recursively call the algorithm on:  
1) `node1.left` and `node2`  
2) `node1` and `node2.right`

3: Otherwise `node1.value` < `node2.value`  
Recursively call the algorithm on:  
1) `node1` and `node2.left`  
2) `node1.right` and `node2`

Combine the lists, remove duplicates and return.

This algorithm performs a maximum of O(h) operations per node (where h is the height of the tree). Extra space required is only for the common list.

###2: Traversal-based Solution

This algorithm performs an inorder traversal of both trees and then finds the common nodes in the traversal. It takes only O(n) time to find the common nodes as *the inorder traversal of a BST is always sorted*.
