import static org.junit.Assert.*;
import org.junit.Test;

public class TestCommonNodes
{
    @Test
    public void sameTreeRecursive()
    {
	BinarySearchTree<Integer> t1 = new BinarySearchTree<Integer>(4);
	t1.insert(2);
	t1.insert(5);

	BinarySearchTree<Integer> t2 = new BinarySearchTree<Integer>(4);
	t2.insert(2);
	t2.insert(5);

	assertArrayEquals(new Integer[] {2, 4, 5}, CommonNodes.findCommonRecursive(t1, t2));
    }

    @Test
    public void singleNodeRecursive()
    {
	BinarySearchTree<Integer> t1 = new BinarySearchTree<Integer>(4);
	BinarySearchTree<Integer> t2 = new BinarySearchTree<Integer>(4);

	assertArrayEquals(new Integer[] {4}, CommonNodes.findCommonRecursive(t1, t2));
    }

    @Test
    public void oneNodeOnLeftRecursive()
    {
	BinarySearchTree<Integer> t1 = new BinarySearchTree<Integer>(4);
	t1.insert(2);
	t1.insert(5);
	t1.insert(1);

	BinarySearchTree<Integer> t2 = new BinarySearchTree<Integer>(3);
	t2.insert(8);
	t2.insert(2);

	assertArrayEquals(new Integer[] {2}, CommonNodes.findCommonRecursive(t1, t2));
    }

    @Test
    public void oneNodeOnRightRecursive()
    {
	BinarySearchTree<Integer> t1 = new BinarySearchTree<Integer>(4);
	t1.insert(2);
	t1.insert(5);

	BinarySearchTree<Integer> t2 = new BinarySearchTree<Integer>(3);
	t2.insert(8);
	t2.insert(1);
	t2.insert(5);

	assertArrayEquals(new Integer[] {5}, CommonNodes.findCommonRecursive(t1, t2));
    }

    @Test
    public void noCommonRecursive()
    {
	BinarySearchTree<Integer> t1 = new BinarySearchTree<Integer>(14);
	t1.insert(12);
	t1.insert(5);
	t1.insert(1);

	BinarySearchTree<Integer> t2 = new BinarySearchTree<Integer>(3);
	t2.insert(18);
	t2.insert(2);

	assertArrayEquals(new Integer[] {}, CommonNodes.findCommonRecursive(t1, t2));
    }

    @Test
    public void sameTreeTraversal()
    {
	BinarySearchTree<Integer> t1 = new BinarySearchTree<Integer>(4);
	t1.insert(2);
	t1.insert(5);

	BinarySearchTree<Integer> t2 = new BinarySearchTree<Integer>(4);
	t2.insert(2);
	t2.insert(5);

	assertArrayEquals(new Integer[] {2, 4, 5}, CommonNodes.findCommonTraversal(t1, t2));
    }

    @Test
    public void singleNodeTraversal()
    {
	BinarySearchTree<Integer> t1 = new BinarySearchTree<Integer>(4);
	BinarySearchTree<Integer> t2 = new BinarySearchTree<Integer>(4);

	assertArrayEquals(new Integer[] {4}, CommonNodes.findCommonTraversal(t1, t2));
    }

    @Test
    public void oneNodeOnLeftTraversal()
    {
	BinarySearchTree<Integer> t1 = new BinarySearchTree<Integer>(4);
	t1.insert(2);
	t1.insert(5);
	t1.insert(1);

	BinarySearchTree<Integer> t2 = new BinarySearchTree<Integer>(3);
	t2.insert(8);
	t2.insert(2);

	assertArrayEquals(new Integer[] {2}, CommonNodes.findCommonTraversal(t1, t2));
    }

    @Test
    public void oneNodeOnRightTraversal()
    {
	BinarySearchTree<Integer> t1 = new BinarySearchTree<Integer>(4);
	t1.insert(2);
	t1.insert(5);

	BinarySearchTree<Integer> t2 = new BinarySearchTree<Integer>(3);
	t2.insert(8);
	t2.insert(1);
	t2.insert(5);

	assertArrayEquals(new Integer[] {5}, CommonNodes.findCommonTraversal(t1, t2));
    }

    @Test
    public void noCommonTraversal()
    {
	BinarySearchTree<Integer> t1 = new BinarySearchTree<Integer>(14);
	t1.insert(12);
	t1.insert(5);
	t1.insert(1);

	BinarySearchTree<Integer> t2 = new BinarySearchTree<Integer>(3);
	t2.insert(18);
	t2.insert(2);

	assertArrayEquals(new Integer[] {}, CommonNodes.findCommonTraversal(t1, t2));
    }
}
