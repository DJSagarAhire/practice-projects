public class QuickSort
{
    public static void sort(Comparable[] a)
    {
	quickSort(a, 0, a.length-1);
    }

    private static void quickSort(Comparable[] a, int low, int high)
    {
	if(low >= high)   // Only one element. Nothing to do here
	    return;

	int pivotLocation = (int) (Math.random() * (high - low)) + low;   // Select a random pivot in [low, high]

	// Swap pivot with a[low]
	Comparable pivot = a[pivotLocation];
	a[pivotLocation] = a[low];
	a[low] = pivot;

	// 2 pointer algorithm for intermediate swaps
	int i = low+1;
	int j = high;
	while(true)
	{
	    // Loop till an element greater than the pivot is found
	    for(; a[i].compareTo(pivot) < 0; i++)
		if(i == high)
		    break;

	    // Loop till an element less than the pivot is found
	    for(; a[j].compareTo(pivot) > 0; j--)
		if(j == low)
		    break;

	    if(i >= j)   // Pointers have crossed
		break;

	    // Swap out of order elements
	    Comparable temp = a[i];
	    a[i] = a[j];
	    a[j] = temp;
	}

	// Move pivot to correct position
	a[low] = a[j];
	a[j] = pivot;
	
	// Recursive calls
	quickSort(a, low, j-1);
	quickSort(a, j+1, high);
    }
}
