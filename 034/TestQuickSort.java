import static org.junit.Assert.*;
import org.junit.Test;

public class TestQuickSort
{
    @Test
    public void alreadyUnsorted()
    {
	Integer[] a1 = new Integer[] {3, 2, 6, 1};
	QuickSort.sort(a1);
	assertArrayEquals(new Integer[] {1, 2, 3, 6}, a1);
    }

    @Test
    public void reverseSorted()
    {
	Integer[] a1 = new Integer[] {8, 6, 4, 2, 1};
	QuickSort.sort(a1);
	assertArrayEquals(new Integer[] {1, 2, 4, 6, 8}, a1);
    }

    @Test
    public void alreadySorted()
    {
	Integer[] a1 = new Integer[] {2, 4, 6, 8, 10, 12, 14};
	QuickSort.sort(a1);
	assertArrayEquals(new Integer[] {2, 4, 6, 8, 10, 12, 14}, a1);
    }

    @Test
    public void singleElement()
    {
	Integer[] a1 = new Integer[] {3};
	QuickSort.sort(a1);
	assertArrayEquals(new Integer[] {3}, a1);
    }

    @Test
    public void strings()
    {
	String[] a1 = new String[] {"happy", "sad", "angry", "depressed"};
	QuickSort.sort(a1);
	assertArrayEquals(new String[] {"angry", "depressed", "happy", "sad"}, a1);
    }
}
