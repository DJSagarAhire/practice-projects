public class MedianQuickSort
{
    public static void sort(Comparable[] a)
    {
	medianQuickSort(a, 0, a.length-1);
    }

    private static void medianQuickSort(Comparable[] a, int low, int high)
    {
	if(low >= high)   // One element or less. Get outta here.
	    return;

	int pivotLocation;
	if(high - low >= 5)
	{
	    // Calculate pivot location to be median-of-3 (a[low], a[high], a[mid])
	    int mid = (low + high) / 2;
	    if(a[low].compareTo(a[high]) < 0)  // a[low] < a[high]
	    {
		if(a[high].compareTo(a[mid]) < 0)   // a[low] < a[high], a[high] < a[mid]
		    pivotLocation = high;
		else if(a[low].compareTo(a[mid]) < 0)   // a[low] < a[high], a[mid] < a[high], a[low] < a[mid]
		    pivotLocation = mid;
		else                                // a[low] < a[high], a[mid] < a[high], a[mid] < a[low]
		    pivotLocation = low;
	    }
	    else                             // a[high] < a[low]
	    {
		if(a[low].compareTo(a[mid]) < 0)   // a[high] < a[low], a[low] < a[mid]
		    pivotLocation = low;
		else if(a[high].compareTo(a[mid]) < 0)       // a[high] < a[low], a[mid] < a[low], a[high] < a[mid]
		    pivotLocation = mid;
		else              // a[high] < a[low], a[mid] < a[low], a[mid] < a[high]
		    pivotLocation = high;
	    }
	}
	else    // Don't bother for very small sub-arrays
	    pivotLocation = low;

	// Swap pivot to low if it isn't already there
	if(pivotLocation != low)
	{
	    Comparable temp = a[low];
	    a[low] = a[pivotLocation];
	    a[pivotLocation] = temp;
	}
	Comparable pivot = a[low];

	// 2 pointer algorithm for swaps on the basis of pivots
	int i = low + 1;
	int j = high;
	while(true)
	{
	    for(; a[i].compareTo(pivot) < 0; i++)
		if(i >= high)
		    break;

	    for(; a[j].compareTo(pivot) > 0; j--)
		if(j <= low-1)
		    break;

	    if(i >= j)
		break;

	    // Swap a[i] with a[j]
	    Comparable temp = a[i];
	    a[i] = a[j];
	    a[j] = temp;
	}

	// Swap pivot into correct position
	a[low] = a[j];
	a[j] = pivot;

	// Recursive calls
	medianQuickSort(a, low, j-1);
	medianQuickSort(a, j+1, high);
    }
}
