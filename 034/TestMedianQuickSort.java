import static org.junit.Assert.*;
import org.junit.Test;

public class TestMedianQuickSort
{
    @Test
    public void alreadyUnsorted()
    {
	Integer[] a1 = new Integer[] {3, 2, 6, 1, 9, 13, 10, 4, 12};
	MedianQuickSort.sort(a1);
	assertArrayEquals(new Integer[] {1, 2, 3, 4, 6, 9, 10, 12, 13}, a1);
    }

    @Test
    public void reverseSorted()
    {
	Integer[] a1 = new Integer[] {14, 12, 10, 8, 6, 4, 2, 1};
	MedianQuickSort.sort(a1);
	assertArrayEquals(new Integer[] {1, 2, 4, 6, 8, 10, 12, 14}, a1);
    }

    @Test
    public void alreadySorted()
    {
	Integer[] a1 = new Integer[] {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26};
	MedianQuickSort.sort(a1);
	assertArrayEquals(new Integer[] {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26}, a1);
    }

    @Test
    public void singleElement()
    {
	Integer[] a1 = new Integer[] {3};
	MedianQuickSort.sort(a1);
	assertArrayEquals(new Integer[] {3}, a1);
    }

    @Test
    public void strings()
    {
	String[] a1 = new String[] {"happy", "sad", "angry", "depressed", "joyous", "scared", "bored", "excited"};
	MedianQuickSort.sort(a1);
	assertArrayEquals(new String[] {"angry", "bored", "depressed", "excited", "happy", "joyous", "sad", "scared"}, a1);
    }
}
