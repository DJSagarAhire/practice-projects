##Quick Sort

*Problem Statement*: Implement Quick sort for an array.

Two implementations are given, each of which use different methods to mitigate the notorious problem of bad worst-case performance:

1: Normal Quick Sort with Random Pivot  
This reduces the probability that each pivot will be the smallest/largest element in the array as each pivot is randomly selected.

2: Median-of-3 Quicksort  
The pivot is selected as the median of `arr[low]`, `arr[mid]` and `arr[high]`. This helps in case the array is already sorted or almost-sorted, giving the best case performance when the array is already sorted.

Time Complexity: **O(n log n)** Space Complexity: **O(1)** (in place)
