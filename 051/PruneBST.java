/**
   Prunes nodes of a BST outside a specified range. Requires BinarySearchTree.class
   Warning: Prunes in-place
 */
public class PruneBST
{
    public static <Item extends Comparable<Item>> void prune(BinarySearchTree<Item> t, Item low, Item high)
    {
	t.root = pruneNodes(t.root, low, high);
    }

    private static <Item extends Comparable<Item>> BinarySearchTree<Item>.Node pruneNodes(BinarySearchTree<Item>.Node n, Item low, Item high)
    {
	if(n == null)
	    return null;

	Item currentItem = n.item;
	if(currentItem.compareTo(low) < 0)      // less than low
	    return pruneNodes(n.right, low, high);
	else if(currentItem.compareTo(high) > 0)   // greater than high
	    return pruneNodes(n.left, low, high);
	else
	{
	    n.left = pruneNodes(n.left, low, high);
	    n.right = pruneNodes(n.right, low, high);
	    return n;
	}
    }

    public static void main(String[] ar)
    {
	BinarySearchTree<Integer> t1 = new BinarySearchTree<Integer>(20);
	t1.insert(8);
	t1.insert(4);
	t1.insert(6);
	t1.insert(19);
	t1.insert(11);
	t1.insert(10);
	t1.insert(17);
	t1.insert(50);

	t1.printTree();
	prune(t1, 5, 15);
	t1.printTree();
    }
}
