##Prune nodes of a BST outside specified range

*Problem Statement*: Accept a binary search tree and two values `low` and `high` (`low` <= `high`). Change the binary search tree such that all values lie between `low` and `high` with rest of the nodes pruned away.

The recursive algorithm implemented performs the following on a node `n`. The aim is to return a node that satisfies the boundary condition so that it can be used as a pointer by a recursive call on a higher level.

1: If `n` is null, return `null`.

2: If `n.item` < `low`, we can ignore the left subtree of `n`. Return the output of recursing on `n.right`.

3: Likewise, if `n.item` > `high`, we can ignore the right subtree of `n`. Return the output of recursing on `n.left`.

4: If `low` < `n` < `high`, `n` is a valid node. Set `n.left` to the output of recursion on `n.left` and `n.right` to the output of the recursion on `n.right`. Finally, return `n`.

Thus, if a node fails to satisfy the boundary condition, it is replaced by its closest child that satisfies the condition, or `null` if none exists.

Time Complexity: **O(n)**, Space Complexity: **O(1)**
