##Find Intersection Point of 2 Linked Lists

*Problem Statement*: Write a function that accepts 2 linked lists and returns the node at which the lists intersect.

2 implementations are supplied: `detectIntersectionHashmap()`, which runs a hashing-based algorithm, and `detectIntersectionTraversal()`, which runs a traversal-based algorithm.

###Hashing-based Algorithm

The following are the steps of this algorithm:

1: Traverse all the nodes in `list1` and put them in a hashmap.  
2: For each node in `list2`, check if it already exists in the hashmap.  
3: If the node is found, return that node as output.  
4: If not found, go to Step 2 until `list2` is exhausted.  
5: If `list2` is exhausted, return failure (i.e. the lists do not intersect).

Time complexity: **O(m+n)**, Space complexity: **O(m)**  
Number of traversals required: At most 2 full traversals

###Traversal-based Algorithm

The following are the steps of this algorithm:

1: Find the sizes of `list1` and `list2` by traversing them, and store them in `size1` and `size2`.  
2: If `size1` > `size2`, traverse the first (`size1`-`size2`) nodes of `list1`. Otherwise, do similarly for `list2`.  
3: Now, traverse both lists one node at a time using pointers `node1` and `node2` for `list1` and `list2` respectively.  
4: If `node1` == `node2`, return the node as output. Otherwise, continue till both lists are exhausted (both lists should be exhausted together).  
5: If the lists are exhausted, return failure (i.e. the lists do not intersect).

Time complexity: **O(m+n)**, Space complexity: **O(1)**  
Number of traversals required: At least 3 full traversals, 4 if lists do not intersect.
