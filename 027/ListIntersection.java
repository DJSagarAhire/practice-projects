import java.util.HashMap;

public class ListIntersection
{
    public MySkeletalLinkedList.Node detectIntersectionHashmap(MySkeletalLinkedList l1, MySkeletalLinkedList l2)
    {
	HashMap<MySkeletalLinkedList.Node, Boolean> hash = new HashMap<MySkeletalLinkedList.Node, Boolean>();

	for(MySkeletalLinkedList.Node n1=l1.head; n1 != null; n1=n1.next)
	    hash.put(n1, true);

	for(MySkeletalLinkedList.Node n2=l2.head; n2 != null; n2=n2.next)
	{
	    if(hash.containsKey(n2))
		return n2;
	}

	return null;
    }

    public MySkeletalLinkedList.Node detectIntersectionTraversal(MySkeletalLinkedList l1, MySkeletalLinkedList l2)
    {
	// 1: Get Sizes
	int size1=0, size2=0;
	for(MySkeletalLinkedList.Node n1=l1.head; n1 != null; n1=n1.next, size1++);
	for(MySkeletalLinkedList.Node n2=l2.head; n2 != null; n2=n2.next, size2++);

	MySkeletalLinkedList.Node node1=l1.head, node2=l2.head;
	int difference;

	// 2: Traverse part of longer list
	if(size1 > size2)
	{
	    difference = size1 - size2;
	    for(int i=0; i<difference; i++, node1=node1.next);
	}
	else
	{
	    difference = size2 - size1;
	    for(int i=0; i<difference; i++, node2=node2.next);
	}

	// 3: Traverse both lists together
	for(; node1 != null && node2 != null; node1=node1.next, node2=node2.next)
	    if(node1.equals(node2))
		return node1;
	return null;
    }
}
