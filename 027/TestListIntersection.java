import org.junit.Test;
import static org.junit.Assert.*;

public class TestListIntersection
{
    @Test
    public void normalHash()
    {
	ListIntersection test = new ListIntersection();

	MySkeletalLinkedList<Integer> l1 = new MySkeletalLinkedList<Integer>();
	l1.createList(new Integer[] {1,2,4,8,16});
	MySkeletalLinkedList<Integer> l2 = new MySkeletalLinkedList<Integer>();
	l2.createList(new Integer[] {6,9,15});
	l2.head.next.next.next = l1.head.next.next;

	MySkeletalLinkedList.Node res = test.detectIntersectionHashmap(l1, l2);
	assertEquals((int)res.item, 4);
    }

    @Test
    public void normalTraversal()
    {
	ListIntersection test = new ListIntersection();

	MySkeletalLinkedList<Integer> l1 = new MySkeletalLinkedList<Integer>();
	l1.createList(new Integer[] {1,2,4,8,16});
	MySkeletalLinkedList<Integer> l2 = new MySkeletalLinkedList<Integer>();
	l2.createList(new Integer[] {6,9,15});
	l2.head.next.next.next = l1.head.next.next;

	MySkeletalLinkedList.Node res = test.detectIntersectionTraversal(l1, l2);
	assertEquals((int)res.item, 4);
    }

    @Test
    public void emptyHash()
    {
	ListIntersection test = new ListIntersection();

	MySkeletalLinkedList<Integer> l1 = new MySkeletalLinkedList<Integer>();
	l1.head = null;
	MySkeletalLinkedList<Integer> l2 = new MySkeletalLinkedList<Integer>();
	l2.createList(new Integer[] {2,3,4,5});

	MySkeletalLinkedList.Node res = test.detectIntersectionHashmap(l1, l2);
	assertNull(res);
    }

    @Test
    public void emptyTraversal()
    {
	ListIntersection test = new ListIntersection();

	MySkeletalLinkedList<Integer> l1 = new MySkeletalLinkedList<Integer>();
	l1.head = null;
	MySkeletalLinkedList<Integer> l2 = new MySkeletalLinkedList<Integer>();
	l2.createList(new Integer[] {2,3,4,5});

	MySkeletalLinkedList.Node res = test.detectIntersectionTraversal(l1, l2);
	assertNull(res);
    }

    @Test
    public void sameValueHash()
    {
	ListIntersection test = new ListIntersection();

	MySkeletalLinkedList<Integer> l1 = new MySkeletalLinkedList<Integer>();
	l1.createList(new Integer[] {2,4,6,8,10});
	MySkeletalLinkedList<Integer> l2 = new MySkeletalLinkedList<Integer>();
	l2.createList(new Integer[] {2,5,9});
	l2.head.next.next.next = l1.head.next.next.next;

	MySkeletalLinkedList.Node res = test.detectIntersectionHashmap(l1, l2);
	assertEquals((int)res.item, 8);
    }

    @Test
    public void sameValueTraversal()
    {
	ListIntersection test = new ListIntersection();

	MySkeletalLinkedList<Integer> l1 = new MySkeletalLinkedList<Integer>();
	l1.createList(new Integer[] {2,4,6,8,10});
	MySkeletalLinkedList<Integer> l2 = new MySkeletalLinkedList<Integer>();
	l2.createList(new Integer[] {2,5,9});
	l2.head.next.next.next = l1.head.next.next.next;

	MySkeletalLinkedList.Node res = test.detectIntersectionTraversal(l1, l2);
	assertEquals((int)res.item, 8);
    }
}
