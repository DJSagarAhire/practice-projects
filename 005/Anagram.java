import java.util.Arrays;
import java.util.HashMap;

/**
   Provides methods to find if 2 strings are anagrams of each other.
   @author Sagar Ahire
 */
public class Anagram
{
    /**
       Checks if 2 strings are anagrams of each other using the Sorting method. Use for shorter strings.
       @param str1 A string
       @param str2 A string
       @returns true if the strings are anagrams of each other, false otherwise.
     */
    public boolean anagramSort(String str1, String str2)
    {
	// First check lengths
	if(str1.length() != str2.length())
	    return false;

	// Convert to char arrays for further processing
	char[] chr1 = str1.toCharArray();
	char[] chr2 = str2.toCharArray();

	// Sort them both - O(n*logn)
	Arrays.sort(chr1);
	Arrays.sort(chr2);

	// Now compare them
	if(Arrays.equals(chr1, chr2))
	    return true;
	else
	    return false;
    }

    /**
       Checks if 2 strings are anagrams of each other using the Hashing method. Use for very long strings.
       @param str1 A string
       @param str2 A string
       @returns true if the strings are anagrams of each other, false otherwise.
     */
    public boolean anagramHash(String str1, String str2)
    {
	// First check lengths
	if(str1.length() != str2.length())
	    return false;

	// Create a blank HashMap
	HashMap<Character, Integer> map = new HashMap<Character, Integer>();

	// Process str1: Add values to map - O(n)
	for(Character c : str1.toCharArray())
	{
	    Integer val = map.get(c);

	    if(val != null) // Character already encountered
		map.put(c, val+1); // Add 1 to the count
	    else
		map.put(c, 1); // New character, store with count = 1
	}

	// Process str2: Remove values from map - O(n)
	for(Character c : str2.toCharArray())
	{
	    Integer val = map.get(c);

	    if(val == null)
		return false; // Extra Character not in str1
	    else if(val == 1)
		map.remove(c); // Last instance of character, now remove key
	    else
		map.put(c, val-1); // Decrement count
	}

	if(map.isEmpty())
	    return true;
	else
	    return false;
    }

    /**
       Driver method for testing
     */
    public static void main(String[] ar)
    {
	Anagram test = new Anagram();

	// Building the test cases
	String[][] cases = new String[4][2];

	// First case: anagrams
	cases[0][0] = "mango";
	cases[0][1] = "among";

	// Second case: not anagrams
	cases[1][0] = "mango";
	cases[1][1] = "manga";

	// Third case: equal strings
	cases[2][0] = "manga";
	cases[2][1] = "manga";

	// Fourth case: one string empty
	cases[3][0] = "";
	cases[3][1] = "mango";

	// Run all test cases
	for(int i=0; i<cases.length; i++)
	{
	    String str1 = cases[i][0];
	    String str2 = cases[i][1];
	    System.out.println("Checking whether " + str1 + " and " + str2 + " are anagrams");
	    System.out.println("Sort method: " + test.anagramSort(str1, str2));
	    System.out.println("Hash method: " + test.anagramHash(str1, str2));
	}
    }
}
