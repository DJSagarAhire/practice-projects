##Checking A Pair of Strings for Anagrams

*Problem Statement*: Given a pair of strings, find if they are anagrams of each other.

Two implementations are given. The first one is a naive (and dare I say, more readable) approach which simply sorts the 2 strings and compares them. The second one counts the frequencies of the characters in both strings, which is comparatively more efficient using hashmaps (O(1) access).

###Sort Implementation

The sort implementation simply sorts both the strings and compares the now sorted strings for equality. The algorithm is as follows:

1: Accept `str1` and `str2`. If their lengths differ, return `false` immediately.

2: Sort the characters in `str1` and `str2`. This can be done by creating char arrays out of the 2 strings and using, for example, Java's `Arrays.sort()` method.

3: Compare the sorted strings for equality. If equal, return `true` else return `false`.

The sort is the most expensive operation in the algorithm, which has a running time of **O(n*logn)**.

###Hash Implementation

The hash implementation counts the frequency of each character occuring in either string using a HashMap with characters as keys and integers representing the frequency of the characters as values. For the first string, values in the HashMap are incremented and for the second string, the values are decremented.

The algorithm can be stated as follows:

1: Accept `str1` and `str2`. If their lengths differ, return `false` immediately.

2: For each character `c` in `str1`, do the following:

2.1: If `c` is a key in the HashMap, add 1 to its value.  
2.2: If not, enter a new key for `c` with the value of 1.

3: For each character `c` in `str2`, do the following:

3.1: If `c` is not a key in the HashMap, return `false`. An extra character was encountered.  
3.2: If the value of `c` is 1, remove its key from the HashMap.  
3.3: Otherwise, decrement the value for `c` in the HashMap by 1.

The running time for this algorithm is **O(n)**.

If the range of possible characters is known, this can also be adapted to an array-based lookup algorithm with all possible entries as array indices. However, for full-blown Unicode strings, this array can get very large.
