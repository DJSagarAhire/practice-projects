import static org.junit.Assert.*;
import org.junit.Test;

public class TestMergeArrays
{
    @Test
    public void normalTest()
    {
	Integer[][] test = {{2,7,16,19,26}, {1,4,5,18,21}, {3,10,13,17,40}};
	Integer[] expected = new Integer[] {1, 2, 3, 4, 5, 7, 10, 13, 16, 17, 18, 19, 21, 26, 40};
	assertArrayEquals(expected, MergeArrays.merge(test));
    }

    @Test
    public void singleElementArrays()
    {
	Integer[][] test = {{6}, {2}, {1}, {5}, {3}, {4}};
	Integer[] expected = new Integer[] {1,2,3,4,5,6};
	assertArrayEquals(expected, MergeArrays.merge(test));
    }

    @Test
    public void singleArray()
    {
	Integer[][] test = {{1,2,3,4,5,6}};
	Integer[] expected = new Integer[] {1,2,3,4,5,6};
	assertArrayEquals(expected, MergeArrays.merge(test));
    }

    @Test
    public void sortedAcrossArrays()
    {
	Integer[][] test = {{1,3,5,7}, {9,10,11,12}, {20,30,40,50}};
	Integer[] expected = new Integer[] {1,3,5,7,9,10,11,12,20,30,40,50};
	assertArrayEquals(expected, MergeArrays.merge(test));
    }
}
