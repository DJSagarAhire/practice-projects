import java.util.PriorityQueue;
import java.util.HashMap;

public class MergeArrays
{
    /**
       Accepts a k*n array 'a' representing 'k' sorted arrays with 'n' elements each and merges them into a sorted array with n*k elements.
     */
    public static Comparable[] merge(Comparable[][] a)
    {
	int k = a.length;
	int n = a[0].length;

	// min heap for obtaining minimum element out of current contenders
	PriorityQueue<Comparable> minHeap = new PriorityQueue<Comparable>();

	// Hashmap to identify the array to which the min element belongs
	HashMap<Comparable, Integer> map = new HashMap<Comparable, Integer>();

	// Final array
	Comparable[] result = new Comparable[n*k];

	// Initialize with first elt of each array
	for(int i=0; i<k; i++)
	{
	    minHeap.add(a[i][0]);
	    map.put(a[i][0], i);
	}

	int[] arrayPointers = new int[k];
	int responinter = 0;

	while(minHeap.size() != 0)
	{
	    Comparable minElt = minHeap.remove();
	    int arrayIndex = map.remove(minElt);
	    result[responinter++] = minElt;

	    if(arrayPointers[arrayIndex] < n-1)
	    {
		arrayPointers[arrayIndex]++;
		minHeap.add(a[arrayIndex][arrayPointers[arrayIndex]]);
		map.put(a[arrayIndex][arrayPointers[arrayIndex]], arrayIndex);
	    }
	}

	return result;
    }

    public static void main(String[] ar)
    {
	Integer[][] test1 = {{2,7,16,19,26}, {1,4,5,18,21}, {3,10,13,17,40}};
	Comparable[] merged = MergeArrays.merge(test1);

	for(Comparable i : merged)
	    System.out.print(i + " ");
	System.out.println();
    }
}
