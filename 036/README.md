##Merge k Sorted Arrays

*Problem Statement*: Accept `k` sorted arrays of size `n` each and merge them into a single array.

The algorithm uses a min heap to store the candidate elements and the minimum element out of them is thus obtained. At each stage, the candidate element is the current element of the array, which starts at the 0th element and continues till the array is exhausted.

To find which array the element came from, an auxiliary hashmap is maintained. All elements are assumed to be distinct.

Time Complexity: **O(n log k)**, Space Complexity: **O(k)** for the min heap and hashmap
