import static org.junit.Assert.*;
import org.junit.Test;

public class TestInPlaceSortedMerge
{
    @Test
    public void normalTest()
    {
	Integer[] a = new Integer[7];
	a[0] = 9;
	a[1] = 11;
	a[2] = 15;
	Integer[] b = new Integer[] {3, 5, 12, 13};
	InPlaceSortedMerge.merge(a, b);
	assertArrayEquals(new Integer[] {3,5,9,11,12,13,15}, a);
    }

    @Test
    public void aLessThanB()
    {
	Integer[] a = new Integer[7];
	a[0] = 2;
	a[1] = 5;
	Integer[] b = new Integer[] {12, 13, 16, 17, 20};
	InPlaceSortedMerge.merge(a, b);
	assertArrayEquals(new Integer[] {2,5,12,13,16,17,20}, a);
    }

    @Test
    public void bLessThanA()
    {
	Integer[] a = new Integer[7];
	a[0] = 25;
	a[1] = 35;
	Integer[] b = new Integer[] {12, 13, 16, 17, 20};
	InPlaceSortedMerge.merge(a, b);
	assertArrayEquals(new Integer[] {12,13,16,17,20,25,35}, a);
    }

    @Test
    public void singleA()
    {
	Integer[] a = new Integer[8];
	a[0] = 5;
	Integer[] b = new Integer[] {4, 7, 12, 13, 16, 17, 20};
	InPlaceSortedMerge.merge(a, b);
	assertArrayEquals(new Integer[] {4,5,7,12,13,16,17,20}, a);
    }

    @Test
    public void singleB()
    {
	Integer[] a = new Integer[5];
	a[0] = 5;
	a[1] = 8;
	a[2] = 11;
	a[3] = 16;
	Integer[] b = new Integer[] {14};
	InPlaceSortedMerge.merge(a, b);
	assertArrayEquals(new Integer[] {5,8,11,14,16}, a);
    }
}
