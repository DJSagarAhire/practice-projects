##Merge 2 Sorted Arrays In-Place

*Problem Statement*: Given 2 sorted arrays `A` and `B` with `A` having a large enough buffer at the end to store `B`, write a method to merge `B` into `A` in sorted order.

This is accomplished by a linear scan starting from the end of each array.

Time Complexity: **O(n)**, Space Complexity: **O(1)**
