public class InPlaceSortedMerge
{
    public static void merge(Comparable[] a, Comparable[] b)
    {
	int bPointer = b.length - 1;
	int aPointer = a.length - b.length - 1;

	for(int i=a.length-1; aPointer>=0 && bPointer>=0; i--)
	{
	    if(a[aPointer].compareTo(b[bPointer]) > 0)
		a[i] = a[aPointer--];
	    else
		a[i] = b[bPointer--];
	}

	if(aPointer < 0)           // Array a is done, b is remaining
	    while(bPointer >= 0)
		a[bPointer] = b[bPointer--];

	// If b is done and a remains, a already has the required elements
    }

    public static void main(String[] ar)
    {
	Integer[] a = new Integer[7];
	a[0] = 9;
	a[1] = 11;
	a[2] = 15;
	Integer[] b = new Integer[] {3, 5, 12, 13};
	InPlaceSortedMerge.merge(a, b);

	System.out.println(java.util.Arrays.toString(a));
    }
}
