/**
   This class provides methods to check and print prime numbers.
   @author Sagar Ahire
 */
public class Primes
{
    /**
       Accepts an integer and checks if it is prime.
       @param num An integer (num >= 2)
       @return A boolean value indicating whether <code>num</code> is prime
     */
    public boolean checkPrime(int num)
    {
	// Check if number is less than 2
	if(num < 2)
	    throw new IllegalArgumentException("Number less than 2");

	// Check if number is even
	if(num % 2 == 0 && num != 2)
	    return false;

	int i;

	// Check if divisible by any odd number upto sqrt(num)
	for(i=3; i<=Math.sqrt(num); i+=2)
	{
	    if(num % i == 0)
		return false;
	}
	return true;
    }

    /**
       Accepts an integer and prints all prime numbers up to that integer (inclusive) on STDOUT, separated by newlines.
       @param num An integer
     */
    public void printPrimes(int num)
    {
	for(int i=2; i<=num; i++)
	    if(checkPrime(i))
		System.out.println(i);
    }

    /**
       Driver method for testing.
     */
    public static void main(String[] ar)
    {
	int num;
	if(ar.length == 1)
	    num = Integer.parseInt(ar[0]);
	else
	    num = 20;

	Primes test = new Primes();
	test.printPrimes(num);
    }

}
