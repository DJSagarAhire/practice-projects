##001: Print Prime Numbers upto a certain value

A Java implementation is provided in `primes.java`. The steps used for checking whether a number is prime are as follows:

1: Accept a number `num`.

2: If `num` is less than 2, throw an `IllegalArgumentException`. (This is because 2 is the smallest prime number.)

3: If `num` is even or equal to 2, return `true`.

4: Now that 2 and all even numbers have been checked, we can check if any odd integer `i` from 3 onwards divides `num`. This is done in a loop until `i` reaches `sqrt(num)`.

5: If any `i` divides `num`, return `true`. If control reaches outside the loop, return `false`.

Number of modulus operations is **O(sqrt(n))**.
