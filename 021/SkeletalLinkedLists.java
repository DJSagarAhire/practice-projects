import java.util.ArrayList;

class MySkeletalLinkedList<Item>
{
    class Node
    {
	Item item;
	Node next;
    }
    Node head;

    private void insertBeginning(Item item)
    {
	Node node = new Node();
	node.item = item;
	node.next = head;
	head = node;
    }

    void createList(Item[] arr)
    {
	for(int i=arr.length-1; i>=0; i--)
	    insertBeginning(arr[i]);
    }

    void printList()
    {
	for(Node n=head; n!=null; n=n.next)
	    System.out.println(n.item);
    }
}

class MySkeletalDoublyLinkedList<Item>
{
    class Node
    {
	Item item;
	Node next;
	Node prev;
    }
    Node head;
    Node tail;

    private void insertBeginning(Item item)
    {
	Node node = new Node();
	node.item = item;
	node.next = head;
	node.prev = null;
	if(head == null)    // List is empty. Have to set tail too.
	    tail = node;
	else
	    head.prev = node;
	head = node;
    }

    void createList(Item[] arr)
    {
	for(int i=arr.length-1; i>=0; i--)
	    insertBeginning(arr[i]);
    }

    void printList()
    {
	for(Node n=head; n!=null; n=n.next)
	    System.out.println(n.item);
    }

    void printListBackwards()
    {
	for(Node n=tail; n!=null; n=n.prev)
	    System.out.println(n.item);
    }
}

public class SkeletalLinkedLists
{
    public static void main(String[] ar)
    {
	MySkeletalLinkedList<Integer> l = new MySkeletalLinkedList<Integer>();
	l.createList(new Integer[]{1,2,3});
	l.printList();
    }
}
