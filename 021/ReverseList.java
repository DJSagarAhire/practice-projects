public class ReverseList
{
    public static void reverseIterative(MySkeletalLinkedList list)
    {
	if(list.head == null)
	    return;

	MySkeletalLinkedList.Node prev = null;
	MySkeletalLinkedList.Node curr = list.head;
	MySkeletalLinkedList.Node next = list.head.next;

	while(curr != null)
	{
	    next = curr.next;
	    curr.next = prev;
	    prev = curr;
	    curr = next;
	}

	list.head = prev;
    }

    public static void reverseRecursive(MySkeletalLinkedList list)
    {
	if(list.head == null)
	    return;

	if(list.head.next == null)
	    return;

	MySkeletalLinkedList newlist = new MySkeletalLinkedList();
	newlist.head = list.head.next;    // Split list into two

	reverseRecursive(newlist);

	list.head.next.next = list.head;    // Reverse pointer of 2nd node
	list.head.next = null;    // Make current 'head' the last node
	list.head = newlist.head;    // Make head of reversed part the new 'head'
    }

    public static void main(String[] ar)
    {
	MySkeletalLinkedList<Integer> l = new MySkeletalLinkedList<Integer>();
	l.createList(new Integer[]{1,2,3});
	l.printList();
	reverseIterative(l);
	// reverseRecursive(l);
	l.printList();
    }
}
