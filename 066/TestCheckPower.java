import static org.junit.Assert.*;
import org.junit.Test;

public class TestCheckPower
{
    @Test
    public void isPower()
    {
	assertTrue(CheckPower.check(81, 3));
    }

    @Test
    public void isNotPower()
    {
	assertTrue(!CheckPower.check(6, 3));
    }

    @Test
    public void zeroPow()
    {
	assertTrue(CheckPower.check(0, 0));
    }

    @Test
    public void onePow()
    {
	assertTrue(CheckPower.check(1, 4));
    }

    @Test
    public void onePowZeroBase()
    {
	assertTrue(CheckPower.check(1, 0));
    }
}
