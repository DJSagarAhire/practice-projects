public class CheckPower
{
    public static boolean check(int pow, int exp)
    {
	if(pow == exp)
	    return true;

	double log = Math.log(pow) / Math.log(exp);
	return log == Math.floor(log);
    }
}
