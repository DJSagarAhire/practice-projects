##Check if an Integer is a Power of Another

*Problem Statement*: Given 2 integers `n` and `k`, find if `n` is a power of `k`.

The code works on the fact that if `n` is a power of `k` then,

![](http://quicklatex.com/cache3/ql_6fe428d88d39f3766354666bc133ac62_l3.png)

The code works for only non-negative numbers. 0 needs to be handled explicitly as log 0 is not defined.
