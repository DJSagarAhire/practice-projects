import org.junit.Test;
import static org.junit.Assert.*;

public class TestNToLast
{
    @Test
    public void normal()
    {
	NToLast test = new NToLast();
	MySkeletalLinkedList<Integer> l = new MySkeletalLinkedList<Integer>();
	l.createList(new Integer[] {1,2,3,4});
	assertEquals((int)test.findNToLast(l, 2), 3);
    }

    @Test
    public void largeN()
    {
	NToLast test = new NToLast();
	MySkeletalLinkedList<Integer> l = new MySkeletalLinkedList<Integer>();
	l.createList(new Integer[] {1,2,3,4});
	assertNull(test.findNToLast(l, 6));
    }

    @Test
    public void emptyList()
    {
	NToLast test = new NToLast();
	MySkeletalLinkedList<Integer> l = new MySkeletalLinkedList<Integer>();
	assertNull(test.findNToLast(l, 6));
    }
}
