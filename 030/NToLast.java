public class NToLast
{
    public <Item> Item findNToLast(MySkeletalLinkedList<Item> l, int n)
    {
	if(l.head == null)
	    return null;

	MySkeletalLinkedList.Node ahead = l.head;
	for(int i=1; i<=n; i++, ahead = ahead.next)
	    if(ahead == null)
		return null;

	MySkeletalLinkedList.Node behind = l.head;
	for(; ahead != null; behind = behind.next, ahead = ahead.next);

	return (Item)behind.item;
    }
}
