##Find nth to last Element in List

*Problem Statement*: Given a singly-linked list, return the nth to last element in it.

The implementation is inspired by Floyd's cycle detection algorithm. It uses an `ahead` pointer which first moves `n` places forward in the list and then starts a `behind` pointer which moves in sync with the ahead pointer, one position at a time, until the `ahead` pointer reaches the end of the list.

If the `ahead` pointer reaches the end before `n` elements are passed, the function returns `null`.
