/**
   Checks if a binary tree is a binary search tree. Requires BinaryTree.class from #042.
 */
public class CheckBST
{
    public static <Item extends Comparable<Item>> boolean check(BinaryTree<Item> t)
    {
	return checkNodes(t.root);
    }

    private static <Item extends Comparable<Item>> boolean checkNodes(BinaryTree<Item>.Node n)
    {
	if(n == null)
	    return true;

	if(n.left == null && n.right == null)
	    return true;

	if(n.right == null)
	{
	    if(n.left.item.compareTo(n.item) < 0)
		return checkNodes(n.left);
	    else
		return false;
	}
	else if(n.left == null)
	{
	    if(n.right.item.compareTo(n.item) > 0)
		return checkNodes(n.right);
	    else
		return false;
	}
	else
	{
	    if(n.left.item.compareTo(n.item) < 0 && n.right.item.compareTo(n.item) > 0)
		return checkNodes(n.left) && checkNodes(n.right);
	    else
		return false;
	}
    }
}
