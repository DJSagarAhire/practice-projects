import static org.junit.Assert.*;
import org.junit.Test;

public class TestCheckBST
{
    @Test
    public void completeBST()
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(4);
	BinaryTree.Node n1 = t.insertNode(t.root, 2, true);
	BinaryTree.Node n2 = t.insertNode(n1, 1, true);
	BinaryTree.Node n3 = t.insertNode(n1, 3, false);
	BinaryTree.Node n4 = t.insertNode(t.root, 6, false);
	BinaryTree.Node n5 = t.insertNode(n4, 5, true);
	BinaryTree.Node n6 = t.insertNode(n4, 7, false);

	assertTrue(CheckBST.check(t));
    }

    @Test
    public void bstWithoutLeftChild()
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(4);
	BinaryTree.Node n4 = t.insertNode(t.root, 6, false);
	BinaryTree.Node n5 = t.insertNode(n4, 5, true);
	BinaryTree.Node n6 = t.insertNode(n4, 7, false);

	assertTrue(CheckBST.check(t));
    }

    @Test
    public void bstWithoutRightChild()
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(4);
	BinaryTree.Node n1 = t.insertNode(t.root, 2, true);
	BinaryTree.Node n2 = t.insertNode(n1, 1, true);
	BinaryTree.Node n3 = t.insertNode(n1, 3, false);

	assertTrue(CheckBST.check(t));
    }

    @Test
    public void singleElementBST()
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(4);

	assertTrue(CheckBST.check(t));
    }

    @Test
    public void completeNonBST()
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(4);
	BinaryTree.Node n1 = t.insertNode(t.root, 5, true);
	BinaryTree.Node n2 = t.insertNode(n1, 1, true);
	BinaryTree.Node n3 = t.insertNode(n1, 3, false);
	BinaryTree.Node n4 = t.insertNode(t.root, 6, false);
	BinaryTree.Node n5 = t.insertNode(n4, 2, true);
	BinaryTree.Node n6 = t.insertNode(n4, 7, false);

	assertTrue(!CheckBST.check(t));
    }

    @Test
    public void nonBSTWithoutLeftChild()
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(4);
	BinaryTree.Node n4 = t.insertNode(t.root, 6, false);
	BinaryTree.Node n5 = t.insertNode(n4, 7, true);
	BinaryTree.Node n6 = t.insertNode(n4, 5, false);

	assertTrue(!CheckBST.check(t));
    }

    @Test
    public void nonBSTWithoutRightChild()
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(2);
	BinaryTree.Node n1 = t.insertNode(t.root, 4, true);
	BinaryTree.Node n2 = t.insertNode(n1, 1, true);
	BinaryTree.Node n3 = t.insertNode(n1, 3, false);

	assertTrue(!CheckBST.check(t));
    }
}
