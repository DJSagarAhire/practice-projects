##Check if a Binary Tree is a BST

*Problem Statement*: Given a binary tree, check if it is a binary search tree.

The solution provided uses a recursive approach. If `node.left.item < node.item < node.right.item`, then `node` satisfies the BST property. The property then needs to be checked for `node.left` and `node.right`.

An alternative approach is performing an inorder traversal of the tree and checking if elements come out in sorted order.
