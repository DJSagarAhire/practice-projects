##007: Conversion of Roman to Decimal Numbers

*Problem Statement*: Given a string representing a roman numeral, return its equivalent decimal number.

This is a straight recursive implementation of the conversion table found [here](http://www.rapidtables.com/convert/number/how-roman-numerals-to-number.htm).
