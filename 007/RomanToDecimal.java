/**
   Provides a method to convert an input roman numeral to a decimal numeral.
   @author Sagar Ahire
 */

public class RomanToDecimal
{
    /**
       Converts an input roman numeral to a decimal numeral.
       @param roman A String representing a roman numeral assumed to be valid.
       @return The decimal equivalent of 'roman'.
     */
    public int convert(String roman)
    {
	if(roman.equals(""))
	    return 0;
	if(roman.startsWith("M"))
	    return (1000 + convert(roman.substring(1)));
	if(roman.startsWith("CM"))
	    return (900 + convert(roman.substring(2)));
	if(roman.startsWith("D"))
	    return (500 + convert(roman.substring(1)));
	if(roman.startsWith("CD"))
	    return (400 + convert(roman.substring(2)));
	if(roman.startsWith("C"))
	    return (100 + convert(roman.substring(1)));
	if(roman.startsWith("XC"))
	    return (90 + convert(roman.substring(2)));
	if(roman.startsWith("L"))
	    return (50 + convert(roman.substring(1)));
	if(roman.startsWith("XL"))
	    return (40 + convert(roman.substring(2)));
	if(roman.startsWith("X"))
	    return (10 + convert(roman.substring(1)));
	if(roman.startsWith("IX"))
	    return (9 + convert(roman.substring(2)));
	if(roman.startsWith("V"))
	    return (5 + convert(roman.substring(1)));
	if(roman.startsWith("IV"))
	    return (4 + convert(roman.substring(2)));
	if(roman.startsWith("I"))
	    return (1 + convert(roman.substring(1)));
	throw new IllegalArgumentException("Unrecognized roman numeral: " + roman);
    }

    public static void main(String[] ar)
    {
	String roman;
	if(ar.length == 1)
	    roman = ar[0];
	else
	    roman = "MIV";

	RomanToDecimal test = new RomanToDecimal();
	System.out.println("Roman: " + roman + ", Decimal: " + test.convert(roman));
    }
}
