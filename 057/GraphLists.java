import java.util.Queue;
import java.util.LinkedList;

/**
   Defines a Graph class that uses adjacency lists.
   Nodes are denoted using integers 0, 1, ..., n-1.
 */
public class GraphLists
{
    public LinkedList[] lists;
    public int size;

    public GraphLists() { }
    public GraphLists(int nodes)
    {
	size = nodes;
	lists = new LinkedList[size];
	for(int i=0; i<size; i++)
	    lists[i] = new LinkedList();
    }

    public void addEdge(int startNode, int endNode)
    {
	if(!lists[startNode].contains(endNode))
	    lists[startNode].add(endNode);
    }

    public void removeEdge(int startNode, int endNode)
    {
	lists[startNode].remove(endNode);
    }

    /**
       Performs a depth-first search starting from specified node. If specified node value is -1, starts from 0 instead.
     */
    public void depthFirst(int node)
    {
	if(node == -1)
	    node = 0;
	boolean[] visited = new boolean[size];
	depthFirstRecursive(node, visited);
    }

    private boolean[] depthFirstRecursive(int node, boolean[] visited)
    {
	if(visited[node])
	    return visited;

	visited[node] = true;
	System.out.print(node + ", ");

	for(Object connectedNode: lists[node])
	    if(!visited[(int) connectedNode])
		visited = depthFirstRecursive((int) connectedNode, visited);

	return visited;
    }

    public void breadthFirst(int node)
    {
	if(node == -1)
	    node = 0;

	boolean[] discovered = new boolean[size];
	boolean[] visited = new boolean[size];

	Queue<Integer> nodeQueue = new java.util.ArrayDeque<>();
	discovered[node] = true;
	nodeQueue.add(node);

	while(nodeQueue.size() != 0)
	{
	    int currentNode = nodeQueue.remove();
	    visited[currentNode] = true;
	    System.out.print(currentNode + ", ");

	    for(Object connectedNode: lists[currentNode])
		if(!discovered[(int) connectedNode])
		{
		    discovered[(int) connectedNode] = true;
		    nodeQueue.add((int) connectedNode);
		}
	}
    }

    public static void main(String[] ar)
    {
	// Undirected graphs

	// Path on 4 vertices
	GraphLists p4 = new GraphLists(4);
	p4.addEdge(0, 1);
	p4.addEdge(1, 0);
	p4.addEdge(1, 2);
	p4.addEdge(2, 1);
	p4.addEdge(2, 3);
	p4.addEdge(3, 2);

	System.out.println("P4: ");
	p4.depthFirst(2);
	System.out.println();
	p4.breadthFirst(2);
	System.out.println();

	// Complete bipartite graph on 1 and 3 vertices
	GraphLists k13 = new GraphLists(4);
	k13.addEdge(0, 1);
	k13.addEdge(1, 0);
	k13.addEdge(0, 2);
	k13.addEdge(2, 0);
	k13.addEdge(0, 3);
	k13.addEdge(3, 0);

	System.out.println("K(1, 3): ");
	k13.depthFirst(2);
	System.out.println();
	k13.breadthFirst(2);
	System.out.println();

	// Cycle on 4 vertices
	GraphLists c4 = new GraphLists(4);
	c4.addEdge(0, 1);
	c4.addEdge(1, 0);
	c4.addEdge(1, 2);
	c4.addEdge(2, 1);
	c4.addEdge(2, 3);
	c4.addEdge(3, 2);
	c4.addEdge(3, 0);
	c4.addEdge(0, 3);

	System.out.println("C4: ");
	c4.depthFirst(0);
	System.out.println();
	c4.breadthFirst(0);
	System.out.println();

	// Complete bipartite graph on 3 and 2 vertices
	GraphLists k32 = new GraphLists(5);
	k32.addEdge(0, 3);
	k32.addEdge(3, 0);
	k32.addEdge(0, 4);
	k32.addEdge(4, 0);
	k32.addEdge(1, 3);
	k32.addEdge(3, 1);
	k32.addEdge(1, 4);
	k32.addEdge(4, 1);
	k32.addEdge(2, 3);
	k32.addEdge(3, 2);
	k32.addEdge(2, 4);
	k32.addEdge(4, 2);

	System.out.println("K(3, 2):");
	k32.depthFirst(3);
	System.out.println();
	k32.breadthFirst(3);
	System.out.println();

	// 2 components each containing C3/K3
	GraphLists c3c3 = new GraphLists(6);
	c3c3.addEdge(0, 1);
	c3c3.addEdge(1, 0);
	c3c3.addEdge(1, 2);
	c3c3.addEdge(2, 1);
	c3c3.addEdge(2, 0);
	c3c3.addEdge(0, 2);
	c3c3.addEdge(3, 4);
	c3c3.addEdge(4, 3);
	c3c3.addEdge(4, 5);
	c3c3.addEdge(5, 4);
	c3c3.addEdge(3, 5);
	c3c3.addEdge(5, 3);

	System.out.println("C3 + C3:");
	c3c3.depthFirst(4);
	System.out.println();
	c3c3.breadthFirst(4);
	System.out.println();

	// Directed Graphs

	// Path on 4 vertices
	GraphLists p4d = new GraphLists(4);
	p4d.addEdge(0, 1);
	p4d.addEdge(1, 2);
	p4d.addEdge(2, 1);
	p4d.addEdge(3, 2);

	System.out.println("Directed Subset of P4:");
	p4d.depthFirst(0);
	System.out.println();
	p4d.breadthFirst(0);
	System.out.println();

	// Complete bipartite graph on 1 and 3 vertices
	GraphLists k13d = new GraphLists(4);
	k13d.addEdge(0, 1);
	k13d.addEdge(0, 2);
	k13d.addEdge(0, 3);
	k13d.addEdge(2, 0);

	System.out.println("Directed subset of K(1, 3):");
	k13d.depthFirst(2);
	System.out.println();
	k13d.breadthFirst(2);
	System.out.println();
    }
}
