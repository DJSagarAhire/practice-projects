/**
   Provides an array-based implementation of a circular queue.
   Operations supported:
   1) enqueue
   2) dequeue
   3) peek
   4) isEmpty
   5) size
   The array dynamically resizes based on the number of elements in the queue
   @author Sagar Ahire
 */

public class MyQueue<Item>
{
    private Item[] q = (Item[]) new Object[2];
    private int front = 0;  // Location from where next item will be removed (currently non-empty if queue is non-empty)
    private int back = 0;  // Location where next item will be inserted (currently empty)
    // front == back indicates empty queue; 0 is default position

    private void resize(int size)
    {
	int i=0;
	Item[] temp = (Item[]) new Object[size];
	for(i=0; i<=size(); i++)
	    temp[i] = q[(i+front) % q.length];

	// Update front, back and the array itself
	front = 0;
	back = i-1;
	q = temp;
    }

    /**
       Adds an element to the queue.
       @param i Item to be added.
     */
    public void enqueue(Item i)
    {
	// Can't let queue fill up entirely as then empty condition will be same as full condition
	if(size() == q.length-1)
	    resize(2 * q.length);
	q[back] = i;
	back = (back + 1) % q.length;
    }

    /**
       Removes an element from the queue
       @return The item at the front of the queue.
     */
    public Item dequeue()
    {
	if(isEmpty())
	    throw new java.util.NoSuchElementException("Attempt to dequeue from an empty queue");
	Item i = q[front];
	if(size() < q.length / 4)
	    resize(q.length / 2);
	front = (front + 1) % q.length;

	return i;
    }

    /**
       Returns front of queue but does not remove it from the queue.
       @return The item at the front of the queue.
     */
    public Item peek()
    {
	if(isEmpty())
	    throw new java.util.NoSuchElementException("Attempt to peek from an empty queue");
	return q[front];
    }

    /**
       Checks if the queue is empty.
       @return true if the queue is empty, false otherwise.
     */
    public boolean isEmpty()
    {
	return (front == back);
    }

    /**
       Returns size of queue.
       @return Integer indicating current size of queue (no of elements)
     */
    public int size()
    {
	if(back >= front)
	    return back - front;
	else
	    return q.length - (front-back);
    }

    public static void main(String[] ar)
    {
	MyQueue<Integer> test = new MyQueue<Integer>();

	System.out.println("Is queue empty: " + test.isEmpty());

	System.out.println("Enqueue 4.");
	test.enqueue(4);

	System.out.println("Dequeue: " + test.dequeue());

	System.out.println("Enqueue 5.");
	test.enqueue(5);

	System.out.println("Dequeue: " + test.dequeue());

	System.out.println("Enqueue 6.");
	test.enqueue(6);

	System.out.println("Dequeue: " + test.dequeue());

	System.out.println("Enqueue 7.");
	test.enqueue(7);

	System.out.println("Is queue empty: " + test.isEmpty());

	System.out.println("Size of queue: " + test.size());

	System.out.println("Peek: " + test.peek());

	System.out.println("Dequeue: " + test.dequeue());

	System.out.println("Is queue empty: " + test.isEmpty());
    }
}
