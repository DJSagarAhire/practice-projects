##2 Stacks using 1 Deque

*Problem Statement*: Implement 2 stacks using 1 deque.

Here, Stack 1 uses the front of the deque while Stack 2 uses the back of the deque. The count of elements in each stack is maintained separately so that a stack empty condition does not cause elements from the other stack to be dequeued out.

This folder contains 2 implementations. They are:

`My2Stacks1Deque.java`, using the custom class `MyDeque` from Problem 011, and

`J2Stacks1Deque.java`, using the `ArrayDeque` class from Java's Collection Framework.
