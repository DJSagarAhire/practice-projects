/**
   Provides an implementation of 2 stacks using 1 deque.
   Requires MyDeque.class
   @author Sagar Ahire
 */
public class My2Stacks1Deque<Item>
{
    private MyDeque<Item> d = new MyDeque<Item>();
    private int size_1 = 0;
    private int size_2 = 0;

    /**
       Pushes an item into Stack 1.
       @param i Item to be pushed.
     */
    public void push1(Item i)
    {
	d.enqueueFront(i);
	size_1++;
    }

    /**
       Pushes an item into Stack 2.
       @param i Item to be pushed.
     */
    public void push2(Item i)
    {
	d.enqueueBack(i);
	size_2++;
    }

    /**
       Pops an item from Stack 1.
       @return Item at the top of stack 1.
     */
    public Item pop1()
    {
	if(size_1 == 0)
	    throw new java.util.EmptyStackException();
	Item i = d.dequeueFront();
	size_1--;
	return i;
    }

    /**
       Pops an item from Stack 2.
       @return Item at the top of stack 2.
     */
    public Item pop2()
    {
	if(size_2 == 0)
	    throw new java.util.EmptyStackException();
	Item i = d.dequeueBack();
	size_2--;
	return i;
    }

    /**
       Returns the item at the top of Stack 1 but does not remove it from the stack.
       @return Item at the top of Stack 1.
     */
    public Item peek1()
    {
	if(size_1 == 0)
	    throw new java.util.EmptyStackException();
	return (d.peekFront());
    }

    /**
       Returns the item at the top of Stack 2 but does not remove it from the stack.
       @return Item at the top of Stack 2.
     */
    public Item peek2()
    {
	if(size_2 == 0)
	    throw new java.util.EmptyStackException();
	return (d.peekBack());
    }

    /**
       Returns the number of elements in Stack 1.
       @return Number of elements in Stack 1.
     */
    public int size1()
    {
	return size_1;
    }

    /**
       Returns the number of elements in Stack 2.
       @return Number of elements in Stack 2.
     */
    public int size2()
    {
	return size_2;
    }

    /**
       Checks if Stack 1 is empty.
       @return true if Stack 1 is empty, false otherwise.
     */
    public boolean isEmpty1()
    {
	return (size_1 == 0);
    }

    /**
       Checks if Stack 2 is empty.
       @return true if Stack 2 is empty, false otherwise.
     */
    public boolean isEmpty2()
    {
	return (size_2 == 0);
    }

    public static void main(String[] ar)
    {
	My2Stacks1Deque<Integer> test = new My2Stacks1Deque<Integer>();

	System.out.println("Pushing 1 on Stack 1.");
	test.push1(1);

	System.out.println("Pushing 2 on Stack 1.");
	test.push1(2);

	System.out.println("Pushing 3 on Stack 2.");
	test.push2(3);

	System.out.println("Pushing 4 on Stack 1.");
	test.push1(4);

	System.out.println("Pushing 5 on Stack 1.");
	test.push1(5);

	System.out.println("Popping Stack 2: " + test.pop2());

	System.out.println("Popping Stack 1: " + test.pop1());
    }
}
