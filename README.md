#Practice Projects

This is a repository which consists of a list of practice programming questions along with solutions implemented by me. Each program has its own folder whose name corresponds to its number in the below list.

List of Projects:

##Generic Programs (Part 1)

001: Print a list of prime numbers upto a specified number `n`.

002: Return the `n`th Fibonacci number.

003: Sieve of Eratosthenes

004: Pascal Triangle

005: Checking if a string is an anagram or not

006: Compute the `n`th [Catalan number](http://en.wikipedia.org/wiki/Catalan_number)

007: Convert a roman numeral to a decimal numeral

##Stacks and Queues using Arrays

008: Stack Implementation

009: Queue Implementation

010: 2 Stacks using 1 Array

011: Deque Implementation

012: 1 Stack using 2 Queues

013: 2 Stacks using 1 Deque

014: Josephus Problem using Queues

015: Arithmetic Expression Evaluation using Dijkstra's 2-stack Algorithm

016: 3 Stacks using 1 Array

017: Stack with `push`, `pop` and `min` operations in O(1) time

018: Set of Stacks

##Linked Lists

019: Singly Linked List

020: Doubly Linked List

021: Reverse a Linked List

022: Convert singly-linked list to doubly-linked list

023: Stack using Linked List

024: Queue using Linked List

025: Deque using Linked List

026: Merge 2 sorted linked lists

027: Find intersection point of 2 linked lists

028: Detect loop in linked list

029: Remove Duplicates in List

030: Find nth to Last Element in List

031: Sum 2 natural numbers represented as Linked Lists

##Searching and Sorting

032: Selection Sort

033: Insertion Sort

034: Quick Sort

035: Merge Sort

036: Merge `k` sorted arrays

037: Sort array by frequency

038: Find median of an array

039: Find mode of an array

040: Merge 2 sorted arrays in-place

041: Find element in sorted, rotated array

##Trees

042: Binary Tree

043: Binary Search Tree

044: Given a binary tree, check if it is a BST

045: Convert a BST to a sorted linked list

046: Print all root-to-leaf paths of a binary tree

047: Diameter of Binary Tree

048: Lowest Common Ancestor in Binary Tree

049: Common Nodes in 2 BSTs

050: Reconstruct binary tree from Inorder and Preorder Traversals

051: Prune nodes in a BST outside specified range

052: Check if a Binary Tree is balanced

053: Deepest Node in Binary Tree

054: Level-wise Linked Lists of Nodes in BST

055: Print paths summing up to specified value in Binary Tree

##Graphs

056: Graph using Adjacency Matrix, including Traversals

057: Graph using Adjacency Lists, including Traversals

058: Number of Components of a Graph

059: Checking whether an undirected graph has an Euler circuit

060: Prim's Minimal Spanning Tree Algorithm

061: Kruskal's Minimal Spanning Tree Algorithm

062: Dijkstra's Shortest Path Algorithm

063: Checking if graph is Bipartite

064: Topological Sort

##Other Algorithmic Problems

065: Reverse a 32-bit Integer

066: Check if an integer is a power of another

067: Clock Angle Problem

068: String Edit Distance

069: Shortest Common Supersequence

070: Longest Palindromic Substring

071: [Knapsack Problem](http://en.wikipedia.org/wiki/Knapsack_problem)

072: [Rod Cutting Problem](http://www.radford.edu/~nokie/classes/360/dp-rod-cutting.html)

##Other Questions from Contests and Interviews

073: Spiral Printing of Matrix

074: [Rectangular Game](https://www.hackerrank.com/contests/101mar14/challenges/rectangular-game)

075: Radius of a Graph

076: [T9 Spelling](https://code.google.com/codejam/contest/351101/dashboard#s=p2)

077: [Find Index of 0 to be replaced with 1 to get longest continuous sequence of 1s in a binary array](http://www.geeksforgeeks.org/find-index-0-replaced-1-get-longest-continuous-sequence-1s-binary-array/)

078: Minimum Number of Platforms Required for a Railway Station
