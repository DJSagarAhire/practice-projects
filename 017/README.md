##Stack with O(1) min() function

*Problem Statement*: Implement a stack with `push`, `pop` and `min` operations that execute in O(1) time.

The idea here is to maintain an additional stack of min values and build it up as the stack is built. When an element is pushed in the main stack, another element is pushed in the min stack. This is the min of the current top of the min stack and the newly pushed element. When `pop` is called, an element from the min stack is popped and discarded too. One can see that the top of the min stack will always contain the minimum element of the stack.

Based on [this stackoverflow answer](http://stackoverflow.com/a/685074/1677707).
