/**
   Provides an implementation of a special stack with an additional min() operation that works in O(1) time.
   Supports only integers currently.
   @author Sagar Ahire
 */
public class MinStack
{
    private int[] s = new int[2];
    private int[] m = new int[2];
    private int top = -1;

    /**
       Resizes the array holding the stacks to a specified new size.
       @param newsize The new size of the array.
     */
    private void resize(int newsize)
    {
	int[] temp = new int[newsize];
	int[] tempmin = new int[newsize];

	for(int i=0; i<=top; i++)
	{
	    temp[i] = s[i];
	    tempmin[i] = m[i];
	}

	s = temp;
	m = tempmin;
    }

    /**
       Pushes an integer into the stack. Also updates the minimum accordingly.
       @param i The integer to be pushed.
     */
    public void push(int i)
    {
	if(top == s.length-1)
	    resize(s.length * 2);

	if(isEmpty())
	    m[0] = i;
	else if(m[top] > i)
	    m[top+1] = i;
	else
	    m[top+1] = m[top];
	s[++top] = i;
    }

    /**
       Pops an integer from the top of the stack.
       @return The integer at the top of the stack.
     */
    public int pop()
    {
	if(isEmpty())
	    throw new java.util.EmptyStackException();
	if(top < s.length/4)
	    resize(s.length/2);

	int i = s[top--];

	return i;
    }

    /**
       Gets the minimum of all the values currently in the stack. Works in O(1) time due to the existance of an additional min stack.
       @return The minimum value in the stack.
     */
    public int min()
    {
	if(isEmpty())
	    throw new java.util.EmptyStackException();
	return m[top];
    }

    /**
       Returns the value at the top of the stack but does not remove it from the stack.
       @return The integer at the top of the stack.
     */
    public int peek()
    {
	if(isEmpty())
	    throw new java.util.EmptyStackException();
	return s[top];
    }

    /**
       Returns the number of elements in the stack.
       @return Number of elements in the stack.
     */
    public int size()
    {
	return top+1;
    }

    /**
       Checks if the stack is empty.
       @return True if the stack is empty, false otherwise.
     */
    public boolean isEmpty()
    {
	return size() == 0;
    }

    public static void main(String[] ar)
    {
	MinStack test = new MinStack();

	System.out.println("Pushing 3.");
	test.push(3);
	System.out.println("Current minimum: " + test.min());

	System.out.println("Pushing 2.");
	test.push(2);
	System.out.println("Current minimum: " + test.min());

	System.out.println("Pushing 4.");
	test.push(4);
	System.out.println("Current minimum: " + test.min());

	System.out.println("Pop: " + test.pop());
	System.out.println("Current minimum: " + test.min());

	System.out.println("Pop: " + test.pop());
	System.out.println("Current minimum: " + test.min());
    }
}
