public class MySetOfStacks<Item>
{
    private static final int DEFAULT_CAPACITY = 4;
    private Item[][] s;
    private int setTop, stackTop, capacity;

    public MySetOfStacks()
    {
	this(DEFAULT_CAPACITY);
    }

    public MySetOfStacks(int capacity)
    {
	s = (Item[][]) new Object[1][capacity];
	setTop = 0;
	stackTop = -1;
	this.capacity = capacity;
    }

    public void push(Item i)
    {
	if(stackTop == capacity-1)
	    addStack();
	s[setTop][++stackTop] = i;
    }

    public Item pop()
    {
	if(stackTop == -1)
	    removeStack();

	Item i = s[setTop][stackTop];
	s[setTop][stackTop--] = null;
	return i;
    }

    public Item peek()
    {
	if(stackTop == -1)
	    removeStack();

	return s[setTop][stackTop];
    }

    private void addStack()
    {
	Item[][] temp = (Item[][]) new Object[setTop+2][capacity];

	for(int i=0; i<=setTop; i++)
	    temp[i] = s[i];
	setTop++;
	stackTop = -1;
	s = temp;
    }

    private void removeStack()
    {
	if(setTop == 0)
	    throw new java.util.EmptyStackException();

	s[setTop--] = null;
	stackTop = capacity-1;
    }

    public int setSize()
    {
	return setTop+1;
    }

    public int lastSize()
    {
	return stackTop+1;
    }

    public boolean isEmpty()
    {
	return (setSize()==1 && lastSize()==0);
    }

    public static void main(String[] ar)
    {
	MySetOfStacks<Integer> test = new MySetOfStacks<Integer>();

	System.out.println("Pushing 1.");
	test.push(1);

	System.out.println("Pushing 2.");
	test.push(2);

	System.out.println("Pushing 3.");
	test.push(3);

	System.out.println("Pushing 4.");
	test.push(4);

	System.out.println("Number of stacks: " + test.setSize());

	System.out.println("Pushing 5.");
	test.push(5);

	System.out.println("Number of stacks: " + test.setSize());

	System.out.println("Pop: " + test.pop());

	System.out.println("Pop: " + test.pop());

	System.out.println("Number of stacks: " + test.setSize());

	System.out.println("Pushing 6.");
	test.push(6);

	System.out.println("Pushing 7.");
	test.push(7);

	System.out.println("Number of stacks: " + test.setSize());

	System.out.println("Peek: " + test.peek());

	System.out.println("Pop: " + test.pop());

	System.out.println("Pop: " + test.pop());

	System.out.println("Pop: " + test.pop());

	System.out.println("Pop: " + test.pop());

	System.out.println("Pop: " + test.pop());

	System.out.println("Is stack empty: " + test.isEmpty());
    }
}
