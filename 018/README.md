##Set Of Stacks

*Problem Statement*: Implement a set of stacks.

This consists of a number of fixed-length stacks. Once a stack gets filled up, a new stack is allocated, and new elements are pushed into that stack. Similarly, once a stack becomes empty, it is de-allocated and the previous stack is used. The stack methods `push`, `pop`, etc. should operate as if they are operating on a single stack.
