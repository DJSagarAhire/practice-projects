import org.junit.Test;
import static org.junit.Assert.*;

public class TestMySetOfStacks
{
    @Test
    public void testPushOneElement()
    {
	MySetOfStacks ob = new MySetOfStacks(3);
	ob.push(2);

	assertEquals(ob.setSize(), 1);
	assertEquals(ob.lastSize(), 1);
    }

    @Test
    public void testCreateTwoStacks()
    {
	MySetOfStacks ob = new MySetOfStacks(3);

	ob.push(1);
	ob.push(2);
	ob.push(3);
	ob.push(4);

	assertEquals(ob.setSize(), 2);
	assertEquals(ob.lastSize(), 1);
    }

    @Test
    public void testPopWithOneStack()
    {
	MySetOfStacks ob = new MySetOfStacks(3);

	ob.push(1);
	ob.push(2);

	assertEquals(ob.pop(), 2);
    }

    @Test
    public void testPopWithTwoStacks()
    {
	MySetOfStacks ob = new MySetOfStacks(3);

	ob.push(1);
	ob.push(2);
	ob.push(3);
	ob.push(4);

	assertEquals(ob.setSize(), 2);
	assertEquals(ob.pop(), 4);
	assertEquals(ob.pop(), 3);
	assertEquals(ob.setSize(), 1);
    }

    @Test
    public void testPopFromEmptyStack()
    {
	MySetOfStacks ob = new MySetOfStacks(3);

	try
	{
	    ob.pop();
	    fail("Expected EmptyStackException");
	}
	catch(java.util.EmptyStackException e)
	{ /* Ignore. This exception is expected. */ }
    }
}
