public class SumLists
{
    public MySkeletalLinkedList<Integer> sum(MySkeletalLinkedList<Integer> l1, MySkeletalLinkedList<Integer> l2)
    {
	if(l1.head == null)
	    return l2;
	if(l2.head == null)
	    return l1;

	MySkeletalLinkedList<Integer> result = new MySkeletalLinkedList<Integer>();

	MySkeletalLinkedList<Integer>.Node n1 = l1.head;
	MySkeletalLinkedList<Integer>.Node n2 = l2.head;
	MySkeletalLinkedList<Integer>.Node curr = null;
	int carry = 0;

	while(n1 != null || n2 != null)
	{
	    int resnum = carry;

	    if(n1 != null)
		resnum += n1.item;
	    if(n2 != null)
		resnum += n2.item;

	    if(curr == null)    // First node
	    {
		curr = result.new Node();
		result.head = curr;
	    }
	    else
	    {
		curr.next = result.new Node();
		curr = curr.next;
	    }

	    if(resnum >= 10)    // Current addition carries over
	    {
		resnum -= 10;
		curr.item = resnum;
		carry = 1;
	    }
	    else
	    {
		curr.item = resnum;
		carry = 0;
	    }

	    curr.next = null;
	    n1 = n1 == null ? null : n1.next;
	    n2 = n2 == null ? null : n2.next;
	}

	if(carry == 1)
	{
	    curr.next = result.new Node();
	    curr.next.item = 1;
	    curr.next.next = null;
	}

	return result;
    }

    public static void main(String[] ar)
    {
	SumLists test = new SumLists();

	// Test 1
	MySkeletalLinkedList<Integer> l1 = new MySkeletalLinkedList<Integer>();
	l1.createList(new Integer[] {9, 6}); // 69

	MySkeletalLinkedList<Integer> l2 = new MySkeletalLinkedList<Integer>();
	l2.createList(new Integer[] {3, 3}); // 33

	System.out.println("69 + 33:");
	test.sum(l1, l2).printList();

	// Test 2
	MySkeletalLinkedList<Integer> l3 = new MySkeletalLinkedList<Integer>();
	l3.createList(new Integer[] {3, 1, 5}); // 513

	MySkeletalLinkedList<Integer> l4 = new MySkeletalLinkedList<Integer>();
	l4.createList(new Integer[] {5, 9, 2}); // 295

	System.out.println("513 + 295:");
	test.sum(l3, l4).printList();

	// Test 3
	MySkeletalLinkedList<Integer> l5 = new MySkeletalLinkedList<Integer>();
	l5.createList(new Integer[] {3, 5}); // 53

	MySkeletalLinkedList<Integer> l6 = new MySkeletalLinkedList<Integer>();
	l6.createList(new Integer[] {5, 9, 2}); // 295

	System.out.println("53 + 295:");
	test.sum(l5, l6).printList();

	// Test 4
	MySkeletalLinkedList<Integer> l7 = new MySkeletalLinkedList<Integer>();
	l7.createList(new Integer[] {9, 9, 9, 9}); // 9999

	MySkeletalLinkedList<Integer> l8 = new MySkeletalLinkedList<Integer>();
	l8.createList(new Integer[] {1}); // 1

	System.out.println("9999 + 1:");
	test.sum(l7, l8).printList();
    }
}
