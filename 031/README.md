##Sum 2 Integers represented as Linked Lists

*Problem Statement*: Accept 2 integers represented as linked lists (with each digit having its own node and the units place digit at the head of the list) and add them up, returning the result as a linked list.

For example, 5->3->8 + 3->1->6 = 8->4->4->1 (i.e. 835 + 613 = 8441).

Several corner-cases need to be handled properly, especially relating to the carry.
