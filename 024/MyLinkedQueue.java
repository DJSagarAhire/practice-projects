public class MyLinkedQueue<Item>
{
    private class Node
    {
	Item item;
	Node next;
    }
    private Node front = null;
    private Node back = null;
    private int size = 0;

    public void enqueue(Item i)
    {
	Node n = new Node();
	n.item = i;
	n.next = null;

	if(front != null)
	    front.next = n;
	front = n;
	if(back == null)
	    back = front;

	size++;
    }

    public Item dequeue()
    {
	if(front == null)
	    throw new java.util.NoSuchElementException("Attempt to dequeue from empty queue");

	Item i = back.item;
	back = back.next;
	size--;
	return i;
    }

    public Item peek()
    {
	if(front == null)
	    throw new java.util.NoSuchElementException("Attempt to dequeue from empty queue");
	return back.item;
    }

    public boolean isEmpty()
    {
	return front == null;
    }

    public int getSize()
    {
	return size;
    }

    public static void main(String[] ar)
    {
	MyLinkedQueue<Integer> test = new MyLinkedQueue<Integer>();

	System.out.println("Enqueue 1.");
	test.enqueue(1);

	System.out.println("Enqueue 2.");
	test.enqueue(2);

	System.out.println("Is queue empty: " + test.isEmpty());
	System.out.println("Size of queue: " + test.getSize());

	System.out.println("Dequeue: " + test.dequeue());

	System.out.println("Dequeue: " + test.dequeue());
    }
}
