##Array-based Stack Implementation

This is an implementation of a stack using an underlying array. The following operations are supported:

* Push  
* Pop  
* Is Empty  
* Size  
* Peek


