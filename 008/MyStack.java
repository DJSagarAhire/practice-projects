/**
   Provides an array-based implementation of a stack.
   Operations supported:
   1) push
   2) pop
   3) peek
   4) isEmpty
   5) size
   The array dynamically resizes based on the number of elements in the stack.
   @author Sagar Ahire
 */
public class MyStack<Item>
{
    private Item[] s = (Item[]) new Object[2];
    private int top = -1;  // No item in stack by default

    private void resize(int size)
    {
	Item[] temp = (Item[]) new Object[size];
	// Item[] temp = new Item[size];
	for(int i=0; i<=top; i++)
	    temp[i] = s[i];
	s = temp;
    }

    /**
       Pushes an item into the stack.
       @param i Item to be pushed.
     */
    public void push(Item i)
    {
	if(top == s.length - 1)  // Stack is full. Resize it to twice the current size.
	    resize(s.length * 2);
	s[++top] = i;
    }

    /**
       Pops an item from the stack.
       @return The item at the top of the stack.
     */
    public Item pop()
    {
	if(isEmpty())
	    throw new java.util.EmptyStackException();
	Item i = s[top--];
	if(top < s.length / 4)
	    resize(s.length / 2);
	return i;
    }

    /**
       Returns top of stack but does not remove it from the stack.
       @return The item at the top of the stack.
     */
    public Item peek()
    {
	if(isEmpty())
	    throw new java.util.EmptyStackException();
	return s[top];
    }

    /**
       Checks if the stack is empty.
       @return true if the stack is empty, false otherwise.
     */
    public boolean isEmpty()
    {
	return (top == -1);
    }

    /**
       Returns size of stack.
       @return Integer indicating current size of stack (no of elements)
     */
    public int size()
    {
	return (top + 1);
    }

    public static void main(String[] ar)
    {
	MyStack<Integer> test = new MyStack<Integer>();

	System.out.println("Is stack empty: " + test.isEmpty());

	System.out.println("Pushing 4.");
	test.push(4);

	System.out.println("Pushing 5.");
	test.push(5);

	System.out.println("Pop: " + test.pop());

	System.out.println("Pop: " + test.pop());

	System.out.println("Pushing 6.");
	test.push(6);

	System.out.println("Pushing 7.");
	test.push(7);

	System.out.println("Is stack empty: " + test.isEmpty());

	System.out.println("Size of stack: " + test.size());

	System.out.println("Peek: " + test.peek());

	System.out.println("Pop: " + test.pop());

	System.out.println("Pop: " + test.pop());

	System.out.println("Pushing 8.");
	test.push(8);
    }
}
