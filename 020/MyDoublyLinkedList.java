/**
   Provides an implementation of a doubly linked list.
   Operations supported:
   1) Insert (beginning, end, index)
   2) Remove (beginning, end, index)
   3) Find
   4) Size
 */
public class MyDoublyLinkedList<Item>
{
    private class Node
    {
	Item item;
	Node next;
	Node prev;
    }
    private int size=0;
    private Node head;
    private Node tail;

    /**
       Inserts the specified item at the beginning of the list (index 0).
       @param item The item to be inserted
     */
    public void insertBeginning(Item item)
    {
	Node node = new Node();
	node.item = item;
	node.next = head;
	node.prev = null;
	if(head == null)    // List is empty. Have to set tail too.
	    tail = node;
	else
	    head.prev = node;
	head = node;
	size++;
    }

    /**
       Inserts the specified item at the end of the list.
       @param item The item to be inserted
     */
    public void insertEnd(Item item)
    {
	Node node = new Node();
	node.item = item;
	node.next = null;
	node.prev = tail;
	if(head == null)    // List is empty. Have to set head too.
	    head = node;
	else
	    tail.next = node;
	tail = node;
	size++;
    }

    /**
       Inserts the specified item at the specified index position in the list.
       @param item The item to be inserted
       @param index The zero-based index position where the item is to be inserted
     */
    public void insert(Item item, int index)
    {
	if(index > size)
	    throw new IllegalArgumentException("Index for insertion " + index + " is greater than size " + size);

	if(index == 0)
	    insertBeginning(item);
	else if(index == size)
	    insertEnd(item);
	else
	{
	    Node n = head;
	    for(int i=0; i<(index-1); i++, n=n.next);  // Traverse linked list till node before specified index is reached

	    Node node = new Node();
	    node.item = item;
	    node.next = n.next;
	    node.prev = n;

	    n.next.prev = node;
	    n.next = node;

	    size++;
	}
    }

    /**
       Removes the node at the beginning of the list (index 0) and returns its value.
       @return The value stored in the removed node
     */
    public Item removeBeginning()
    {
	if(head == null)
	    throw new EmptyListException();

	Item item = head.item;
	if(head == tail)    // List will now become empty. Set tail to null.
	    tail = null;
	else
	    head.next.prev = null;
	head = head.next;
	size--;

	return item;
    }

    /**
       Removes the node at the end of the list and returns its value.
       @return The value stored in the removed node
     */
    public Item removeEnd()
    {
	if(head == null)
	    throw new EmptyListException();

	Item item = tail.item;
	if(head == tail)    // List will now become empty. Set head to null.
	    head = null;
	else
	    tail.prev.next = null;
	tail = tail.prev;
	size--;

	return item;
    }

    /**
       Removes the node at the specified position from the linked list and returns its value.
       @param index The 0-based index of the node to be removed
       @return The value stored in the removed node
     */
    public Item remove(int index)
    {
	if(head == null)
	    throw new EmptyListException();
	if(index >= size)
	    throw new IllegalArgumentException("Index for removal " + index + " is greater than maximum " + (size-1));

	Item item = null;

	if(index == 0)
	    item = removeBeginning();
	else if(index == size-1)
	    item = removeEnd();
	else
	{
	    Node n = head;
	    for(int i=0; i<index; i++, n=n.next);  // Traverse linked list till node at specified index is reached

	    item = n.item;
	    n.prev.next = n.next;
	    n.next.prev = n.prev;
	    size--;
	}

	return item;
    }

    /**
       Returns the value of node at the specified index but does not remove it from the list.
       @return The value stored in the node at the given index
     */
    public Item peek(int index)
    {
	if(index >= size)
	    throw new IllegalArgumentException("Index for removal " + index + " is greater than maximum " + (size-1));

	Node n = head;
	for(int i=0; i<index; i++, n=n.next);  // Traverse linked list till node at specified index is reached

	return n.item;
    }

    /**
       A convenience method which returns the value of the node at the beginning of the list (index 0) but does not remove it from the list.
       @return The value stored in the node
     */
    public Item peekBeginning()
    {
	return peek(0);
    }

    /**
       A convenience method which returns the value of the node at the end of the list but does not remove it from the list.
       @return The value stored in the node
     */
    public Item peekEnd()
    {
	return peek(size-1);
    }

    /**
       Returns the index of the first node containing the specified value in the list. Returns -1 if no such node is found.
       @param item The item to be found
       @return The index of the specified item
     */
    public int find(Item item)
    {
	Node n = head;
	for(int i=0; i<size; i++, n=n.next)
	    if(n.item == item)
		return i;

	return -1;
    }

    /**
       Returns the number of nodes in the linked list. This value is cached and appropriately updated, and so is an O(1) operation.
       @return The number of nodes in the list.
     */
    public int getSize()
    {
	return size;
    }

    private void printList()
    {
	for(Node n=head; n!=null; n=n.next)
	    System.out.println(n.item);
    }

    private void printListBackwards()
    {
	for(Node n=tail; n!=null; n=n.prev)
	    System.out.println(n.item);
    }

    public static void main(String[] ar)
    {
	MyDoublyLinkedList<Integer> test = new MyDoublyLinkedList<Integer>();

	System.out.println("Insert 1 at end.");
	test.insertEnd(1);

	System.out.println("Insert 2 at end.");
	test.insertEnd(2);

	System.out.println("Current list:");
	test.printList();

	System.out.println("Current list backwards:");
	test.printListBackwards();

	System.out.println("Insert 3 at index 1.");
	test.insert(3, 1);

	System.out.println("Insert 4 at index 3.");
	test.insert(4, 3);

	System.out.println("Insert 5 at index 0.");
	test.insert(5, 0);

	System.out.println("Current list:");
	test.printList();

	System.out.println("Current list backwards:");
	test.printListBackwards();

	System.out.println("Size of list: " + test.getSize());

	System.out.println("Removed from beginning: " + test.removeBeginning());

	System.out.println("Removed from end: " + test.removeEnd());

	System.out.println("Removed from index 1: " + test.remove(1));

	System.out.println("Size of list: " + test.getSize());

	System.out.println("Current list:");
	test.printList();

	System.out.println("Current list backwards:");
	test.printListBackwards();
    }
}

class EmptyListException extends RuntimeException
{
}
