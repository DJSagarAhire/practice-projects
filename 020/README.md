##Doubly Linked List

*Problem Statement*: Implement a doubly-linked list.

This is similar to the singly linked list, except that each node now has a pointer to its previous node in addition to its next node. Also, an additional `tail` pointer is maintained to the last node of the list.

The operations supported are the exact same as the singly linked list in Problem 019.
