import org.junit.Test;
import static org.junit.Assert.*;

public class TestMyDoublyLinkedList
{
    @Test
    public void insertBeginningEmptyList()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();
	ob.insertBeginning(2);

	assertEquals((int)ob.peekBeginning(), 2);
    }

    @Test
    public void insertBeginning()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();
	ob.insertBeginning(2);
	ob.insertBeginning(3);

	assertEquals((int)ob.peekBeginning(), 3);
    }

    @Test
    public void insertEndEmptyList()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();
	ob.insertEnd(2);

	assertEquals((int)ob.peekBeginning(), 2);
    }

    @Test
    public void insertEnd()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();
	ob.insertEnd(2);
	ob.insertEnd(3);

	assertEquals((int)ob.peekEnd(), 3);
    }

    @Test
    public void insertIndexEmptyList()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();
	ob.insert(3, 0);

	assertEquals((int)ob.peekBeginning(), 3);
    }

    @Test
    public void insertIndexBeginning()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();
	ob.insert(3, 0);
	ob.insert(5, 0);

	assertEquals((int)ob.peek(0), 5);
    }

    @Test
    public void insertIndexMiddle()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();
	ob.insertBeginning(3);
	ob.insertBeginning(5);
	ob.insert(4, 1);

	assertEquals((int)ob.peek(1), 4);
    }

    @Test
    public void insertIndexEnd()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();
	ob.insert(3, 0);
	ob.insert(5, 1);

	assertEquals((int)ob.peek(1), 5);
    }

    @Test
    public void insertInvalidIndexEmptyList()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();

	try
	{
	    ob.insert(4, 3);
	    fail("Expected IllegalArgumentException");
	}
	catch(IllegalArgumentException e){}
    }

    @Test
    public void insertInvalidIndex()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();

	ob.insertBeginning(2);
	ob.insertBeginning(3);
	ob.insertBeginning(5);
	try
	{
	    ob.insert(4, 20);
	    fail("Expected IllegalArgumentException");
	}
	catch(IllegalArgumentException e){}
    }

    @Test
    public void removeBeginningEmptyList()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();

	try
	{
	    ob.removeBeginning();
	    fail("Expected EmptyListException");
	}
	catch(EmptyListException e){}
    }

    @Test
    public void removeBeginning()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();

	ob.insertBeginning(2);
	ob.insertBeginning(3);
	assertEquals((int)ob.removeBeginning(), 3);
    }

    @Test
    public void removeEndEmptyList()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();

	try
	{
	    ob.removeEnd();
	    fail("Expected EmptyListException");
	}
	catch(EmptyListException e){}
    }

    @Test
    public void removeEnd()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();

	ob.insertBeginning(2);
	ob.insertBeginning(3);
	assertEquals((int)ob.removeEnd(), 2);
    }

    @Test
    public void removeEmptyList()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();

	try
	{
	    ob.remove(0);
	    fail("Expected EmptyListException");
	}
	catch(EmptyListException e){}
    }

    @Test
    public void removeIndex()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();

	ob.insertBeginning(2);
	ob.insertBeginning(3);
	ob.insertBeginning(6);
	assertEquals((int)ob.remove(1), 3);
    }

    @Test
    public void findEmptyList()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();

	assertEquals(ob.find(100), -1);
    }

    @Test
    public void findNonExistentItem()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();

	ob.insertBeginning(2);
	ob.insertBeginning(4);
	ob.insertBeginning(1);

	assertEquals(ob.find(100), -1);
    }

    @Test
    public void findAtBeginning()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();

	ob.insertBeginning(2);
	ob.insertBeginning(100);

	assertEquals(ob.find(100), 0);
    }

    @Test
    public void findInMiddle()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();

	ob.insertBeginning(2);
	ob.insertBeginning(100);
	ob.insertBeginning(3);

	assertEquals(ob.find(100), 1);
    }

    @Test
    public void findAtEnd()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();

	ob.insertBeginning(100);
	ob.insertBeginning(2);
	ob.insertBeginning(3);

	assertEquals(ob.find(100), 2);
    }

    @Test
    public void sizeEmptyList()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();

	assertEquals(ob.getSize(), 0);
    }

    @Test
    public void size()
    {
	MyDoublyLinkedList<Integer> ob = new MyDoublyLinkedList<Integer>();

	ob.insertBeginning(100);
	ob.insertBeginning(2);
	ob.insertBeginning(3);

	assertEquals(ob.getSize(), 3);
    }
}
