public class SelectionSort
{
    public static void sort(Comparable[] a)
    {
	for(int i=0; i<a.length-1; i++)
	{
	    int currentMinIndex = i;
	    for(int j=i; j<a.length; j++)
	    {
		if(a[j].compareTo(a[currentMinIndex]) < 0)
		    currentMinIndex = j;
	    }

	    // Swap a[i] with a[currentMinIndex]
	    Comparable temp = a[i];
	    a[i] = a[currentMinIndex];
	    a[currentMinIndex] = temp;
	}
    }
}
