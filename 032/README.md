##Selection Sort

*Problem Statement*: Implement Selection Sort for an array.

Time Complexity: **O(n^2)**, Space Complexity: **O(1)** (in place)
