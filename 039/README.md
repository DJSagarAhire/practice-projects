##Find mode of an array

*Problem Statement*: Find the mode of an input array.

The algorithm used creates a HashMap to maintain a count of the frequency of all unique elements in the array and keeps track of the element that has occured maximum times. This is achieved in a linear scan of the array.

Time Complexity: **O(n)**, Space Complexity: **O(n)**
