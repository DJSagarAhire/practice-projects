import static org.junit.Assert.*;
import org.junit.Test;

public class TestMode
{
    @Test
    public void normalTest()
    {
	assertEquals(1, Mode.find(new Integer[] {3,2,1,1,1,3,1}));
    }

    @Test
    public void singleRepeatedElement()
    {
	assertEquals(3, Mode.find(new Integer[] {3,3,3,3}));
    }

    @Test
    public void singleElement()
    {
	assertEquals(3, Mode.find(new Integer[] {3}));
    }

    @Test
    public void multipleModes()
    {
	assertEquals(3, Mode.find(new Integer[] {3,3,3,3,4,4,4,4,3,4}));
    }
}
