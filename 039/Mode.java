import java.util.HashMap;

public class Mode
{
    public static Object find(Object[] a)
    {
	HashMap<Object, Integer> freq = new HashMap<Object, Integer>();
	Object currentMode = null;

	for(Object o: a)
	{
	    if(freq.containsKey(o))
		freq.put(o, freq.get(o)+1);
	    else
		freq.put(o, 1);

	    if(currentMode == null)
		currentMode = o;
	    else if(freq.get(currentMode) < freq.get(o))
		currentMode = o;
	}

	return currentMode;
    }

    public static void main(String[] ar)
    {
	System.out.println(Mode.find(new Integer[] {3,2,1,1,1,3,1}));
    }
}
