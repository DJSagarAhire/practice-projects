import java.util.Queue;
import java.util.LinkedList;

public class BinarySearchTree<Item extends Comparable<Item>>
{
    public class Node
    {
	Item item;
	Node left;
	Node right;

	public Node() { }
	public Node(Item i)
	{
	    item = i;
	    left = null;
	    right = null;
	}
    }

    public Node root = null;

    public BinarySearchTree() { }

    public BinarySearchTree(Item i)
    {
	Node n = new Node(i);
	root = n;
    }

    /**
       Inserts the given item in the binary search tree.
       If the item already exists, throws IllegalArgumentException.
     */
    public void insert(Item i)
    {
	Node curr = root;
	Node parent = null;
	while(curr != null)
	{
	    if(curr.item.equals(i))
		throw new IllegalArgumentException();

	    parent = curr;
	    if(curr.item.compareTo(i) > 0)
		curr = curr.left;
	    else
		curr = curr.right;
	}

	Node n = new Node(i);
	if(parent.item.compareTo(i) > 0)
	    parent.left = n;
	else
	    parent.right = n;
    }

    /**
       Returns whether or not a node with the specified item exists in the tree.
     */
    public boolean find(Item i)
    {
	Node curr = root;
	while(curr != null)
	{
	    if(curr.item.equals(i))
		return true;
	    else if(curr.item.compareTo(i) > 0)
		curr = curr.left;
	    else
		curr = curr.right;
	}
	return false;
    }

    /**
       Searches for a node with item i and removes it if it exists.
     */
    public boolean remove(Item i)
    {
	Node curr = root;
	Node parent = null;
	boolean isLeftChild = false;
	while(curr != null)
	{
	    if(curr.item.equals(i))
		break;
	    parent = curr;
	    if(curr.item.compareTo(i) > 0)
	    {
		curr = curr.left;
		isLeftChild = true;
	    }
	    else
	    {
		curr = curr.right;
		isLeftChild = false;
	    }
	}

	if(curr == null)
	    return false;

	if(curr == root)
	{
	    removeRoot();
	    return true;
	}

	// Node to be deleted exists and is stored in 'curr'.
	if(curr.left == null)
	{
	    if(isLeftChild)
		parent.left = curr.right;
	    else
		parent.right = curr.right;
	}
	else if(curr.right == null)
	{
	    if(isLeftChild)
		parent.left = curr.left;
	    else
		parent.right = curr.left;
	}
	else    // curr has 2 children. Select leftmost child of right subtree as item value to replace it.
	{
	    Node replacer = curr.right;
	    while(replacer.left != null)
		replacer = replacer.left;
	    Item ri = replacer.item;
	    remove(ri);
	    curr.item = ri;
	}

	return true;
    }

    private void removeRoot()
    {
	if(root.left == null)
	    root = root.right;
	else if(root.right == null)
	    root = root.left;
	else
	{
	    Node replacer = root.right;
	    while(replacer.left != null)
		replacer = replacer.left;

	    Item ri = replacer.item;
	    remove(ri);
	    root.item = ri;
	}
    }

    // Generic traveral and print functions here

    public void inOrder()
    {
	inOrderNode(root);
	System.out.println();
    }

    private void inOrderNode(Node n)
    {
	if(n == null)
	    return;
	inOrderNode(n.left);
	System.out.print(n.item + ", ");
	inOrderNode(n.right);
    }

    public void printTree()
    {
	printNodes(root);
	System.out.println();
    }

    private void printNodes(Node n)
    {
	if(n == null)
	    return;
	else
	{
	    System.out.print(n.item + ", ");
	    System.out.print("L(");
	    printNodes(n.left);
	    System.out.print("), R(");
	    printNodes(n.right);
	    System.out.print(")");
	}
    }

    public static void main(String[] ar)
    {
	BinarySearchTree<Integer> t = new BinarySearchTree<Integer>(4);
	t.insert(2);
	t.insert(3);
	t.insert(1);
	t.insert(7);
	t.insert(5);

	t.printTree();
	t.inOrder();

	System.out.println("Does 4 exist: " + t.find(4));
	System.out.println("Does 1 exist: " + t.find(1));
	System.out.println("Does 7 exist: " + t.find(7));
	System.out.println("Does 6 exist: " + t.find(6));

	t.remove(2);
	t.printTree();
    }
}
