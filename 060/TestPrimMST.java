import static org.junit.Assert.*;
import org.junit.Test;

public class TestPrimMST
{
    @Test
    public void path()
    {
	WeightedGraph p4 = new WeightedGraph(4);

	p4.addEdge(0, 1, 2);
	p4.addEdge(1, 0, 2);
	p4.addEdge(1, 2, 4);
	p4.addEdge(2, 1, 4);
	p4.addEdge(2, 3, 12);
	p4.addEdge(3, 2, 12);

	assertEquals(PrimMST.findWeight(p4), 18, 0.01);
    }

    @Test
    public void pathWithAddedEdge()
    {
	WeightedGraph p4 = new WeightedGraph(4);

	p4.addEdge(0, 1, 2);
	p4.addEdge(1, 0, 2);
	p4.addEdge(1, 2, 4);
	p4.addEdge(2, 1, 4);
	p4.addEdge(2, 3, 12);
	p4.addEdge(3, 2, 12);
	p4.addEdge(1, 3, 3);
	p4.addEdge(3, 1, 3);

	assertEquals(PrimMST.findWeight(p4), 9, 0.01);
    }

    @Test
    public void complete()
    {
	WeightedGraph k4 = new WeightedGraph(4);

	k4.addEdge(0, 1, 6);
	k4.addEdge(1, 0, 6);
	k4.addEdge(1, 2, 8);
	k4.addEdge(2, 1, 8);
	k4.addEdge(2, 3, 5);
	k4.addEdge(3, 2, 5);
	k4.addEdge(3, 0, 9);
	k4.addEdge(0, 3, 9);
	k4.addEdge(0, 2, 1);
	k4.addEdge(2, 0, 1);
	k4.addEdge(1, 3, 4);
	k4.addEdge(3, 1, 4);

	assertEquals(PrimMST.findWeight(k4), 10, 0.01);
    }

    @Test
    public void isolatedVertex()
    {
	WeightedGraph i1 = new WeightedGraph(1);

	assertEquals(PrimMST.findWeight(i1), 0, 0.01);
    }
}
