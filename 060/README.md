##Prim's Minimal Spanning Tree Algorithm

*Problem Statement*: Given a simple graph with weights on edges, return the weight of the minimal spanning tree obtained from it using Prim's algorithm.

[Prim's Minimal Spanning Tree Algorithm](http://en.wikipedia.org/wiki/Prim's_algorithm) creates a spanning tree by growing it out of a single starting vertex. The following have been implemented for this:

1: A Weighted Graph has been implemented, which associates a `HashMap` with every vertex, with keys representing the other vertices in the graph, and values representing the weight of the edge from the current vertex to the vertex in the key.

2: The Prim algorithm has been implemented. This implementation unfortunately runs in O(n^2) where `n` is the number of vertices. This is because there is no easy way to sort a `Map` on values rather than keys in Java.
