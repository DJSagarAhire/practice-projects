import java.util.HashMap;

/**
   Defines a graph using an "adjacency hashmap" - each vertex has an associated hashmap which associates weights to vertices representing the weight of the edge between the 2 vertices.
 */
public class WeightedGraph
{
    public HashMap<Integer, Double>[] edges;
    public int size;

    public WeightedGraph() { }
    public WeightedGraph(int nodes)
    {
	size = nodes;
	edges = new HashMap[size];
	for(int i=0; i<size; i++)
	    edges[i] = new HashMap<Integer, Double>();
    }

    public void addEdge(int startNode, int endNode, double weight)
    {
	edges[startNode].put(endNode, weight);
    }

    public void removeEdge(int startNode, int endNode)
    {
	edges[startNode].remove(endNode);
    }
}
