import java.util.ArrayList;
import java.util.HashMap;

/**
   Implements Prim's Minimal Spanning Tree algorithm to find the weight of the minimal spanning tree in the graph. Requires WeightedGraph.class.
   Graph is assumed to be connected.
 */
public class PrimMST
{
    public static double findWeight(WeightedGraph g)
    {
	boolean[] covered = new boolean[g.size];
	covered[0] = true;

	HashMap<Integer, Double> minWeights = new HashMap<>();
	for(int v: g.edges[0].keySet())
	    minWeights.put(v, g.edges[0].get(v));

	double mstWeight = 0.0;

	while(minWeights.size() != 0)
	{
	    int minVertex = getMinimun(minWeights);
	    double minValue = minWeights.remove(minVertex);
	    covered[minVertex] = true;
	    mstWeight += minValue;

	    for(int v: g.edges[minVertex].keySet())
	    {
		if(covered[v])
		    continue;
		if(minWeights.containsKey(v))
		    minWeights.put(v, Math.min(minWeights.get(v), g.edges[minVertex].get(v)));
		else
		    minWeights.put(v, g.edges[minVertex].get(v));
	    }
	}

	return mstWeight;
    }

    // Bad news: O(n) minimum.
    private static int getMinimun(HashMap<Integer, Double> minWeights)
    {
	int minVertex = Integer.MAX_VALUE;
	double minValue = Double.MAX_VALUE;

	for(int v: minWeights.keySet())
	    if(minWeights.get(v) < minValue)
	    {
		minVertex = v;
		minValue = minWeights.get(v);
	    }

	return minVertex;
    }
}
