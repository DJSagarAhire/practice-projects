import static org.junit.Assert.*;
import org.junit.Test;

public class TestReplaceIndex
{
    @Test
    public void allOnes()
    {
	assertEquals(ReplaceIndex.findIndex(new int[] {1,1,1,1}), -1);
    }

    @Test
    public void singleZeroStarting()
    {
	assertEquals(ReplaceIndex.findIndex(new int[] {0,1,1,1}), 0);
    }

    @Test
    public void singleZeroMid()
    {
	assertEquals(ReplaceIndex.findIndex(new int[] {1,1,0,1,1}), 2);
    }

    @Test
    public void singleZeroEnd()
    {
	assertEquals(ReplaceIndex.findIndex(new int[] {1,1,1,0}), 3);
    }

    @Test
    public void singleZero()
    {
	assertEquals(ReplaceIndex.findIndex(new int[] {0}), 0);
    }

    @Test
    public void twoZerosSecondSolution()
    {
	assertEquals(ReplaceIndex.findIndex(new int[] {1,0,1,1,1,1,0,1,1,1,1,1}), 6);
    }

    @Test
    public void twoZerosFirstSolution()
    {
	assertEquals(ReplaceIndex.findIndex(new int[] {1,1,1,1,1,1,1,0,1,1,0,1,1}), 7);
    }

    @Test
    public void other1()
    {
	assertEquals(ReplaceIndex.findIndex(new int[] {1,1,0,0,1,0,1,1,1,0,1,1,1}), 9);
    }
}
