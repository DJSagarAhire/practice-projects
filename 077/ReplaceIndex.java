public class ReplaceIndex
{
    public static int findIndex(int[] arr)
    {
	int maxSequence = 0;
	int currentMaxWithReplacement = 0;
	int currentReplacedIndex = -1;
	int currentMaxWithoutReplacement = 0;
	int maxIndex = -1;

	for(int i=0; i<arr.length; i++)
	{
	    if(arr[i] == 1)
	    {
		currentMaxWithoutReplacement++;
		currentMaxWithReplacement++;
	    }
	    else
	    {
		currentMaxWithReplacement = currentMaxWithoutReplacement + 1;
		currentMaxWithoutReplacement = 0;
		currentReplacedIndex = i;
	    }
	    if(currentMaxWithReplacement > maxSequence)
	    {
		maxSequence = currentMaxWithReplacement;
		maxIndex = currentReplacedIndex;
	    }
	}

	return maxIndex;
    }
}
