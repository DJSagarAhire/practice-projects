##Find Index of 0 to be replaced with 1 to get longest continuous sequence of 1s in a binary array

*Problem Statement*: [Geeks for Geeks](http://www.geeksforgeeks.org/find-index-0-replaced-1-get-longest-continuous-sequence-1s-binary-array/)

While the problem statement is lifted from geeksforgeeks, the solution provided is different. This solution involves keeping track of the following, at each index `i` of the array:

1: Length of longest sequence of 1's ending at `i`, assuming that no replacement has occured. This will change to 0 if `arr[i]` is 0, otherwise it increments by 1.

2: Length of longest sequence of 1's ending at `i`, assuming that a replacement has occured. This will change to longest sequence without replacement **+ 1** if `arr[i]` is 0, otherwise it will also increment by 1.

3: Corresponding index which caused the latest replacement upto `arr[i]` (simply the last index whose value is 0).

4: Maximum length of longest sequence of 1's found so far, with replacement. This will update if length of longest sequence ending at `i` with replacement is greater than the current value.

5: Corresponding index for the maximum replacement, which updates similarly.

Time Complexity: **O(n)**  
Space Complexity: **O(1)**
