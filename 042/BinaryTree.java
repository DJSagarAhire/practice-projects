import java.util.LinkedList;
import java.util.Queue;

public class BinaryTree<Item>
{
    public class Node
    {
	Item item;
	Node left;
	Node right;

	public Node() { }
	public Node(Item i)
	{
	    item = i;
	    left = null;
	    right = null;
	}
    }
    public Node root = null;

    // Default constructor which does nothing
    public BinaryTree() { }

    /**
       Constructor which accepts an item and creates a new binary tree with the root node having the specified item value.
     */
    public BinaryTree(Item i)
    {
	root = new Node(i);
    }

    /**
       Inserts a new node with item 'i' as a child of node 'n', left child if specified otherwise right.
       If the child already exists, throws IllegalArgumentException.
       Returns the node that was created.
     */
    public Node insertNode(Node n, Item i, boolean left)
    {
	if((left && n.left != null) || (!left && n.right != null))
	    throw new IllegalArgumentException();

	Node newNode = new Node(i);
	if(left)
	    n.left = newNode;
	else
	    n.right = newNode;

	return newNode;
    }

    public void inOrder()
    {
	inOrderNode(root);
	System.out.println();
    }

    private void inOrderNode(Node n)
    {
	if(n == null)
	    return;
	inOrderNode(n.left);
	System.out.print(n.item + ", ");
	inOrderNode(n.right);
    }

    public void preOrder()
    {
	preOrderNode(root);
	System.out.println();
    }

    private void preOrderNode(Node n)
    {
	if(n == null)
	    return;
	System.out.print(n.item + ", ");
	preOrderNode(n.left);
	preOrderNode(n.right);
    }

    public void postOrder()
    {
	postOrderNode(root);
	System.out.println();
    }

    private void postOrderNode(Node n)
    {
	if(n == null)
	    return;
	postOrderNode(n.left);
	postOrderNode(n.right);
	System.out.print(n.item + ", ");
    }

    public void levelOrder()
    {
	Queue<Node> nodes = new LinkedList<Node>();
	nodes.add(root);

	while(nodes.size() > 0)
	{
	    Node current = nodes.remove();
	    System.out.print(current.item + ", ");
	    if(current.left != null)
		nodes.add(current.left);
	    if(current.right != null)
		nodes.add(current.right);
	}
	System.out.println();
    }

    public void printTree()
    {
	printNodes(root);
	System.out.println();
    }

    private void printNodes(Node n)
    {
	if(n == null)
	    return;
	else
	{
	    System.out.print(n.item + ", ");
	    System.out.print("L(");
	    printNodes(n.left);
	    System.out.print("), R(");
	    printNodes(n.right);
	    System.out.print(")");
	}
    }

    public static void main(String[] ar)
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(4);
	BinaryTree.Node n2 = t.insertNode(t.root, 2, true);
	BinaryTree.Node n1 = t.insertNode(n2, 1, true);
	BinaryTree.Node n0 = t.insertNode(n1, 0, true);
	BinaryTree.Node n3 = t.insertNode(n2, 3, false);
	BinaryTree.Node n5 = t.insertNode(t.root, 5, false);

	t.printTree();
	t.inOrder();
	t.preOrder();
	t.postOrder();
	t.levelOrder();
    }
}
