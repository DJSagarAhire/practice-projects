##Binary Tree

This contains an implementation of a binary tree with the following functionality:

1: Insert Node  
2: Traversal - Pre-order, In-order, Post-order and Level-order  
3: A convenience function for printing the tree
