##T9 Keyboard

*Problem Statement*: [Google Code Jam Africa 2010 Qualification Round](https://code.google.com/codejam/contest/351101/dashboard#s=p2)  
Given a message, output the sequence of keys to be pressed on a mobile keyboard.

Extremely straightforward mapping of letters to numeric strings.

Time Complexity: **O(n)**  
Space Complexity: **O(1)** (A mapping of the size of the alphabet)
