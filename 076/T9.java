import java.io.*;
import java.util.HashMap;

public class T9
{
    public static void main(String[] ar) throws IOException
    {
	HashMap<Character, String> charMap = new HashMap<>();
	charMap.put('a', "2");
	charMap.put('b', "22");
	charMap.put('c', "222");
	charMap.put('d', "3");
	charMap.put('e', "33");
	charMap.put('f', "333");
	charMap.put('g', "4");
	charMap.put('h', "44");
	charMap.put('i', "444");
	charMap.put('j', "5");
	charMap.put('k', "55");
	charMap.put('l', "555");
	charMap.put('m', "6");
	charMap.put('n', "66");
	charMap.put('o', "666");
	charMap.put('p', "7");
	charMap.put('q', "77");
	charMap.put('r', "777");
	charMap.put('s', "7777");
	charMap.put('t', "8");
	charMap.put('u', "88");
	charMap.put('v', "888");
	charMap.put('w', "9");
	charMap.put('x', "99");
	charMap.put('y', "999");
	charMap.put('z', "9999");
	charMap.put(' ', "0");

	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	int t = Integer.parseInt(br.readLine());

	for(int count=1; count<=t; count++)
	{
	    System.out.print("Case #" + count + ": ");
	    String message = br.readLine();
	    StringBuilder numeric = new StringBuilder(charMap.get(message.charAt(0)));

	    for(int i=1; i<message.length(); i++)
	    {
		char curr = message.charAt(i);
		char prev = message.charAt(i-1);

		String currMsg = charMap.get(curr);
		String prevMsg = charMap.get(prev);

		if(prevMsg.charAt(0) == currMsg.charAt(0))
		    numeric.append(" ");
		numeric.append(currMsg);
	    }

	    System.out.println(numeric.toString());
	}
    }
}
