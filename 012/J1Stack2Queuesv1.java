import java.util.Queue;
import java.util.LinkedList;

/**
   Provides an implementation of a single stack using 2 queues.
   This provides an efficient O(1) push but an inefficient O(n) pop.
   @author Sagar Ahire
 */
public class J1Stack2Queuesv1<Item>
{
    private Queue<Item> q1 = new LinkedList<Item>();
    private Queue<Item> q2 = new LinkedList<Item>();

    /**
       Pushes an item into the stack.
       @param i Item to be pushed.
     */
    public void push(Item i)
    {
	q1.add(i);
    }

    /**
       Pops an item from the stack.
       @return The item at the top of the stack.
     */
    public Item pop()
    {
	while(q1.size() != 1)
	    q2.add(q1.remove());

	Item i = q1.remove();

	// Swap queues
	Queue temp = q1;
	q1 = q2;
	q2 = temp;

	return i;
    }

    /**
       Returns top of stack but does not remove it from the stack.
       @return The item at the top of the stack.
     */
    public Item peek()
    {
	while(q1.size() != 1)
	    q2.add(q1.remove());

	Item i = q1.remove();
	q2.add(q1.remove());

	// Swap queues
	Queue temp = q1;
	q1 = q2;
	q2 = temp;

	return i;
    }

    /**
       Checks if the stack is empty.
       @return true if the stack is empty, false otherwise.
     */
    public boolean isEmpty()
    {
	return q1.isEmpty();
    }

    /**
       Returns size of stack.
       @return Integer indicating current size of stack (no of elements)
     */
    public int size()
    {
	return q1.size();
    }

    public static void main(String[] ar)
    {
	J1Stack2Queuesv1<Integer> test = new J1Stack2Queuesv1<Integer>();

	System.out.println("Push 1.");
	test.push(1);

	System.out.println("Pop: " + test.pop());

	System.out.println("Push 2.");
	test.push(2);

	System.out.println("Push 3.");
	test.push(3);

	System.out.println("Size of stack: " + test.size());

	System.out.println("Pop: " + test.pop());

	System.out.println("Size of stack: " + test.size());

	System.out.println("Is stack empty: " + test.isEmpty());

	System.out.println("Pop: " + test.pop());

	System.out.println("Is stack empty: " + test.isEmpty());
    }
}
