##1 Stack using 2 Queues

*Problem Statement*: Implement 1 stack using 2 queues.

2 versions of stacks are possible using 2 queues:

* Efficient O(1) push, Inefficient O(n) pop  
* Efficient O(1) pop, Inefficient O(n) push.

This folder contains 2 implementations of each of these versions. They are:

**Custom-made Classes**  
The files `My1Stack2Queuesv1.java` and `My1Stack2Queuesv2.java` contain implementations using the `MyQueue` class created in Problem 009.

**Java Classes**  
The files `J1Stack2Queuesv1.java` and `J1Stack2Queuesv2.java` contain implementations using classes from the Java Collections Framework.
