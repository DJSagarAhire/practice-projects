import java.util.ArrayList;

public class SpiralMatrix
{
    public static int[] traverse(int[][] matrix)
    {
	int n = matrix.length;
	int m = matrix[0].length;

	boolean right = true;
	boolean down = true;
	int numHorizontal = m;
	int numVertical = n-1;
	int ptrX = 0;
	int ptrY = -1;

	ArrayList<Integer> spiralList = new ArrayList<>();

	while(true)
	{
	    // Move horizontal
	    if(numHorizontal == 0)
		break;

	    for(int i=0; i<numHorizontal; i++)
	    {
		if(right)
		    ptrY++;
		else
		    ptrY--;
		spiralList.add(matrix[ptrX][ptrY]);
	    }
	    numHorizontal--;
	    right = !right;

	    // Move vertical
	    if(numVertical == 0)
		break;

	    for(int i=0; i<numVertical; i++)
	    {
		if(down)
		    ptrX++;
		else
		    ptrX--;
		spiralList.add(matrix[ptrX][ptrY]);
	    }
	    numVertical--;
	    down = !down;
	}

	int[] spiralArr = new int[spiralList.size()];
	for(int i=0; i<spiralArr.length; i++)
	    spiralArr[i] = spiralList.get(i);

	return spiralArr;
    }
}
