import static org.junit.Assert.*;
import org.junit.Test;

public class TestSpiralMatrix
{
    @Test
    public void mat3x3()
    {
	int[][] m = {{1,2,3}, {4,5,6}, {7,8,9}};
	assertArrayEquals(SpiralMatrix.traverse(m), new int[] {1,2,3,6,9,8,7,4,5});
    }

    @Test
    public void mat4x4()
    {
	int[][] m = {{1,2,3,4}, {5,6,7,8}, {9,10,11,12}, {13,14,15,16}};
	assertArrayEquals(SpiralMatrix.traverse(m), new int[] {1,2,3,4,8,12,16,15,14,13,9,5,6,7,11,10});
    }

    @Test
    public void mat3x1()
    {
	int[][] m = {{1}, {2}, {3}};
	assertArrayEquals(SpiralMatrix.traverse(m), new int[] {1,2,3});
    }

    @Test
    public void mat1x3()
    {
	int[][] m = {{1, 2, 3}};
	assertArrayEquals(SpiralMatrix.traverse(m), new int[] {1,2,3});
    }

    @Test
    public void mat3x4()
    {
	int[][] m = {{1,2,3,4}, {5,6,7,8}, {9,10,11,12}};
	assertArrayEquals(SpiralMatrix.traverse(m), new int[] {1,2,3,4,8,12,11,10,9,5,6,7});
    }

    @Test
    public void mat4x3()
    {
	int[][] m = {{1,2,3}, {4,5,6}, {7,8,9}, {10,11,12}};
	assertArrayEquals(SpiralMatrix.traverse(m), new int[] {1,2,3,6,9,12,11,10,7,4,5,8});
    }
}
