##Spiral Printing of Matrix

*Problem Statement*: Given a matrix of size `m` x `n`, return its elements traversed in spiral order starting from (0, 0).

The code works by keeping track of the following:  
1) The indices of our current location in the array,  
2) The number of elements to be printed horizontally in the next iteration,  
3) The number of elements to be printed vertically in the next iteration,  
4) Whether the horizontal direction is rightwards or not,  
4) Whether the vertical direction is downwards or not.

Appropriately, the matrix is traversed.
