import org.junit.Test;
import static org.junit.Assert.*;

public class TestMyLinkedList
{
    @Test
    public void insertBeginningEmptyList()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();
	ob.insertBeginning(2);

	assertEquals((int)ob.peekBeginning(), 2);
    }

    @Test
    public void insertBeginning()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();
	ob.insertBeginning(2);
	ob.insertBeginning(3);

	assertEquals((int)ob.peekBeginning(), 3);
    }

    @Test
    public void insertEndEmptyList()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();
	ob.insertEnd(2);

	assertEquals((int)ob.peekBeginning(), 2);
    }

    @Test
    public void insertEnd()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();
	ob.insertEnd(2);
	ob.insertEnd(3);

	assertEquals((int)ob.peekEnd(), 3);
    }

    @Test
    public void insertIndexEmptyList()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();
	ob.insert(3, 0);

	assertEquals((int)ob.peekBeginning(), 3);
    }

    @Test
    public void insertIndexBeginning()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();
	ob.insert(3, 0);
	ob.insert(5, 0);

	assertEquals((int)ob.peek(0), 5);
    }

    @Test
    public void insertIndexMiddle()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();
	ob.insertBeginning(3);
	ob.insertBeginning(5);
	ob.insert(4, 1);

	assertEquals((int)ob.peek(1), 4);
    }

    @Test
    public void insertIndexEnd()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();
	ob.insert(3, 0);
	ob.insert(5, 1);

	assertEquals((int)ob.peek(1), 5);
    }

    @Test
    public void insertInvalidIndexEmptyList()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();

	try
	{
	    ob.insert(4, 3);
	    fail("Expected IllegalArgumentException");
	}
	catch(IllegalArgumentException e){}
    }

    @Test
    public void insertInvalidIndex()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();

	ob.insertBeginning(2);
	ob.insertBeginning(3);
	ob.insertBeginning(5);
	try
	{
	    ob.insert(4, 20);
	    fail("Expected IllegalArgumentException");
	}
	catch(IllegalArgumentException e){}
    }

    @Test
    public void removeBeginningEmptyList()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();

	try
	{
	    ob.removeBeginning();
	    fail("Expected EmptyListException");
	}
	catch(EmptyListException e){}
    }

    @Test
    public void removeBeginning()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();

	ob.insertBeginning(2);
	ob.insertBeginning(3);
	assertEquals((int)ob.removeBeginning(), 3);
    }

    @Test
    public void removeEndEmptyList()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();

	try
	{
	    ob.removeEnd();
	    fail("Expected EmptyListException");
	}
	catch(EmptyListException e){}
    }

    @Test
    public void removeEnd()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();

	ob.insertBeginning(2);
	ob.insertBeginning(3);
	assertEquals((int)ob.removeEnd(), 2);
    }

    @Test
    public void removeEmptyList()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();

	try
	{
	    ob.remove(0);
	    fail("Expected EmptyListException");
	}
	catch(EmptyListException e){}
    }

    @Test
    public void removeIndex()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();

	ob.insertBeginning(2);
	ob.insertBeginning(3);
	ob.insertBeginning(6);
	assertEquals((int)ob.remove(1), 3);
    }

    @Test
    public void findEmptyList()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();

	assertEquals(ob.find(100), -1);
    }

    @Test
    public void findNonExistentItem()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();

	ob.insertBeginning(2);
	ob.insertBeginning(4);
	ob.insertBeginning(1);

	assertEquals(ob.find(100), -1);
    }

    @Test
    public void findAtBeginning()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();

	ob.insertBeginning(2);
	ob.insertBeginning(100);

	assertEquals(ob.find(100), 0);
    }

    @Test
    public void findInMiddle()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();

	ob.insertBeginning(2);
	ob.insertBeginning(100);
	ob.insertBeginning(3);

	assertEquals(ob.find(100), 1);
    }

    @Test
    public void findAtEnd()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();

	ob.insertBeginning(100);
	ob.insertBeginning(2);
	ob.insertBeginning(3);

	assertEquals(ob.find(100), 2);
    }

    @Test
    public void sizeEmptyList()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();

	assertEquals(ob.getSize(), 0);
    }

    @Test
    public void size()
    {
	MyLinkedList<Integer> ob = new MyLinkedList<Integer>();

	ob.insertBeginning(100);
	ob.insertBeginning(2);
	ob.insertBeginning(3);

	assertEquals(ob.getSize(), 3);
    }
}
