/**
   Provides an implementation of a singly linked list.
   Supported operations:
   1) Insert (beginning, middle, end)
   2) Delete (beginning, middle, end)
   3) Search
   4) Size
 */
public class MyLinkedList<Item>
{
    private class Node
    {
	Item item;
	Node next;
    }
    private Node head;
    private int size=0;

    /**
       Inserts the specified item at the specified index position in the list.
       @param item The item to be inserted
       @param index The zero-based index position where the item is to be inserted
     */
    public void insert(Item item, int index)
    {
	if(index > size)
	    throw new IllegalArgumentException("Index for insertion " + index + " is greater than size " + size);

	if(index == 0)
	    insertBeginning(item);
	else
	{
	    Node n = head;
	    for(int i=0; i<(index-1); i++, n=n.next);  // Traverse linked list till node before specified index is reached

	    Node node = new Node();
	    node.item = item;
	    node.next = n.next;
	    n.next = node;
	    size++;
	}
    }

    /**
       Inserts the specified item at the beginning of the list (index 0).
       @param item The item to be inserted
     */
    public void insertBeginning(Item item)
    {
	Node node = new Node();
	node.item = item;
	node.next = head;
	head = node;
	size++;
    }

    /**
       A convenience method that inserts the specified item at the end of the list.
       @param item The item to be inserted
     */
    public void insertEnd(Item item)
    {
	insert(item, size);
    }

    /**
       Removes the node at the specified position from the linked list and returns its value.
       @param index The 0-based index of the node to be removed
       @return The value stored in the removed node
     */
    public Item remove(int index)
    {
	if(head == null)
	    throw new EmptyListException();
	if(index >= size)
	    throw new IllegalArgumentException("Index for removal " + index + " is greater than maximum " + (size-1));

	Item item = null;
	if(index == 0)
	    item = removeBeginning();
	else
	{
	    Node n = head;
	    for(int i=0; i<(index-1); i++, n=n.next);  // Traverse linked list till node before specified index is reached

	    item = n.next.item;
	    n.next = n.next.next;
	    size--;
	}

	return item;
    }

    /**
       Removes the node at the beginning of the list (index 0) and returns its value.
       @return The value stored in the removed node
     */
    public Item removeBeginning()
    {
	if(head == null)
	    throw new EmptyListException();

	Item item = head.item;
	head = head.next;
	size--;

	return item;
    }

    /**
       A convenience method which removes the node at the end of the list and returns its value.
       @return The value stored in the removed node
     */
    public Item removeEnd()
    {
	if(head == null)
	    throw new EmptyListException();

	return remove(size-1);
    }

    /**
       Returns the value of node at the specified index but does not remove it from the list.
       @return The value stored in the node at the given index
     */
    public Item peek(int index)
    {
	if(index >= size)
	    throw new IllegalArgumentException("Index for removal " + index + " is greater than maximum " + (size-1));

	Node n = head;
	for(int i=0; i<index; i++, n=n.next);  // Traverse linked list till node before specified index is reached

	return n.item;
    }

    /**
       A convenience method which returns the value of the node at the beginning of the list (index 0) but does not remove it from the list.
       @return The value stored in the node
     */
    public Item peekBeginning()
    {
	return peek(0);
    }

    /**
       A convenience method which returns the value of the node at the end of the list but does not remove it from the list.
       @return The value stored in the node
     */
    public Item peekEnd()
    {
	return peek(size-1);
    }

    /**
       Returns the index of the first node containing the specified value in the list. Returns -1 if no such node is found.
       @param item The item to be found
       @return The index of the specified item
     */
    public int find(Item item)
    {
	Node n = head;
	for(int i=0; i<size; i++, n=n.next)
	    if(n.item == item)
		return i;

	return -1;
    }

    /**
       Returns the number of nodes in the linked list. This value is cached and appropriately updated, and so is an O(1) operation.
       @return The number of nodes in the list.
     */
    public int getSize()
    {
	return size;
    }

    private void printList()
    {
	for(Node n=head; n!=null; n=n.next)
	    System.out.println(n.item);
    }

    public static void main(String[] ar)
    {
	MyLinkedList<Integer> test = new MyLinkedList<Integer>();

	System.out.println("Insert 3 at End.");
	test.insertEnd(3);

	System.out.println("Insert 1 at Beginning.");
	test.insertBeginning(1);

	System.out.println("Insert 2 at Beginning.");
	test.insertBeginning(2);

	System.out.println("Size: " + test.getSize());

	System.out.println("Current list:");
	test.printList();

	System.out.println("Insert 4 at index 1.");
	test.insert(4, 1);

	System.out.println("Insert 5 at index 4.");
	test.insert(5, 4);

	System.out.println("Insert 6 at index 0.");
	test.insert(6, 0);

	System.out.println("6 occurs at index " + test.find(6));

	System.out.println("Current list:");
	test.printList();

	System.out.println("Removed item at beginning: " + test.removeBeginning());

	System.out.println("Current list:");
	test.printList();

	System.out.println("Removed item at index 2: " + test.remove(2));

	System.out.println("Current list:");
	test.printList();

	System.out.println("Removed item at index 3: " + test.remove(3));

	System.out.println("Peeked item at end: " + test.peekEnd());

	System.out.println("Removed item at end: " + test.removeEnd());

	System.out.println("Current list:");
	test.printList();
    }
}

class EmptyListException extends RuntimeException
{
}
