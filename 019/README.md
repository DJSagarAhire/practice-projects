##Singly Linked List

*Problem Statement*: Implement a singly linked list.

The implemented linked list supports the following operations:

1: Insert at beginning, end, or by index  
2: Remove at beginning, end, or by index  
3: Peek at beginning, end, or by index  
4: Search for an element (returns index of element)  
5: Get size (cached in list)

JUnit tests are also supplied in `TestMyLinkedList.java`.
