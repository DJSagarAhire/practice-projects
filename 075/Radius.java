import java.util.Queue;
// import java.util.HashMap;

/**
   Finds the radius of the given graph. Requires GraphLists.class
 */
public class Radius
{
    public static int find(GraphLists g)
    {
	int n = g.size;
	int[][] distances = new int[n][n];
	for(int i=0; i<n; i++)
	    for(int j=0; j<n; j++)
		distances[i][j] = Integer.MAX_VALUE;

	// BFS for each vertex
	for(int i=0; i<n; i++)
	{
	    boolean[] visited = new boolean[n];
	    boolean[] discovered = new boolean[n];
	    Queue<Integer> nodeQueue = new java.util.ArrayDeque<>();
	    // HashMap<Integer, Integer> nodeDist = new HashMap<>();

	    discovered[i] = true;
	    nodeQueue.add(i);
	    distances[i][i] = 0;

	    while(nodeQueue.size() > 0)
	    {
		int currentNode = nodeQueue.remove();
		visited[currentNode] = true;

		for(Object connectedNode: g.lists[currentNode])
		    if(!discovered[(int) connectedNode])
		    {
			discovered[(int) connectedNode] = true;
			nodeQueue.add((int) connectedNode);
			distances[i][(int) connectedNode] = 1 + distances[i][currentNode];
			distances[(int) connectedNode][i] = distances[i][(int) connectedNode];
		    }
	    }
	}

	int[] eccentricity = new int[n];
	for(int i=0; i<n; i++)
	{
	    for(int j=0; j<n; j++)
		eccentricity[i] = Math.max(eccentricity[i], distances[i][j]);
	}

	int radius = Integer.MAX_VALUE;
	for(int i=0; i<n; i++)
	    radius = Math.min(radius, eccentricity[i]);

	return radius;
    }
}
