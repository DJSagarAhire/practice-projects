import static org.junit.Assert.*;
import org.junit.Test;

public class TestRadius
{
    @Test
    public void singleEdge()
    {
	GraphLists e = new GraphLists(2);
	e.addEdge(0, 1);
	e.addEdge(1, 0);

	assertEquals(Radius.find(e), 1);
    }

    @Test
    public void triangle()
    {
	GraphLists c3 = new GraphLists(3);
	c3.addEdge(0, 1);
	c3.addEdge(1, 0);
	c3.addEdge(0, 2);
	c3.addEdge(2, 0);
	c3.addEdge(1, 2);
	c3.addEdge(2, 1);

	assertEquals(Radius.find(c3), 1);
    }

    @Test
    public void cycle4()
    {
	GraphLists c4 = new GraphLists(4);
	c4.addEdge(0, 1);
	c4.addEdge(1, 0);
	c4.addEdge(1, 2);
	c4.addEdge(2, 1);
	c4.addEdge(3, 2);
	c4.addEdge(2, 3);
	c4.addEdge(3, 0);
	c4.addEdge(0, 3);

	assertEquals(Radius.find(c4), 2);
    }

    @Test
    public void bull()
    {
	GraphLists bull = new GraphLists(5);
	bull.addEdge(0, 1);
	bull.addEdge(1, 0);
	bull.addEdge(1, 2);
	bull.addEdge(2, 1);
	bull.addEdge(0, 2);
	bull.addEdge(2, 0);
	bull.addEdge(1, 3);
	bull.addEdge(3, 1);
	bull.addEdge(4, 2);
	bull.addEdge(2, 4);

	assertEquals(Radius.find(bull), 2);
    }

    @Test
    public void path7()
    {
	GraphLists p7 = new GraphLists(7);
	p7.addEdge(0, 1);
	p7.addEdge(1, 0);
	p7.addEdge(1, 2);
	p7.addEdge(2, 1);
	p7.addEdge(2, 3);
	p7.addEdge(3, 2);
	p7.addEdge(3, 4);
	p7.addEdge(4, 3);
	p7.addEdge(4, 5);
	p7.addEdge(5, 4);
	p7.addEdge(5, 6);
	p7.addEdge(6, 5);

	assertEquals(Radius.find(p7), 3);
    }
}
