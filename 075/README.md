##Radius of a Graph

*Problem Statement*: Given a graph, find its radius. The [radius](http://en.wikipedia.org/wiki/Distance_\(graph_theory\)#Related_concepts) of a graph is the minimum eccentricity among all vertices of the graph, where the eccentricity of a vertex is the maximum distance of that vertex from any other vertex in the graph.

This is done by running an all-pairs shortest path using BFS to find all the distances, the finding all eccentricities by doing a `max` over all distances for a particular vertex, and finally doing a `min` over all eccentricities to get the radius.

Time Complexity: **O(mn)**, where `m` is number of vertices, `n` is number of edges.  
Space Complexity: **O(m^2)**
