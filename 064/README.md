##Topological Sort of a DAG

*Problem Statement*: Accept a Directed Acyclic Graph and print a Topological Sort of its vertices.

The algorithm used is the DFS-based version [described on Wikipedia](http://en.wikipedia.org/wiki/Topological_sorting#Algorithms) with some minor modifications. The current implementation assumes that the graph is a DAG.
