import java.util.Stack;

/**
   Prints a topological sort of a DAG. Requires GraphLists.class.
 */
public class TopSort
{
    private static Stack<Integer> sortedNodes = new Stack<>();
    private static boolean[] visited;

    public static void sort(GraphLists g)
    {
	visited = new boolean[g.size];

	for(int i=0; i<g.size; i++)
	{
	    if(!visited[i])
		visit(g, i);
	}

	while(sortedNodes.size() > 0)
	    System.out.print(sortedNodes.pop() + ", ");
    }

    private static void visit(GraphLists g, int node)
    {
	for(Object connectedNode: g.lists[node])
	{
	    if(!visited[(int) connectedNode])
		visit(g, (int) connectedNode);
	}

	sortedNodes.push(node);
	visited[node] = true;
    }

    public static void main(String[] ar)
    {
	// G1
	GraphLists g1 = new GraphLists(6);
	g1.addEdge(0, 2);
	g1.addEdge(1, 2);
	g1.addEdge(1, 3);
	g1.addEdge(1, 4);
	g1.addEdge(2, 3);
	g1.addEdge(3, 5);

	sort(g1);
	System.out.println();

	// G2 (From Wikipedia)
	GraphLists g2 = new GraphLists(8);
	g2.addEdge(3, 7);
	g2.addEdge(3, 4);
	g2.addEdge(2, 7);
	g2.addEdge(1, 4);
	g2.addEdge(1, 6);
	g2.addEdge(7, 0);
	g2.addEdge(7, 5);
	g2.addEdge(7, 6);
	g2.addEdge(4, 5);

	sort(g2);
	System.out.println();

	// G3 (From geeksforgeeks)
	GraphLists g3 = new GraphLists(6);
	g3.addEdge(5, 2);
	g3.addEdge(5, 0);
	g3.addEdge(4, 0);
	g3.addEdge(4, 1);
	g3.addEdge(2, 3);
	g3.addEdge(3, 1);

	sort(g3);
	System.out.println();
    }
}
