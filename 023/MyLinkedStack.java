public class MyLinkedStack<Item>
{
    private class Node
    {
	Item item;
	Node next;
    }
    Node top = null;
    int size = 0;

    public void push(Item i)
    {
	Node n = new Node();
	n.item = i;
	n.next = top;
	top = n;
	size++;
    }

    public Item pop()
    {
	if(top == null)
	    throw new java.util.EmptyStackException();

	Item i = top.item;
	top = top.next;
	size--;
	return i;
    }

    public Item peek()
    {
	if(top == null)
	    throw new java.util.EmptyStackException();
	return top.item;
    }

    public boolean isEmpty()
    {
	return top == null;
    }

    public int getSize()
    {
	return size;
    }

    public static void main(String[] ar)
    {
	MyLinkedStack<Integer> test = new MyLinkedStack<Integer>();

	System.out.println("Push 1.");
	test.push(1);

	System.out.println("Push 2.");
	test.push(2);

	System.out.println("Is stack empty: " + test.isEmpty());
	System.out.println("Size: " + test.getSize());

	System.out.println("Peek: " + test.peek());

	System.out.println("Pop: " + test.pop());

	System.out.println("Pop: " + test.pop());

	System.out.println("Is stack empty: " + test.isEmpty());
	System.out.println("Size: " + test.getSize());

	System.out.println("Push 3.");
	test.push(3);

	System.out.println("Pop: " + test.pop());
    }
}
