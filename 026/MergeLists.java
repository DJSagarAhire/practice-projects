/**
   Provides a method to merge two sorted linked lists in sorted order.
   Requires MySkeletalLinkedList.class and MySkeletalLinkedList$Node.class
   @author Sagar Ahire
 */
public class MergeLists
{
    public MySkeletalLinkedList<Integer> merge(MySkeletalLinkedList<Integer> list1, MySkeletalLinkedList<Integer> list2)
    {
	MySkeletalLinkedList<Integer> reslist = new MySkeletalLinkedList<Integer>();
	MySkeletalLinkedList.Node prev = null;

	for(MySkeletalLinkedList.Node n1=list1.head, n2=list2.head; ; )
	{
	    if(n1 == null)
	    {
		for(; n2 != null ; n2 = n2.next)
		{
		    MySkeletalLinkedList.Node node = reslist.new Node();
		    node.item = n2.item;
		    if(prev != null)
			prev.next = node;
		    else
			reslist.head = node;
		    prev = node;
		}
		break;
	    }

	    else if(n2 == null)
	    {
		for(; n1 != null ; n1 = n1.next)
		{
		    MySkeletalLinkedList.Node node = reslist.new Node();
		    node.item = n1.item;
		    if(prev != null)
			prev.next = node;
		    else
			reslist.head = node;
		    prev = node;
		}
		break;
	    }

	    Integer i;
	    if((Integer) n1.item > (Integer) n2.item)
	    {
		i = (Integer) n2.item;
		n2 = n2.next;
	    }
	    else
	    {
		i = (Integer) n1.item;
		n1 = n1.next;
	    }

	    MySkeletalLinkedList.Node node = reslist.new Node();
	    node.item = i;
	    if(prev != null)
		prev.next = node;
	    else
		reslist.head = node;
	    prev = node;
	}

	return reslist;
    }

    public static void main(String[] ar)
    {
	MergeLists test = new MergeLists();

	MySkeletalLinkedList<Integer> test1 = new MySkeletalLinkedList<Integer>();
	test1.createList(new Integer[] {});

	MySkeletalLinkedList<Integer> test2 = new MySkeletalLinkedList<Integer>();
	test2.createList(new Integer[] {1,2});

	test.merge(test1, test2).printList();
    }
}
