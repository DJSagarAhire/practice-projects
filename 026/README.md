##Merging two Sorted Linked Lists

*Problem Statement*: Write a function which accepts 2 linked lists, assumed to be sorted, and returns a third list which merges the 2 lists.
