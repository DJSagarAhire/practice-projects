##Knapsack Problem

*Problem Statement*: Given a knapsack with maximum weight `W` and `i` items each with weight `w_i` and value `v_i`, find the maximum possible obtainable value that can be filled in the knapsack without exceeding the weight limit.

This can be done using a standard dynamic programming algorithm given by the following recurrence:
![](http://quicklatex.com/cache3/ql_dfef3bc06c0fd0acc3ff7c9ad06a7845_l3.png)

Here, `maxval(i, w)` stands for the maximum value obtained using the first `i` items with a restriction of weight `w`. The first line is applicable only if the weight of the `i`th item fits in the current restriction and signifies the maximum of 2 cases: the `i`th item itself is either used or not used. If the weight of the `i`th item is more than the limit, then it cannot be used anyway.

The table can be filled using bottom-up or top-down appeoaches.

Time Complexity: **O(nw)**  
Space Complexity: **O(nw)**
