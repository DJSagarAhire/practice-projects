public class Knapsack
{
    public static int maxValue(int w, int[] values, int[] weights)
    {
	int n = values.length;
	int[][] maxval = new int[n+1][w+1];

	for(int i=1; i<n+1; i++)
	{
	    for(int j=1; j<w+1; j++)
	    {
		if(j >= weights[i-1])
		    maxval[i][j] = Math.max(maxval[i-1][j], maxval[i-1][j-weights[i-1]] + values[i-1]);
		else
		    maxval[i][j] = maxval[i-1][j];
	    }
	}

	return maxval[n][w];
    }
}
