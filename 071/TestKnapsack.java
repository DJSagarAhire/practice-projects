import static org.junit.Assert.*;
import org.junit.Test;

public class TestKnapsack
{
    @Test
    public void noItems()
    {
	assertEquals(Knapsack.maxValue(2, new int[] {2, 100, 25, 9}, new int[] {3,6,3,4}), 0);
    }

    @Test
    public void firstItem()
    {
	assertEquals(Knapsack.maxValue(3, new int[] {2, 100, 25, 9}, new int[] {3,6,4,4}), 2);
    }

    @Test
    public void lastItem()
    {
	assertEquals(Knapsack.maxValue(3, new int[] {2, 100, 25, 9}, new int[] {5,6,4,3}), 9);
    }

    @Test
    public void oneOfMultipleItems()
    {
	assertEquals(Knapsack.maxValue(5, new int[] {2, 100, 25, 9}, new int[] {3,3,4,3}), 100);
    }

    @Test
    public void multipleItems()
    {
	assertEquals(Knapsack.maxValue(9, new int[] {2, 30, 25, 9, 10}, new int[] {3,6,5,4,8}), 34);
    }
}
