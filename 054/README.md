##Level-wise Linked Lists of Nodes in BST

*Problem Statement*: Accept a Binary Search Tree of `d` levels and return `d` linked lists, where the `i`th list contains all nodes at the `i`th level of the BST.

A simple recursive solution that progressively builds an ArrayList of LinkedLists, keeping track of the current depth, is given.
