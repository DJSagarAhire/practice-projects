/**
   Creates level-by-level linked lists from a BST. Requires BinarySearchTree.class
 */

import java.util.ArrayList;
import java.util.LinkedList;

public class LevelLL
{
    public static <Item extends Comparable<Item>> ArrayList<LinkedList<Item>> list(BinarySearchTree<Item> t)
    {
	ArrayList<LinkedList<Item>> finalList = new ArrayList<LinkedList<Item>>();
	listNodes(t.root, finalList, 0);
	return finalList;
    }

    private static <Item extends Comparable<Item>> void listNodes(BinarySearchTree<Item>.Node n, ArrayList<LinkedList<Item>> currentList, int currentLevel)
    {
	if(n == null)
	    return;

	if(currentList.size() <= currentLevel)
	{
	    LinkedList<Item> l = new LinkedList<Item>();
	    currentList.add(l);
	}

	LinkedList<Item> ll = currentList.get(currentLevel);
	ll.add(n.item);

	listNodes(n.left, currentList, currentLevel+1);
	listNodes(n.right, currentList, currentLevel+1);
    }

    public static void main(String[] ar)
    {
	BinarySearchTree<Integer> t = new BinarySearchTree<Integer>(4);
	t.insert(2);
	t.insert(6);
	t.insert(7);
	t.insert(5);
	t.insert(10);
	t.insert(1);

	for(LinkedList<Integer> l: list(t))
	{
	    for(Integer i: l)
		System.out.print(i + ", ");
	    System.out.println();
	}
    }
}
