import static org.junit.Assert.*;
import org.junit.Test;

public class TestEulerCircuit
{
    @Test
    public void isolatedVertex()
    {
	GraphLists i1 = new GraphLists(1);

	assertTrue(EulerCircuit.containsEulerCircuit(i1));
    }

    @Test
    public void cycle()
    {
	GraphLists c3 = new GraphLists(3);
	c3.addEdge(0, 1);
	c3.addEdge(1, 0);
	c3.addEdge(0, 2);
	c3.addEdge(2, 0);
	c3.addEdge(1, 2);
	c3.addEdge(2, 1);

	assertTrue(EulerCircuit.containsEulerCircuit(c3));
    }

    @Test
    public void multipleIsolatedVertices()
    {
	GraphLists i3 = new GraphLists(3);

	assertTrue(!EulerCircuit.containsEulerCircuit(i3));
    }

    @Test
    public void star()
    {
	GraphLists k13 = new GraphLists(4);
	k13.addEdge(0, 1);
	k13.addEdge(1, 0);
	k13.addEdge(0, 2);
	k13.addEdge(2, 0);
	k13.addEdge(0, 3);
	k13.addEdge(3, 0);

	assertTrue(!EulerCircuit.containsEulerCircuit(k13));
    }
}
