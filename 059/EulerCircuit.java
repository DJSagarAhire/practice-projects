import java.util.Queue;

/**
   Checks if a given graph has an Euler Circuit. Requires GraphLists.class
 */
public class EulerCircuit
{
    public static boolean containsEulerCircuit(GraphLists g)
    {
	return isEven(g) && isConnected(g);
    }

    private static boolean isEven(GraphLists g)
    {
	for(int i=0; i<g.size; i++)
	    if(g.lists[i].size() % 2 != 0)
		return false;
	return true;
    }

    private static boolean isConnected(GraphLists g)
    {
	boolean[] visited = new boolean[g.size];
	boolean[] discovered = new boolean[g.size];

	Queue<Integer> nodeQueue = new java.util.ArrayDeque<>();
	nodeQueue.add(0);
	discovered[0] = true;

	while(nodeQueue.size() != 0)
	{
	    int currentNode = nodeQueue.remove();
	    visited[currentNode] = true;

	    for(Object connectedNode: g.lists[currentNode])
		if(!discovered[(int) connectedNode])
		{
		    discovered[(int) connectedNode] = true;
		    nodeQueue.add((int) connectedNode);
		}
	}

	return allVisited(visited);
    }

    private static boolean allVisited(boolean[] visited)
    {
	for(boolean b: visited)
	    if(!b)
		return false;
	return true;
    }
}
