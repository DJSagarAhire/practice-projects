##Euler Circuit in Undirected Graph

*Problem Statement*: Given an undirected graph, check if it has an Euler Circuit.

A graph has an [Euler circuit](http://en.wikipedia.org/wiki/Eulerian_path) if and only if it satisfies the following conditions:

1: Degrees of all its vertices are even,  
2: The graph is connected.
