import static org.junit.Assert.*;
import org.junit.Test;

public class TestPlatforms
{
    @Test
    public void singleTrain()
    {
	assertEquals(Platforms.findMin(new int[] {2}, new int[] {10}), 1);
    }

    @Test
    public void multipleTrainsOnePlatform()
    {
	assertEquals(Platforms.findMin(new int[] {2, 5, 10, 16}, new int[] {3, 7, 12, 18}), 1);
    }

    @Test
    public void multipleTrains()
    {
	assertEquals(Platforms.findMin(new int[] {2, 5, 10, 16}, new int[] {12, 14, 17, 22}), 3);
    }

    @Test
    public void multipleTrainsJumbledDepartures()
    {
	assertEquals(Platforms.findMin(new int[] {2, 5, 10, 16}, new int[] {30, 6, 13, 18}), 2);
    }
}
