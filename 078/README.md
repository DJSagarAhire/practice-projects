##Minimum Platforms Required for Railway Station

*Problem Statement*: Given an array of arrival times and corresponding departure times for trains at a railway station, find the minimum number of platforms required on the station. (Assume that times are given such that `arrival[i]` and `departure[i]` pertain to train number `i`, and times are integers denoting a certain timestamp).

The idea is to first sort both the arrival and departure arrays (provided solution assumes arrivals are already sorted, but sorting them is trivial), and then keeping track of the number of trains at the platform at a particular time using a merge sort-like step. Read more on [geeks for geeks](http://www.geeksforgeeks.org/minimum-number-platforms-required-railwaybus-station/).

Time Complexity: **O(n log n)**  
Space Complexity: **O(1)** extra.
