import java.util.Arrays;

public class Platforms
{
    public static int findMin(int[] arrival, int[] departure)
    {
	Arrays.sort(departure);
	int i=0, j=0;
	int currTrains = 0;
	int maxTrains = 0;

	while(i < arrival.length)
	{
	    if(arrival[i] < departure[j])
	    {
		currTrains++;
		i++;
	    }
	    else
	    {
		currTrains--;
		j++;
	    }
	    maxTrains = Math.max(currTrains, maxTrains);
	}

	return maxTrains;
    }
}
