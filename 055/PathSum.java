import java.util.ArrayList;

/**
   Prints all paths in a binary tree that sum up to a specified value. Requires BinaryTree.java
 */
public class PathSum
{
    public static void printPaths(BinaryTree<Integer> t, int target)
    {
	ArrayList<Integer> startpath = new ArrayList<Integer>();
	startpath.add(t.root.item);
	printNodePaths(t.root, startpath, target);
    }

    private static void printNodePaths(BinaryTree<Integer>.Node n, ArrayList<Integer> path, int target)
    {
	for(int i=path.size()-1, sum=0; i>=0; i--)
	{
	    sum += path.get(i);
	    if(sum == target)
	    {
		for(int j=i; j<path.size(); j++)
		    System.out.print(path.get(j) + ", ");
		System.out.println();
	    }
	}

	if(n.left != null)
	{
	    ArrayList<Integer> leftpath = new ArrayList<Integer>(path);
	    leftpath.add(n.left.item);
	    printNodePaths(n.left, leftpath, target);
	}
	if(n.right != null)
	{
	    ArrayList<Integer> rightpath = new ArrayList<Integer>(path);
	    rightpath.add(n.right.item);
	    printNodePaths(n.right, rightpath, target);
	}
    }

    public static void main(String[] ar)
    {
	BinaryTree<Integer> t = new BinaryTree<Integer>(10);

	BinaryTree<Integer>.Node n1 = t.insertNode(t.root, 0, true);
	BinaryTree<Integer>.Node n2 = t.insertNode(n1, 0, true);
	BinaryTree<Integer>.Node n3 = t.insertNode(n2, 10, true);
	BinaryTree<Integer>.Node n4 = t.insertNode(n1, 10, false);

	BinaryTree<Integer>.Node n5 = t.insertNode(t.root, 5, false);
	BinaryTree<Integer>.Node n6 = t.insertNode(n5, 2, true);
	BinaryTree<Integer>.Node n7 = t.insertNode(n6, 3, true);
	BinaryTree<Integer>.Node n8 = t.insertNode(n5, 5, false);
	BinaryTree<Integer>.Node n9 = t.insertNode(n8, -10, true);

	t.printTree();

	printPaths(t, 10);
    }
}
