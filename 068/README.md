##String Edit Distance

*Problem Statement*: Given 2 strings `s` and `t` as input, find their [edit distance](http://en.wikipedia.org/wiki/Levenshtein_distance).

This is an outright dynamic programming implementation of the formula described in the accompanying [link](http://en.wikipedia.org/wiki/Levenshtein_distance).

Time Complexity: **O(mn)** (where `m` is length of `s`, `n` is length of `t`)  
Space Complexity: **O(mn)**
