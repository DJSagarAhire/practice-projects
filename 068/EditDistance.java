public class EditDistance
{
    public static int distance(String s, String t)
    {
	int[][] dp = new int[s.length()+1][t.length()+1];
	for(int i=0; i<=s.length(); i++)
	    dp[i][0] = i;
	for(int j=0; j<=t.length(); j++)
	    dp[0][j] = j;

	for(int i=1; i<=s.length(); i++)
	    for(int j=1; j<=t.length(); j++)
	    {
		int insert = dp[i-1][j] + 1;
		int delete = dp[i][j-1] + 1;
		int min = Math.min(insert, delete);

		if(s.charAt(i-1) == t.charAt(j-1))
		    dp[i][j] = Math.min(min, dp[i-1][j-1]);
		else
		    dp[i][j] = Math.min(min, dp[i-1][j-1] + 1);
	    }

	return dp[s.length()][t.length()];
    }
}
