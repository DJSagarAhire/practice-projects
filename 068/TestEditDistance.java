import static org.junit.Assert.*;
import org.junit.Test;

public class TestEditDistance
{
    @Test
    public void sameStrings()
    {
	assertEquals(EditDistance.distance("hello", "hello"), 0);
    }

    @Test
    public void fullyDifferentStrings()
    {
	assertEquals(EditDistance.distance("abc", "defg"), 4);
    }

    @Test
    public void partiallyDifferent()
    {
	assertEquals(EditDistance.distance("beta", "theta"), 2);
    }

    @Test
    public void subString()
    {
	assertEquals(EditDistance.distance("triangle", "angle"), 3);
    }
}
