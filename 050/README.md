##Reconstruct Binary Tree from Inorder and Preorder Traversal

*Problem Statement*: Given the inorder and preorder traversals of a particular binary tree, reconstruct the tree.

The algorithm is a recursive algorithm that uses the fact that the first element in the preorder traversal is always the root of the binary tree. The left and right subtrees can be obtained by finding the index of the root value in the inorder list and dividing the 2 traversals on that basis.

In other words, if the value `preorder[0]` is found at index `i` in the inorder list, then the recursion is as follows:

1: Left subtree: inorder list = `inorder[0..i-1]`, preorder list = `preorder[1..i]`,  
2: Right subtree: inorder list = `inorder[i+1..n]`, preorder list = `preorder[i+1..n]`.  
Assume all indices are inclusive and new copies of the lists are created for each recursive call so that the indices always start from 0.
