import java.util.Arrays;

/**
   Reconstructs a binary tree from its inorder and preorder traversals. Requires BinaryTree.class
 */
public class ReconstructTree
{
    public static <Item> BinaryTree<Item> reconstructTree(Item[] inorder, Item[] preorder)
    {
	BinaryTree<Item> tree = new BinaryTree<Item>();
	tree.root = reconstructNodes(inorder, preorder, tree);

	return tree;
    }

    private static <Item> BinaryTree<Item>.Node reconstructNodes(Item[] inorder, Item[] preorder, BinaryTree<Item> tree)
    {
	// First node of preorder is always the root
	BinaryTree<Item>.Node root = tree.new Node();
	root.item = preorder[0];

	int indexInorder = Arrays.asList(inorder).indexOf(preorder[0]);     // Index of root in inorder traversal
	if(indexInorder == 0)
	    root.left = null;
	else
	    root.left = reconstructNodes(Arrays.copyOfRange(inorder, 0, indexInorder), Arrays.copyOfRange(preorder, 1, indexInorder+1), tree);

	if(indexInorder == inorder.length-1)
	    root.right = null;
	else
	    root.right = reconstructNodes(Arrays.copyOfRange(inorder, indexInorder+1, inorder.length), Arrays.copyOfRange(preorder, indexInorder+1, preorder.length), tree);

	return root;
    }

    public static void main(String[] ar)
    {
	Integer[] in1 = new Integer[] {9,2,6,4,5,1};
	Integer[] pr1 = new Integer[] {6,2,9,1,4,5};

	BinaryTree<Integer> t1 = reconstructTree(in1, pr1);

	t1.printTree();
    }
}
