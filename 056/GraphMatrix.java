import java.util.Queue;

/**
   Defines a Graph class that uses an adjacency matrix.
   Nodes are denoted using integers 0, 1, ..., n-1.
 */
public class GraphMatrix
{
    public boolean[][] adjMatrix;
    public int size;

    public GraphMatrix() { }

    public GraphMatrix(int nodes)        // nodes = No of nodes in graph
    {
	size = nodes;
	adjMatrix = new boolean[size][size];
    }

    public void addEdge(int startNode, int endNode)
    {
	adjMatrix[startNode][endNode] = true;
    }

    public void removeEdge(int startNode, int endNode)
    {
	adjMatrix[startNode][endNode] = false;
    }

    /**
       Performs a depth-first search starting from specified node. If specified node value is -1, starts from 0 instead.
     */
    public void depthFirst(int node)
    {
	if(node == -1)
	    node = 0;

	boolean[] visited = new boolean[size];
	depthFirstRecursive(node, visited);
    }

    private boolean[] depthFirstRecursive(int node, boolean[] visited)
    {
	if(visited[node])
	    return visited;

	visited[node] = true;
	System.out.print(node + ", ");

	for(int i=0; i<size; i++)
	    if(adjMatrix[node][i])
		visited = depthFirstRecursive(i, visited);

	return visited;
    }

    public void breadthFirst(int node)
    {
	if(node == -1)
	    node = 0;

	boolean[] visited = new boolean[size];
	boolean[] discovered = new boolean[size];
	// visited[node] = true;
	// System.out.print(node + ", ");

	Queue<Integer> nodeQueue = new java.util.ArrayDeque<>();
	nodeQueue.add(node);
	discovered[node] = true;

	while(nodeQueue.size() != 0)
	{
	    int curr = nodeQueue.remove();
	    visited[curr] = true;
	    System.out.print(curr + ", ");

	    for(int i=0; i<size; i++)
		if(adjMatrix[curr][i] && !discovered[i])
		{
		    nodeQueue.add(i);
		    discovered[i] = true;
		}
	}
    }

    public static void main(String[] ar)
    {
	// Undirected graphs

	// Path on 4 vertices
	GraphMatrix p4 = new GraphMatrix(4);
	p4.addEdge(0, 1);
	p4.addEdge(1, 0);
	p4.addEdge(1, 2);
	p4.addEdge(2, 1);
	p4.addEdge(2, 3);
	p4.addEdge(3, 2);

	System.out.println("P4: ");
	p4.depthFirst(2);
	System.out.println();
	p4.breadthFirst(2);
	System.out.println();

	// Complete bipartite graph on 1 and 3 vertices
	GraphMatrix k13 = new GraphMatrix(4);
	k13.addEdge(0, 1);
	k13.addEdge(1, 0);
	k13.addEdge(0, 2);
	k13.addEdge(2, 0);
	k13.addEdge(0, 3);
	k13.addEdge(3, 0);

	System.out.println("K(1, 3): ");
	k13.depthFirst(2);
	System.out.println();
	k13.breadthFirst(2);
	System.out.println();

	// Cycle on 4 vertices
	GraphMatrix c4 = new GraphMatrix(4);
	c4.addEdge(0, 1);
	c4.addEdge(1, 0);
	c4.addEdge(1, 2);
	c4.addEdge(2, 1);
	c4.addEdge(2, 3);
	c4.addEdge(3, 2);
	c4.addEdge(3, 0);
	c4.addEdge(0, 3);

	System.out.println("C4: ");
	c4.depthFirst(0);
	System.out.println();
	c4.breadthFirst(0);
	System.out.println();

	// Complete bipartite graph on 3 and 2 vertices
	GraphMatrix k32 = new GraphMatrix(5);
	k32.addEdge(0, 3);
	k32.addEdge(3, 0);
	k32.addEdge(0, 4);
	k32.addEdge(4, 0);
	k32.addEdge(1, 3);
	k32.addEdge(3, 1);
	k32.addEdge(1, 4);
	k32.addEdge(4, 1);
	k32.addEdge(2, 3);
	k32.addEdge(3, 2);
	k32.addEdge(2, 4);
	k32.addEdge(4, 2);

	System.out.println("K(3, 2):");
	k32.depthFirst(3);
	System.out.println();
	k32.breadthFirst(3);
	System.out.println();

	// 2 components each containing C3/K3
	GraphMatrix c3c3 = new GraphMatrix(6);
	c3c3.addEdge(0, 1);
	c3c3.addEdge(1, 0);
	c3c3.addEdge(1, 2);
	c3c3.addEdge(2, 1);
	c3c3.addEdge(2, 0);
	c3c3.addEdge(0, 2);
	c3c3.addEdge(3, 4);
	c3c3.addEdge(4, 3);
	c3c3.addEdge(4, 5);
	c3c3.addEdge(5, 4);
	c3c3.addEdge(3, 5);
	c3c3.addEdge(5, 3);

	System.out.println("C3 + C3:");
	c3c3.depthFirst(4);
	System.out.println();
	c3c3.breadthFirst(4);
	System.out.println();

	// Directed Graphs

	// Path on 4 vertices
	GraphMatrix p4d = new GraphMatrix(4);
	p4d.addEdge(0, 1);
	p4d.addEdge(1, 2);
	p4d.addEdge(2, 1);
	p4d.addEdge(3, 2);

	System.out.println("Directed Subset of P4:");
	p4d.depthFirst(0);
	System.out.println();
	p4d.breadthFirst(0);
	System.out.println();

	// Complete bipartite graph on 1 and 3 vertices
	GraphMatrix k13d = new GraphMatrix(4);
	k13d.addEdge(0, 1);
	k13d.addEdge(0, 2);
	k13d.addEdge(0, 3);
	k13d.addEdge(2, 0);

	System.out.println("Directed subset of K(1, 3):");
	k13d.depthFirst(2);
	System.out.println();
	k13d.breadthFirst(2);
	System.out.println();
    }
}
