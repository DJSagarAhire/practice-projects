##Merge Sort

*Problem Statement*: Implement Merge Sort for an array.

Time Complexity: **O(n log n)** Space Complexity: **O(n)** auxiliary space
