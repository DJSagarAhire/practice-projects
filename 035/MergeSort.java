public class MergeSort
{
    public static void sort(Comparable[] a)
    {
	mergeSort(a, 0, a.length-1);
    }

    private static void mergeSort(Comparable[] a, int low, int high)
    {
	if(low >= high)    // One element. Nothing to do here
	    return;

	// Split array into two and recurse
	int mid = (low + high) / 2;
	mergeSort(a, low, mid);
	mergeSort(a, mid+1, high);

	// Merge both halves
	merge(a, low, high, mid);
    }

    private static void merge(Comparable[] a, int low, int high, int mid)
    {
	int mergeLength = high - low + 1;
	Comparable[] aux = new Comparable[mergeLength];

	int i=low;
	int j=mid+1;
	int k=0;

	// Merge subarrays
	while(k < mergeLength)
	{
	    if(a[i].compareTo(a[j]) < 0)    // a[i] < a[j]
	    {
		aux[k] = a[i];
		i++;
	    }
	    else                            // a[i] > a[j]
	    {
		aux[k] = a[j];
		j++;
	    }
	    k++;

	    if(i == mid+1)      // i has gone past first subarray
	    {
		for(; j <= high; j++, k++)
		    aux[k] = a[j];
	    }

	    if(j == high+1)     // j has gone past second subarray
	    {
		for(; i <= mid; i++, k++)
		    aux[k] = a[i];
	    }
	}

	// Now copy aux into a
	for(int l=0; l<mergeLength; l++)
	{
	    a[low + l] = aux[l];
	}
    }
}
