##Rectangular Game

*Problem Statement*: [Hackerrank 101 Hack March](https://www.hackerrank.com/contests/101mar14/challenges/rectangular-game)  

###Solution

We need to find the area of the rectangle that has maximum additions performed on it, that is the area with the most overlaps. Since (1, 1) is always a part of this rectangle, the other extent of the rectangle is simply the *minimum of all the co-ordinates in either direction*, i.e. (`min_X`, `min_Y`), where `min_X` is the minimum across all X co-ordinates and `min_Y` is the minimum across all Y co-ordinates. These are the largest co-ordinates on which all addition operations are performed.

Note: Final result is too large to fit in 4 bytes.

Time Complexity: **O(n)** [Constraint of n <= 100 was highly misleading]  
Space Complexity: **O(1)**
