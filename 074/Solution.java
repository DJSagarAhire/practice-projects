import java.io.*;

public class Solution
{
    public static void main(String[] ar) throws IOException
    {
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

	int n = Integer.parseInt(br.readLine());
	long minX = Long.MAX_VALUE;
	long minY = Long.MAX_VALUE;

	for(int i=0; i<n; i++)
	{
	    String[] xy = br.readLine().split(" ");
	    minX = Math.min(minX, Long.parseLong(xy[0]));
	    minY = Math.min(minY, Long.parseLong(xy[1]));
	}

	System.out.println(minX * minY);
    }
}
