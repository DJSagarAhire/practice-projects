##2 Stacks Using 1 Array

*Problem Statement*: Implement 2 stacks using 1 single array.

Here, Stack 1 grows from 0 to `size`-1 while Stack 2 grows from `size`-1 to 0.
