/**
   Provides an implementation of 2 stacks in a single array.
   @author Sagar Ahire
 */
public class My2Stacks<Item>
{
    private Item[] s = (Item[]) new Object[2];
    private int top1 = -1;
    private int top2 = 2;

    private void resize(int size)
    {
	Item[] temp = (Item []) new Object[size];

	for(int i=0; i<size1(); i++)
	    temp[i] = s[i];

	for(int i=1; i<=size2(); i++)
	    temp[size - i] = s[s.length - i];

	top2 = size - size2();
	s = temp;
    }

    /**
       Pushes an item into Stack 1.
       @param i Item to be pushed.
     */
    public void push1(Item i)
    {
	if(isFull())
	    resize(s.length * 2);
	s[++top1] = i;
    }

    /**
       Pushes an item into Stack 2.
       @param i Item to be pushed.
     */
    public void push2(Item i)
    {
	if(isFull())
	    resize(s.length * 2);
	s[--top2] = i;
    }

    /**
       Pops an item from Stack 1.
       @return Item at the top of stack 1.
     */
    public Item pop1()
    {
	if(isEmpty1())
	    throw new java.util.EmptyStackException();

	Item i = s[top1];
	s[top1--] = null;

	if(size1() + size2() < s.length / 4)
	    resize(s.length / 2);

	return i;
    }

    /**
       Pops an item from Stack 2.
       @return Item at the top of stack 2.
     */
    public Item pop2()
    {
	if(isEmpty2())
	    throw new java.util.EmptyStackException();

	Item i = s[top2];
	s[top2++] = null;

	if(size1() + size2() < s.length / 4)
	    resize(s.length / 2);

	return i;
    }

    /**
       Returns the item at the top of Stack 1 but does not remove it from the stack.
       @return Item at the top of Stack 1.
     */
    public Item peek1()
    {
	if(isEmpty1())
	    throw new java.util.EmptyStackException();
	return s[top1];
    }

    /**
       Returns the item at the top of Stack 2 but does not remove it from the stack.
       @return Item at the top of Stack 2.
     */
    public Item peek2()
    {
	if(isEmpty2())
	    throw new java.util.EmptyStackException();
	return s[top2];
    }

    /**
       Returns the number of elements in Stack 1.
       @return Number of elements in Stack 1.
     */
    public int size1()
    {
	return top1 + 1;
    }

    /**
       Returns the number of elements in Stack 2.
       @return Number of elements in Stack 2.
     */
    public int size2()
    {
	return s.length - top2;
    }

    private boolean isFull()
    {
	return (top1 == top2-1);
    }

    /**
       Checks if Stack 1 is empty.
       @return true if Stack 1 is empty, false otherwise.
     */
    public boolean isEmpty1()
    {
	return (top1 == -1);
    }

    /**
       Checks if Stack 2 is empty.
       @return true if Stack 2 is empty, false otherwise.
     */
    public boolean isEmpty2()
    {
	return (top2 == s.length);
    }

    public static void main(String[] ar)
    {
	My2Stacks<Integer> test = new My2Stacks<Integer>();

	System.out.println("Pushing 1 on Stack 1.");
	test.push1(1);

	System.out.println("Pushing 2 on Stack 1.");
	test.push1(2);

	System.out.println("Pushing 3 on Stack 2.");
	test.push2(3);

	System.out.println("Pushing 4 on Stack 1.");
	test.push1(4);

	System.out.println("Pushing 5 on Stack 1.");
	test.push1(5);

	System.out.println("Popping Stack 2: " + test.pop2());

	System.out.println("Popping Stack 1: " + test.pop1());
    }
}
