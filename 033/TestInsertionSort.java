import static org.junit.Assert.*;
import org.junit.Test;

public class TestInsertionSort
{
    @Test
    public void alreadyUnsorted()
    {
	Integer[] a1 = new Integer[] {3, 2, 6, 1};
	InsertionSort.sort(a1);
	assertArrayEquals(a1, new Integer[] {1, 2, 3, 6});
    }

    @Test
    public void reverseSorted()
    {
	Integer[] a1 = new Integer[] {8, 6, 4, 2, 1};
	InsertionSort.sort(a1);
	assertArrayEquals(a1, new Integer[] {1, 2, 4, 6, 8});
    }

    @Test
    public void alreadySorted()
    {
	Integer[] a1 = new Integer[] {2, 4, 6, 8};
	InsertionSort.sort(a1);
	assertArrayEquals(a1, new Integer[] {2, 4, 6, 8});
    }

    @Test
    public void singleElement()
    {
	Integer[] a1 = new Integer[] {3};
	InsertionSort.sort(a1);
	assertArrayEquals(a1, new Integer[] {3});
    }

    @Test
    public void strings()
    {
	String[] a1 = new String[] {"happy", "sad", "angry", "depressed"};
	InsertionSort.sort(a1);
	assertArrayEquals(a1, new String[] {"angry", "depressed", "happy", "sad"});
    }
}
