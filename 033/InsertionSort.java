public class InsertionSort
{
    public static void sort(Comparable[] a)
    {
	for(int i=1; i<a.length; i++)
	{
	    int j = binarySearch(a, a[i], 0, i);
	    Comparable elt = a[i];

	    // Now insert elt at 'j' and shift the others
	    for(int k=i-1; k>=j; k--)
		a[k+1] = a[k];
	    a[j] = elt;
	}
    }

    /**
       Searches for the location where 'element' should be inserted in 'a' using binary search.
     */
    private static int binarySearch(Comparable[] a, Comparable element, int low, int high)
    {
	if(low == high)
	    return low;

	if(a[(low+high)/2].compareTo(element) > 0)
	    return binarySearch(a, element, low, (low+high)/2);
	else
	    return binarySearch(a, element, (low+high)/2 + 1, high);
    }

    public static void main(String[] ar)
    {
	Integer[] a1 = new Integer[] {3, 2, 6, 1};
	InsertionSort.sort(a1);

	for(int i : a1)
	    System.out.println(i);
    }
}
