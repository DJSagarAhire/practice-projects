/**
   Converts a binary search tree to a linked list. Requires BinarySearchTree.class from #043 and MySkeletalDoublyLinkedList.class
 */
public class BSTToLL
{
    public static MySkeletalDoublyLinkedList convert(BinarySearchTree t)
    {
	MySkeletalDoublyLinkedList l = new MySkeletalDoublyLinkedList();
	convertNodes(t.root, l);
	return l;
    }

    private static void convertNodes(BinarySearchTree.Node tn, MySkeletalDoublyLinkedList l)
    {
	if(tn == null)
	    return;

	convertNodes(tn.left, l);

	if(l.head == null)
	{
	    l.head = l.new Node();
	    l.head.item = tn.item;
	    l.tail = l.head;
	}
	else
	{
	    l.tail.next = l.new Node();
	    l.tail.next.item = tn.item;
	    l.tail.next.prev = l.tail;
	    l.tail = l.tail.next;
	}

	convertNodes(tn.right, l);
    }

    public static void main(String[] ar)
    {
	BinarySearchTree<Integer> t = new BinarySearchTree<Integer>(5);
	// t.insert(2);
	t.insert(1);
	t.insert(3);
	// t.insert(7);
	// t.insert(6);
	// t.insert(8);

	t.printTree();

	MySkeletalDoublyLinkedList l = convert(t);
	l.printList();
	l.printListBackwards();
    }
}
