##Pascal's Triangle

*Problem Statement*: Given a natural number `n`, generate `n` levels of the Pascal triangle.

The implementation uses the fact that each level of the Pascal triangle can be obtained by summing appropriate entries from the level above.

The following is the algorithm:

1: Accept `n`. If it is not positive, throw an `IllegalArgumentException`.

2: Create a 2D array `pascal` with `n` rows.

3: Create first row `pascal[0]` with 1 element, a 1.

4: Create the next row `pascal[i]` with `i`+1 elements.

5: Set `pascal[i][0]` and `pascal[i][i]` to 1. For all other elements `j`, set `pascal[i][j]` = `pascal[i-1][j-1]` + `pascal[i-1][j]`.

6: Repeat until `i` reaches `n` and then return `pascal`.

Running time: **O(n^2)**.
