/**
   Provides an implementation of a method to generate 'n' levels of Pascal triangle.
   @author Sagar Ahire
 */
public class Pascal
{
    /**
       Returns a 2D array with 'n' levels of the Pascal triangle.
       @param n A natural number
       @return A 2-D array with 'n' rows and columns 1, 2, ..., n.
     */
    public int[][] generatePascal(int n)
    {
	if(n <= 0)
	    throw new IllegalArgumentException("Invalid number of levels " + n);

	int[][] pascal = new int[n][];

	// Top level (Level 0)
	pascal[0] = new int[1];
	pascal[0][0] = 1;

	// Level 1 onwards
	for(int i=1; i<n; i++)
	{
	    pascal[i] = new int[i+1];

	    // Left and right ends of each level are always 1
	    pascal[i][0] = 1;
	    pascal[i][i] = 1;

	    // Other entries for the level are sums of entries in the above level
	    for(int j=1; j<i; j++)
		pascal[i][j] = pascal[i-1][j-1] + pascal[i-1][j];
	}

	return pascal;
    }

    /**
       Accepts a Pascal triangle as a 2D array and prints it. Pretty printing NOT supported.
       @param pascal A 2D array
     */
    public void printPascal(int[][] pascal)
    {
	for(int i=0; i<pascal.length; i++)
	{
	    for(int num : pascal[i])
		System.out.print(num + " ");
	    System.out.println();
	}
    }

    /**
       Driver method for testing
     */
    public static void main(String[] ar)
    {
	int num;
	if(ar.length == 1)
	    num = Integer.parseInt(ar[0]);
	else
	    num = 6;
	Pascal test = new Pascal();

	test.printPascal(test.generatePascal(num));
    }
}
