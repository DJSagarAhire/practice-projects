##Reverse a 32-bit Integer

*Problem Statement*: Accept an integer assumed to be 32 bits, unsigned, and reverse its bits (not to be confused with flip - reverse implies the LSB of the original becomes the MSB of the new integer, and so on).

An easy way is to convert the number into its binary string representation, reverse it, and then build the string accordingly. However, there is a better, more theoretically sound way.

This relies on the fact that the bits of an integer can be obtained in reverse order by performing division modulo 2. These reversed bits can then be combined together by repeated multiplication to get the reversed integer. This is given by:

new reversed number = prev reversed number * 2 + next bit from original number
