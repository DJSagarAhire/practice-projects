public class ReverseInt
{
    public static long reverse(long num)
    {
	long res = 0;
	int bitsDone = 0;

	while(num > 0)
	{
	    long rem = num % 2;
	    res = res * 2 + rem;
	    num /= 2;
	    bitsDone++;
	}

	for(; bitsDone < 32; bitsDone++)
	    res *= 2;

	return res;
    }
}
