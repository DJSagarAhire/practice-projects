import static org.junit.Assert.*;
import org.junit.Test;

public class TestReverseInt
{
    @Test
    public void zero()
    {
	assertEquals(ReverseInt.reverse(0), 0);
    }

    @Test
    public void allOnes()
    {
	assertEquals(ReverseInt.reverse(4294967295l), 4294967295l);
    }

    @Test
    public void smallOnes()
    {
	assertEquals(ReverseInt.reverse(255), 4278190080l);
    }

    @Test
    public void powerOf2()
    {
	assertEquals(ReverseInt.reverse(4096), 524288);
    }

    @Test
    public void intermittentZerosAndOnes()
    {
	assertEquals(ReverseInt.reverse(106), 1442840576);
    }
}
