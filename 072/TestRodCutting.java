import static org.junit.Assert.*;
import org.junit.Test;

public class TestRodCutting
{
    @Test
    public void length1()
    {
	assertEquals(RodCutting.maxValue(new int[] {6}), 6);
    }

    @Test
    public void length2cuts1()
    {
	assertEquals(RodCutting.maxValue(new int[] {6, 20}), 20);
    }

    @Test
    public void length3cuts1()
    {
	assertEquals(RodCutting.maxValue(new int[] {6, 11, 20}), 20);
    }

    @Test
    public void length2cuts2()
    {
	assertEquals(RodCutting.maxValue(new int[] {6, 8}), 12);
    }

    @Test
    public void length3cuts3()
    {
	assertEquals(RodCutting.maxValue(new int[] {6, 9, 10}), 18);
    }

    @Test
    public void length3cuts2()
    {
	assertEquals(RodCutting.maxValue(new int[] {6, 16, 21}), 22);
    }

    @Test
    public void length8cuts2()
    {
	assertEquals(RodCutting.maxValue(new int[] {1, 5, 8, 9, 10, 17, 17, 20}), 22);
    }

    @Test
    public void length8cuts1()
    {
	assertEquals(RodCutting.maxValue(new int[] {3, 5, 8, 9, 10, 17, 17, 20}), 24);
    }
}
