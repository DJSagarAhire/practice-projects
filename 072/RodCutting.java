public class RodCutting
{
    public static int maxValue(int[] values)
    {
	int n = values.length;
	int[] mv = new int[n+1];

	for(int i=0; i<=n; i++)
	    for(int j=1; j<=i; j++)
		mv[i] = Math.max(mv[i], mv[i-j] + values[j-1]);

	return mv[n];
    }
}
