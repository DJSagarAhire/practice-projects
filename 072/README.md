##Rod-Cutting Problem

*Problem Statement*: Given a rod of length `n`, you are given values of various lengths of rod from `0` to `n`. Find the maximum possible value that can be obtained by cutting the rod into pieces and selling them separately for the specified values.

This problem has been covered in more detail [here](http://www.radford.edu/~nokie/classes/360/dp-rod-cutting.html) and [here](http://www.geeksforgeeks.org/dynamic-programming-set-13-cutting-a-rod/), among many other places.

The recurrence for the maximum value for a rod with length `l` is given by:  
![](http://quicklatex.com/cache3/ql_28895ed35b9a63c1cfd7d5f2bb77e5a3_l3.png)

Here, `maxvalue(l)` denotes the maximum possible value obtainable from a rod of length `l`. `maxvalue(0)` is 0.

Time Complexity: **O(n^2)**  
Space Complexity: **O(n)**
