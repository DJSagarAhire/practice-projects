import java.util.TreeMap;
import java.util.ArrayList;

/**
   Creates a Minimal Spanning Tree of a weighted graph using Kruskal's algorithm and outputs its weight.
   Requires WeightedGraph.class and UnionFind.class.
 */
public class KruskalMST
{
    public static double findWeight(WeightedGraph g)
    {
	double mstWeight = 0.0;

	UnionFind uf = new UnionFind(g.size);
	TreeMap<Double, ArrayList<Integer[]>> edges = new TreeMap<>();

	// Populate the TreeMap with edges
	for(int u=0; u<g.size; u++)
	{
	    for(int v: g.edges[u].keySet())
	    {
		double weight = g.edges[u].get(v);

		if(edges.containsKey(weight))
		    edges.get(weight).add(new Integer[] {u, v});
		else
		{
		    ArrayList<Integer[]> al = new ArrayList<>();
		    al.add(new Integer[] {u, v});
		    edges.put(weight, al);
		}
	    }
	}

	while(uf.getComponents() > 1)
	{
	    double currentMinWeight = edges.firstKey();
	    ArrayList<Integer[]> currentEdges = edges.remove(currentMinWeight);
	    for(int i=0; i<currentEdges.size(); i++)
	    {
		Integer[] e = currentEdges.get(i);
		if(uf.find(e[0]) != uf.find(e[1]))
		{
		    uf.union(e[0], e[1]);
		    mstWeight += currentMinWeight;

		    if(uf.getComponents() == 1)
			break;
		}
	    }
	}

	return mstWeight;
    }
}
