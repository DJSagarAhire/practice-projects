##Kruskal's Minimal Spanning Tree Algorithm

*Problem Statement*: Given a weighted simple graph, find the weight of its minimal spanning tree using Kruskal's algorithm.

[Kruskal's Minimal Spanning Tree Algorithm](http://en.wikipedia.org/wiki/Kruskal's_algorithm) creates a minimal spanning tree by selecting the smallest-weight available edge which connects two different components of the graph, starting from a set of isolated vertices.

In order to determine which edges connect different components, a data structure is required to represent components and join them in an efficient manner. This is done by the [Union find](http://en.wikipedia.org/wiki/Disjoint-set_data_structure) data structure. The implementation of Union Find provided here is based on a discussion in Sedgewick's Algorithms textbook.

Time Complexity: **O(m log m)**, where `m` is the number of edges.  
Space Complexity: **O(m+n)**, where `n` is the number of vertices.
