import static org.junit.Assert.*;
import org.junit.Test;

public class TestUnionFind
{
    @Test
    public void findInitial()
    {
	UnionFind uf4 = new UnionFind(4);

	assertEquals(uf4.find(3), 3);
    }

    @Test
    public void findAfterUnion()
    {
	UnionFind uf4 = new UnionFind(6);

	uf4.union(3, 5);
	uf4.union(2, 3);

	assertEquals(uf4.find(2), 5);
    }
}
