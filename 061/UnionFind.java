public class UnionFind
{
    // An element-indexed array. Each entry parents[i] points to i's parent.
    private int[] parents;

    // An element-indexed array. Each entry children[i] stores the number of children in the subtree rooted at i.
    private int[] children;

    // The number of components in the UnionFind.
    private int components;
    private int size;

    public UnionFind() { }
    public UnionFind(int size)
    {
	this.size = size;
	components = size;
	parents = new int[size];
	children = new int[size];

	for(int i=0; i<size; i++)
	{
	    parents[i] = i;         // Each element is its own root
	    children[i] = 1;       // Each subtree has only 1 element
	}
    }

    // Returns the root of the element 'e'
    public int find(int e)
    {
	int root = e;
	while(parents[root] != root)
	    root = parents[root];

	return root;
    }

    // Performs a union of the 2 components for specified elements
    public void union(int e, int f)
    {
	int roote = find(e);
	int rootf = find(f);

	if(children[roote] > children[rootf])    // 'roote' has the larger subtree. Add 'rootf' to it.
	{
	    parents[rootf] = roote;
	    children[roote] += children[rootf];
	}
	else
	{
	    parents[roote] = rootf;
	    children[rootf] += children[roote];
	}

	components--;
    }

    public int getComponents()
    {
	return components;
    }

    public int getSize()
    {
	return size;
    }
}
